package com.manuelschischkoff.companionplantingtool.spring;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.presenter.FieldSolutionView;
import com.manuelschischkoff.companionplantingtool.presenter.PlantSelectionView;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.communication.PushMode;

@Push(PushMode.MANUAL)
@Route
public class MainView extends VerticalLayout {
    private MainController controller = new MainController();
    private PlantSelectionView plantSelectionView;
    private FieldSolutionView fieldSolutionView;

    public MainView() {
        addHeader();
        setupPlantSelectionView();
        setupFieldSolutionView();

        connectController();

        setWidth("90vw");
        setAlignItems(Alignment.CENTER);
    }

    private void addHeader() {
        H1 header = new H1("Companion Planting Tool");
        add(header);
    }

    private void setupPlantSelectionView() {
        plantSelectionView = new PlantSelectionView(controller);
        add(plantSelectionView);
    }

    private void setupFieldSolutionView() {
        fieldSolutionView = new FieldSolutionView(controller);
        fieldSolutionView.setVisible(false);
        add(fieldSolutionView);
    }

    private void connectController() {
        controller.setPlantSelectionView(plantSelectionView);
        controller.setFieldSolutionView(fieldSolutionView);
    }
}
