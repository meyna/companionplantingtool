package com.manuelschischkoff.companionplantingtool.controller;

import com.manuelschischkoff.companionplantingtool.model.*;
import com.manuelschischkoff.companionplantingtool.presenter.FieldSolutionView;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.OptionalParametersDialog;
import com.manuelschischkoff.companionplantingtool.presenter.PlantSelectionView;
import com.manuelschischkoff.companionplantingtool.model.utils.LayoutSolver;
import com.vaadin.flow.component.dialog.Dialog;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class MainController {
    // VARIABLES

    private PlantSelectionView plantSelectionView;
    private FieldSolutionView fieldSolutionView;

    // Solver variables
    private FieldPositionValues waterSupplyPosition;
    private int northSouthDistance;
    private int spaceBetweenRows;
    private List<Plant> selectedPlants;
    private HashMap<Integer, Integer> plantRowLimitations;
    private LayoutSolver layoutSolver;

    // general checking bools
    private boolean layoutsRdyToFilter = false;
    private boolean optimizedAmountOfSun = false;
    private boolean optimizedWaterDistances = false;
    private boolean optimizedBCDistances = false;
    private boolean optimizeGCRs = false;

    private boolean showFilterBtnAmountOfSun = false;
    private boolean showFilterBtnWaterDistances = false;
    private boolean showFilterBtnBCDistances = false;
    private boolean showFilterBtnGCNH = false;

    // model connectors
    private PlantService service = PlantService.getInstance();
    private int currentOptimumValue;
    private boolean findallOptimalSolutions = false;


    // CONSTRUCTOR

    public MainController() {
        service.setController(this);
    }

    // FUNCTIONS

    public Collection<Plant> findAll(String value) {
        return service.findAll(value);
    }

    public void calculateFieldLayout(List<Plant> selectedPlants) {
        this.selectedPlants = selectedPlants;

        Dialog optionsDialog = new OptionalParametersDialog(this, selectedPlants);
        optionsDialog.open();
    }

    public void prepareSolver() {
        layoutSolver= new LayoutSolver(this, selectedPlants, plantRowLimitations, optimizedBCDistances, optimizedAmountOfSun, optimizedWaterDistances, waterSupplyPosition, optimizeGCRs, northSouthDistance, spaceBetweenRows, fieldSolutionView);

        if (findallOptimalSolutions) {
            removeSolutionView();
            refreshSolutions(true);
            layoutSolver.findAllOptimalSolutions(currentOptimumValue);
            fieldSolutionView.addProgressBar();
        }

        layoutSolver.solve();
    }

    public void showSolutions(Solution solution, boolean isInitialCall) {
        fieldSolutionView.setVisible(true);
        fieldSolutionView.getElement().callFunction("scrollIntoView");

        fieldSolutionView.setSolution(solution, isInitialCall);
    }

    public void removeSolutionView() {
        fieldSolutionView.setVisible(false);
    }

    public void refreshSolutions(boolean resetButtonAvailability) {
        if (resetButtonAvailability) {
            resetShowOptButtons();
        }
        fieldSolutionView.refreshSolutions();
    }

    private void resetShowOptButtons() {
        showFilterBtnAmountOfSun = false;
        showFilterBtnBCDistances = false;
        showFilterBtnGCNH = false;
        showFilterBtnWaterDistances = false;
    }

    public void stopCalculations() {
        if (layoutSolver != null) {
            layoutSolver.stopCalculations();
        }
    }

    // GETTER AND SETTER
    public void setPlantSelectionView(PlantSelectionView plantSelectionView) {
        this.plantSelectionView = plantSelectionView;
    }

    private FieldSolutionView getFieldSolutionView() {
        return fieldSolutionView;
    }

    public void setFieldSolutionView(FieldSolutionView fieldSolutionView) {
        this.fieldSolutionView = fieldSolutionView;
    }

    public FieldPositionValues getWaterSupplyPosition() {
        return waterSupplyPosition;
    }

    public void setWaterSupplyPosition(FieldPositionValues waterSupplyPosition) {
        this.waterSupplyPosition = waterSupplyPosition;
    }

    public void setNorthSouthDistance(int northSouthDistance) {
        this.northSouthDistance = northSouthDistance;
    }

    public int getSpaceBetweenRows() {
        return spaceBetweenRows;
    }

    public void setSpaceBetweenRows(int spaceBetweenRows) {
        this.spaceBetweenRows = spaceBetweenRows;
    }

    public void optimizeAmountOfSun() {
        fieldSolutionView.addProgressBar();
        layoutSolver.findLayoutWithMaxAmountOfSunConventional();
    }

    public void optimizeWaterSuppDist(FieldPositionValues fieldPosValue) {
        fieldSolutionView.addProgressBar();
        layoutSolver.findLayoutWithShortestDistanceToWaterSupplyConventional(fieldPosValue);
    }

    public void optimizeBCDist() {
        fieldSolutionView.addProgressBar();
        layoutSolver.findLayoutWithMaximumDistanceBetweenBCs();
    }

    public void ensureGCNH() {
        fieldSolutionView.addProgressBar();
        layoutSolver.findLayoutWithOnlyGCAsNeighbours();
    }

    public void filterSolutions(Solution filteredSolution) {
        fieldSolutionView.filterSolutions(filteredSolution);
    }

    public void restoreResults() {
        if (!isOptimizedBCDistances()) {
            setShowFilterBtnBCDistances(true);
            getFieldSolutionView().getBcOptBtn().setEnabled(true);
        } else {
            setShowFilterBtnBCDistances(false);
            getFieldSolutionView().getBcOptBtn().setEnabled(false);
        }
        if (!isOptimizedAmountOfSun()) {
            setShowFilterBtnAmountOfSun(true);
            getFieldSolutionView().getHeightOptBtn().setEnabled(true);
        } else {
            setShowFilterBtnAmountOfSun(false);
            getFieldSolutionView().getHeightOptBtn().setEnabled(false);
        }
        if (!isOptimizedWaterDistances()) {
            setShowFilterBtnWaterDistances(true);
            getFieldSolutionView().getWaterOptBtn().setEnabled(true);
        } else {
            setShowFilterBtnWaterDistances(false);
            getFieldSolutionView().getWaterOptBtn().setEnabled(false);
        }
        if (!isOptimizeGCRs()) {
            setShowFilterBtnGCNH(true);
            getFieldSolutionView().getGcOptBtn().setEnabled(true);
        } else {
            setShowFilterBtnGCNH(false);
            getFieldSolutionView().getGcOptBtn().setEnabled(false);
        }
        layoutSolver.restoreInitialResults();
    }

    public boolean isShowFilterBtnAmountOfSun() {
        return showFilterBtnAmountOfSun;
    }

    public void setShowFilterBtnAmountOfSun(boolean showFilterBtnAmountOfSun) {
        this.showFilterBtnAmountOfSun = showFilterBtnAmountOfSun;
    }

    public boolean isShowFilterBtnWaterDistances() {
        return showFilterBtnWaterDistances;
    }

    public void setShowFilterBtnWaterDistances(boolean showFilterBtnWaterDistances) {
        this.showFilterBtnWaterDistances = showFilterBtnWaterDistances;
    }

    public boolean isShowFilterBtnBCDistances() {
        return showFilterBtnBCDistances;
    }

    public void setShowFilterBtnBCDistances(boolean showFilterBtnBCDistances) {
        this.showFilterBtnBCDistances = showFilterBtnBCDistances;
    }

    public boolean isShowFilterBtnGCNH() {
        return showFilterBtnGCNH;
    }

    public void setShowFilterBtnGCNH(boolean showFilterBtnGCNH) {
        this.showFilterBtnGCNH = showFilterBtnGCNH;
    }

    public boolean isLayoutsRdyToFilter() {
        return layoutsRdyToFilter;
    }

    public void setLayoutsRdyToFilter(boolean layoutsRdyToFilter) {
        this.layoutsRdyToFilter = layoutsRdyToFilter;
    }

    public boolean isOptimizedAmountOfSun() {
        return optimizedAmountOfSun;
    }

    public void setOptimizedAmountOfSun(boolean optimizedAmountOfSun) {
        this.optimizedAmountOfSun = optimizedAmountOfSun;

        if (optimizedAmountOfSun) {
            setShowFilterBtnAmountOfSun(false);
        }
    }

    public boolean isOptimizedWaterDistances() {
        return optimizedWaterDistances;
    }

    public void setOptimizedWaterDistances(boolean optimizedWaterDistances) {
        this.optimizedWaterDistances = optimizedWaterDistances;

        if (optimizedWaterDistances) {
            setShowFilterBtnWaterDistances(false);
        }
    }

    public boolean isOptimizedBCDistances() {
        return optimizedBCDistances;
    }

    public void setOptimizedBCDistances(boolean optimizedBCDistances) {
        this.optimizedBCDistances = optimizedBCDistances;

        if (optimizedBCDistances) {
            setShowFilterBtnBCDistances(false);
        }
    }

    public boolean isOptimizeGCRs() {
        return optimizeGCRs;
    }

    public void setOptimizeGCRs(boolean optimizeGCRs) {
        this.optimizeGCRs = optimizeGCRs;

        if (optimizeGCRs) {
            setShowFilterBtnGCNH(false);
        }
    }

    public void restoreDefaultPlantList() {
        this.service.restoreDefaultPlantList();
    }

    public void updateMainList() {
        plantSelectionView.updateListAfterItemChanges();
    }

    public void deactivateRestoreListBtn() {
        plantSelectionView.deactivateRestoreBtn();
    }

    public PlantService getPlantService() {
        return this.service;
    }

    public void setFixedVegetablePositions(HashMap<Integer, Integer> plantRowLimitation) {
        this.plantRowLimitations = plantRowLimitation;
    }

    // gets called by the button click of the user.
    public void setFindAllOptimalSolutions(boolean value) {
        this.findallOptimalSolutions = value;

        if (value) {
            prepareSolver();
        }
    }

    // gets set when there was an optimum selected and the solver found the optimum.
    public void setCurrentOptimumValue(int intValue) {
        this.currentOptimumValue = intValue;
    }

    public int getCurrentOptimumValue() {
        return currentOptimumValue;
    }

    public boolean isFindAllOptimalSolutions() {
        return this.findallOptimalSolutions;
    }
}
