package com.manuelschischkoff.companionplantingtool.presenter;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.Solution;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.BadCompanionDistanceOptDialog;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.GCOptDialog;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.HeightOptDialog;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.WaterOptDialog;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.dom.DomEventListener;
import org.vaadin.pekkam.Canvas;
import org.vaadin.pekkam.CanvasRenderingContext2D;

import java.util.List;
import java.util.stream.IntStream;
@Push
public class FieldDrawingAndStatisticsView extends VerticalLayout {


    private final MainController controller;
    private Solution solution;

    private Canvas layoutCanvas;
    private CanvasRenderingContext2D layoutCtx;
    private Canvas heightCanvas;
    private CanvasRenderingContext2D heightCtx;
    private Canvas waterCanvas;
    private CanvasRenderingContext2D waterCtx;
    private Canvas bcCanvas;
    private CanvasRenderingContext2D bcCtx;
    private Canvas gcCanvas;
    private CanvasRenderingContext2D gcCtx;

    private int CANVAS_WIDTH = 800;
    private int LAYOUT_CANVAS_HEIGHT = 500;
    private int HEIGHT_CANVAS_HEIGHT = 0;
    private int WATER_CANVAS_HEIGHT = 0;
    private int BC_CANVAS_HEIGHT = 0;
    private int GC_CANVAS_HEIGHT = 0;
    private int CM_TO_PX_RATIO = 2;
    private int X_MIN_FIELD;
    private int X_MAX_FIELD;
    private int Y_MIN_FIELD;
    private int Y_MAX_FIELD;
    private int Y_0_POINT_HEIGHT_PLOT;
    private int Y_0_POINT_WATER_PLOT;
    private int Y_0_POINT_BC_PLOT;
    private int Y_0_POINT_GC_PLOT;
    private String pathToBGSoilImg = "frontend/img/bgSoilO60.png";
    private double[][] vegRowRegion;
    private int[] xPos;

    private Div fieldLayoutContainer = new Div();
    private Div rowDetailContainer  =  new Div();
    private VerticalLayout verticalScrollableContainer = new VerticalLayout();
    private FieldDrawingRowDetailsView rowDetails = new FieldDrawingRowDetailsView();
    private HorizontalLayout layoutPlusDetailsContainer = new HorizontalLayout();


    // for the field stats
    private HorizontalLayout fieldStatsContainer;
    private Label sizeText = new Label();
    private Label sizeValue = new Label();
    private Label widthText = new Label();
    private Label widthValue = new Label();
    private Label heightText = new Label();
    private Label heightValue = new Label();
    private Label spaceBetweenRowsText = new Label();
    private Label spaceBetweenRowsValue = new Label();

    // for the profiles
    private HorizontalLayout heightProfileHeader = new HorizontalLayout();
    private HorizontalLayout waterProfileHeader = new HorizontalLayout();
    private HorizontalLayout bcProfileHeader = new HorizontalLayout();
    private HorizontalLayout gcProfileHeader = new HorizontalLayout();
    private H4 heightProfileText = new H4("Height-Profile");
    private H4 waterProfileText = new H4("Water-Profile");
    private H4 bcProfileText = new H4("Bad Companion Distances");
    private H4 gcProfileText = new H4("Good Companion Neighborhood");
    private Button heightOptBtn = new Button("Optimize");
    private Button waterOptBtn = new Button("Optimize");
    private Button bcOptBtn = new Button("Optimize");
    private Button gcOptBtn = new Button("Ensure Neighbors Are Good Companions");
//    private Label sunValueText = new Label();
    private Button findAllOptSolutionsBtn;


    public FieldDrawingAndStatisticsView(MainController controller) {
        this.controller = controller;

        fieldStatsContainer = new HorizontalLayout();

        Image northIndicator = new Image("frontend/img/northIndicator.png", "northIndicator");
        northIndicator.getStyle().set("padding-right", "1vw");
        fieldStatsContainer.add(northIndicator);

        sizeText.getStyle().set("font-weight", "bold");
        sizeText.getStyle().set("margin", "auto");
        sizeText.getStyle().set("padding-right", "0.2vw");
        sizeText.setText("Field size: ");
        sizeValue.getStyle().set("margin", "auto");
        sizeValue.getStyle().set("padding-right", "0.5vw");
        fieldStatsContainer.add(sizeText, sizeValue);

        widthText.getStyle().set("font-weight", "bold");
        widthText.getStyle().set("margin", "auto");
        widthText.getStyle().set("padding-right", "0.2vw");
        widthText.setText("Width (W-E): ");
        widthValue.getStyle().set("margin", "auto");
        widthValue.getStyle().set("padding-right", "0.5vw");
        fieldStatsContainer.add(widthText, widthValue);

        heightText.getStyle().set("font-weight", "bold");
        heightText.getStyle().set("margin", "auto");
        heightText.getStyle().set("padding-right", "0.2vw");
        heightText.setText("Length (N-S): ");
        heightValue.getStyle().set("margin", "auto");
        heightValue.getStyle().set("padding-right", "0.5vw");
        fieldStatsContainer.add(heightText, heightValue);

        spaceBetweenRowsText.getStyle().set("font-weight", "bold");
        spaceBetweenRowsText.getStyle().set("margin", "auto");
        spaceBetweenRowsText.getStyle().set("padding-right", "0.2vw");
        spaceBetweenRowsText.setText("Space between rows: ");
        spaceBetweenRowsValue.getStyle().set("margin", "auto");
        spaceBetweenRowsValue.getStyle().set("padding-right", "0.5vw");
        fieldStatsContainer.add(spaceBetweenRowsText, spaceBetweenRowsValue);

        // For the Layout drawing
        fieldLayoutContainer.getStyle().set("overflow", "auto");
        fieldLayoutContainer.getStyle().set("border", "1px solid #e6e6e6");

        fieldLayoutContainer.add(verticalScrollableContainer);

        rowDetailContainer.getStyle().set("overflow", "auto");
        rowDetailContainer.getStyle().set("border", "1px solid #e6e6e6");
        rowDetailContainer.setWidth("40vw");

        rowDetails.getStyle().set("display", "block");
        rowDetails.setSizeUndefined();
        rowDetailContainer.add(rowDetails);
        rowDetailContainer.setVisible(false);

        layoutPlusDetailsContainer.setWidth("85vw");
        layoutPlusDetailsContainer.setHeight("70vh");
        layoutPlusDetailsContainer.add(fieldLayoutContainer, rowDetailContainer);

        fieldStatsContainer.setWidth("50vw");
        add(fieldStatsContainer, layoutPlusDetailsContainer);

        // for the profile headers
        heightOptBtn.addClickListener(event -> {
            this.controller.stopCalculations();
            Dialog heightOptDialog = new HeightOptDialog(controller);
            heightOptDialog.open();
        });
        heightOptBtn.getStyle().set("margin", "auto");
        heightProfileText.getStyle().set("margin", "auto");
        heightProfileText.getStyle().set("padding-right", "1vw");
//        sunValueText.getStyle().set("text-color", "#ff0000");
        heightProfileHeader.add(heightProfileText, heightOptBtn/*, sunValueText*/);

        waterOptBtn.addClickListener(event -> {
            this.controller.stopCalculations();
            Dialog waterOptDialog = new WaterOptDialog(controller);
            waterOptDialog.open();
        });
        waterOptBtn.getStyle().set("margin", "auto");
        waterProfileText.getStyle().set("margin", "auto");
        waterProfileText.getStyle().set("padding-right", "1vw");
        waterProfileHeader.add(waterProfileText, waterOptBtn);

        bcOptBtn.addClickListener(event -> {
            this.controller.stopCalculations();
            Dialog bcdOptDialog = new BadCompanionDistanceOptDialog(controller);
            bcdOptDialog.open();
        });
        bcOptBtn.getStyle().set("margin", "auto");
        bcProfileText.getStyle().set("margin", "auto");
        bcProfileText.getStyle().set("padding-right", "1vw");
        bcProfileHeader.add(bcProfileText, bcOptBtn);

        // for the gc button
        gcOptBtn.addClickListener(event -> {
            this.controller.stopCalculations();
            Dialog gcOptDialog = new GCOptDialog(controller);
            gcOptDialog.open();
        });
        gcOptBtn.getStyle().set("margin", "auto");
        gcProfileText.getStyle().set("margin", "auto");
        gcProfileText.getStyle().set("padding-right", "1vw");
        gcProfileHeader.add(gcProfileText, gcOptBtn);

        disableAllOptBtns();

        if (this.controller.isLayoutsRdyToFilter()) {
            if (this.controller.isShowFilterBtnAmountOfSun()) {
                heightOptBtn.setEnabled(true);
            }
            if (this.controller.isShowFilterBtnWaterDistances()) {
                waterOptBtn.setEnabled(true);
            }
            if (this.controller.isShowFilterBtnBCDistances()) {
                bcOptBtn.setEnabled(true);
            }
            if (this.controller.isShowFilterBtnGCNH()) {
                gcOptBtn.setEnabled(true);
            }
        }
    }

    public void disableAllOptBtns() {
        heightOptBtn.setEnabled(false);
        waterOptBtn.setEnabled(false);
        bcOptBtn.setEnabled(false);
        gcOptBtn.setEnabled(false);
    }

    public void drawLayoutAndStatistics(List<Plant> plants, Solution solution/*, boolean isLastPlantOrder*/) {
        this.solution = solution;

        if (solution.getPlantOrders().size() <= 1) {
            disableAllOptBtns();
        }

        // add field stats
        sizeValue.setText((solution.getFieldHeightInCM() * solution.getFieldWidthInCM() / 10000.00) + " m²");
        widthValue.setText((solution.getFieldWidthInCM() / 100.00) + " m");
        heightValue.setText((solution.getFieldHeightInCM() / 100.00) + " m");
        spaceBetweenRowsValue.setText(solution.getSpaceBetweenRowsInCM() + " cm");

        rowDetailContainer.setVisible(false);

        prepareDrawings();

        drawLayout(plants);

//        if (!controller.isOptimizedAmountOfSun()) {
//            this.sunValueText.setVisible(false);
//        } else {
//            if (!isLastPlantOrder) {
//                this.sunValueText.setText("This layout is not the optimum.");
//            } else {
//                this.sunValueText.setText("This layout is currently the optimum.");
//            }
//            this.sunValueText.setVisible(true);
//        }
        drawHeightProfile(plants);

        drawWaterProfile(plants);

        drawBCProfile(plants);

        drawGCProfile(plants);
    }

    public void prepareDrawings() {
        CANVAS_WIDTH = solution.getFieldWidthInCM() * CM_TO_PX_RATIO + 80;

        if (solution.getFieldHeightInCM() == 0) {
            LAYOUT_CANVAS_HEIGHT = 100 * CM_TO_PX_RATIO + 80;
        } else {
            LAYOUT_CANVAS_HEIGHT = solution.getFieldHeightInCM()* CM_TO_PX_RATIO + 80;
        }

        if (layoutCtx != null) {
            layoutCtx.clearRect(0,0,CANVAS_WIDTH, LAYOUT_CANVAS_HEIGHT);
            verticalScrollableContainer.remove(layoutCanvas);
            verticalScrollableContainer.remove(heightProfileHeader);
            heightCtx.clearRect(0,0,CANVAS_WIDTH, HEIGHT_CANVAS_HEIGHT);
            verticalScrollableContainer.remove(heightCanvas);
            verticalScrollableContainer.remove(waterProfileHeader);
            waterCtx.clearRect(0,0,CANVAS_WIDTH, WATER_CANVAS_HEIGHT);
            verticalScrollableContainer.remove(waterCanvas);
            verticalScrollableContainer.remove(bcProfileHeader);
            bcCtx.clearRect(0,0,CANVAS_WIDTH, BC_CANVAS_HEIGHT);
            verticalScrollableContainer.remove(bcCanvas);
            verticalScrollableContainer.remove(gcProfileHeader);
            gcCtx.clearRect(0,0, CANVAS_WIDTH, GC_CANVAS_HEIGHT);
            verticalScrollableContainer.remove(gcCanvas);
        }

        X_MIN_FIELD = 40;
        X_MAX_FIELD = CANVAS_WIDTH - 40;
        Y_MIN_FIELD = 40;
        Y_MAX_FIELD = LAYOUT_CANVAS_HEIGHT - 40;

        layoutCanvas = new Canvas(CANVAS_WIDTH, LAYOUT_CANVAS_HEIGHT);
        layoutCanvas.setId("fieldLayoutCanvas");
        layoutCtx = layoutCanvas.getContext();

        verticalScrollableContainer.add(layoutCanvas);

        HEIGHT_CANVAS_HEIGHT = 0;
        List<Plant> anyPlantOrder = solution.getPlantOrders().get(0).getPlants();
        for (int i = 0; i < anyPlantOrder.size(); i++) {
            HEIGHT_CANVAS_HEIGHT = Math.max(HEIGHT_CANVAS_HEIGHT, anyPlantOrder.get(i).getHeightInCM());
        }

        HEIGHT_CANVAS_HEIGHT = HEIGHT_CANVAS_HEIGHT * CM_TO_PX_RATIO + 80;
        Y_0_POINT_HEIGHT_PLOT = HEIGHT_CANVAS_HEIGHT - 40;
        heightCanvas = new Canvas(CANVAS_WIDTH, HEIGHT_CANVAS_HEIGHT);
        heightCanvas.setId("heightCanvas");
        heightCtx = heightCanvas.getContext();

        verticalScrollableContainer.add(heightProfileHeader, heightCanvas);

        WATER_CANVAS_HEIGHT = 100 * CM_TO_PX_RATIO + 80;
        Y_0_POINT_WATER_PLOT = WATER_CANVAS_HEIGHT - 40;
        waterCanvas = new Canvas(CANVAS_WIDTH, WATER_CANVAS_HEIGHT);
        waterCanvas.setId("waterCanvas");
        waterCtx = waterCanvas.getContext();
        verticalScrollableContainer.add(waterProfileHeader, waterCanvas);

        BC_CANVAS_HEIGHT =
            anyPlantOrder.size()                /*nr of plants*/
            * Math.max(2 * (anyPlantOrder.size()), 20)    /*either nr of red Lines to other plants or 20 (the min height for text in the rectangle)*/
            * CM_TO_PX_RATIO
            + 80;                               /*Offset for text*/
        Y_0_POINT_BC_PLOT = BC_CANVAS_HEIGHT - 40;
        bcCanvas = new Canvas(CANVAS_WIDTH, BC_CANVAS_HEIGHT);
        bcCanvas.setId("bcCanvas");
        bcCtx = bcCanvas.getContext();
        verticalScrollableContainer.add(bcProfileHeader, bcCanvas);

        GC_CANVAS_HEIGHT =
                anyPlantOrder.size()                 /*nr of plants*/
                        * Math.max(2 * (anyPlantOrder.size()), 20)    /*either nr of red Lines to other plants or 20 (the min height for text in the rectangle)*/
                        * CM_TO_PX_RATIO
                        + 80;                               /*Offset for text*/
        Y_0_POINT_GC_PLOT = GC_CANVAS_HEIGHT - 40;
        gcCanvas = new Canvas(CANVAS_WIDTH, GC_CANVAS_HEIGHT);
        gcCanvas.setId("bcCanvas");
        gcCtx = gcCanvas.getContext();
        verticalScrollableContainer.add(gcProfileHeader, gcCanvas);

        if (controller.isOptimizedBCDistances() ||controller.isOptimizedAmountOfSun() || controller.isOptimizedWaterDistances()) {
            // Not working yet and not exactly necessary.
//            if (findAllOptSolutionsBtn == null && !controller.isFindAllOptimalSolutions()) {
//                findAllOptSolutionsBtn = new Button("Find All Optimal Solutions", click -> controller.setFindAllOptimalSolutions(true));
//                verticalScrollableContainer.add(findAllOptSolutionsBtn);
//            }
        }
    }

    private void drawFieldLayoutBackground() {
        double x = X_MIN_FIELD;
        double y = Y_MIN_FIELD;
        double width = 100;
        double height = 100;

        layoutCtx.save();

        while (y < Y_MAX_FIELD) {
            while (x < X_MAX_FIELD) {
                layoutCtx.drawImage(pathToBGSoilImg, x, y);
                x += width;
            }
            x = X_MIN_FIELD;
            y += height;
        }

        layoutCtx.restore();
    }

    private void drawFieldLayoutGrid() {
        double x = X_MIN_FIELD;
        double y = Y_MIN_FIELD;

        layoutCtx.save();

        // draw thin indicators every 10 cm
        layoutCtx.beginPath();
        layoutCtx.setLineWidth(0.5);
        layoutCtx.setStrokeStyle("#cccccc");
        layoutCtx.moveTo(x,y);

        // for the top to bottom lines
        while (x < X_MAX_FIELD) {
            if (((x - X_MIN_FIELD) / CM_TO_PX_RATIO) % 50 != 0) {
                layoutCtx.moveTo(x, y);
                layoutCtx.lineTo(x, Y_MAX_FIELD);
            }
            x += 10 * CM_TO_PX_RATIO;
        }

        // for the left to right lines
        x = X_MIN_FIELD;
        while (y < Y_MAX_FIELD) {
//            y += 10 * CM_TO_PX_RATIO;
            if (((y - Y_MIN_FIELD) / CM_TO_PX_RATIO) % 50 != 0) {
                layoutCtx.moveTo(x, y);
                layoutCtx.lineTo(X_MAX_FIELD, y);
            }
            y += 10 * CM_TO_PX_RATIO;
        }

        layoutCtx.stroke(); // draw all lines at once at the end of this stroke style
        layoutCtx.closePath();

        // draw main lines
        layoutCtx.beginPath();
        layoutCtx.setLineWidth(1.0);

        // draw thick indicators every 50 cm
        // for the top to bottom lines
        y = Y_MIN_FIELD;
        x= X_MIN_FIELD;
        x += 50 * CM_TO_PX_RATIO;
        while (x < X_MAX_FIELD) {
            layoutCtx.moveTo(x,y);
            layoutCtx.lineTo(x,Y_MAX_FIELD);
            x += 50 * CM_TO_PX_RATIO;
        }

        // for the left to right lines
        x = X_MIN_FIELD;
        y = Y_MIN_FIELD;
        y += 50 * CM_TO_PX_RATIO;
        while (y < Y_MAX_FIELD) {
            layoutCtx.moveTo(x,y);
            layoutCtx.lineTo(X_MAX_FIELD, y);
            y += 50 * CM_TO_PX_RATIO;
        }

        // Top left -> top right
        layoutCtx.moveTo(X_MIN_FIELD,Y_MIN_FIELD);
        layoutCtx.lineTo(X_MAX_FIELD,Y_MIN_FIELD);

        // Top right -> bottom right
        layoutCtx.lineTo(X_MAX_FIELD, Y_MAX_FIELD);

        // Bottom right -> Bottom left
        layoutCtx.lineTo(X_MIN_FIELD, Y_MAX_FIELD);

        // Bottom left -> top left
        layoutCtx.lineTo(X_MIN_FIELD,Y_MIN_FIELD);

        layoutCtx.stroke(); // draw all lines at once at the end of this stroke style
        layoutCtx.closePath();

        // Grid text
        // for the top and bottom lines
        x = X_MIN_FIELD;
        y = 35;
        int verticalTextOffset = 20;
        int textValue = 0;
        layoutCtx.setFont("20px Consolas");
        layoutCtx.fillText(textValue + "cm", x, y);
        layoutCtx.fillText("" + textValue, x, Y_MAX_FIELD + verticalTextOffset);

        int xOffset;
        String text;
        int length;
        x += 50 * CM_TO_PX_RATIO;
        while (x < X_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();

            if (textValue == solution.getFieldWidthInCM()) {
                xOffset = 10 * length + (2 * (length-1)); // 10 ... width of Consolas 20px letter, 2...space between 2 letters
            } else {
                xOffset = (10 * length + (2 * (length-1))) / 2;
            }

            layoutCtx.fillText(text, x - xOffset, y);
            layoutCtx.fillText(text, x - xOffset, Y_MAX_FIELD + verticalTextOffset);
            x += 50 * CM_TO_PX_RATIO;
        }

        // for the left and right lines
        x = X_MIN_FIELD - 39;
        y = Y_MIN_FIELD + verticalTextOffset - 5;
        textValue = 0;
        layoutCtx.fillText(textValue + "cm", x, y);
        layoutCtx.fillText("" + textValue, X_MAX_FIELD + 5, y);

        x = X_MIN_FIELD;
        y = Y_MIN_FIELD + 5;
        y += 50 * CM_TO_PX_RATIO;
        while (y < Y_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();

            if (textValue == solution.getFieldHeightInCM()) {
                y = Y_MAX_FIELD;
            }

            xOffset = 10 * length + (2 * (length-1)) + 5;

            layoutCtx.fillText(text, x - xOffset, y);
            layoutCtx.fillText(text, X_MAX_FIELD + 5, y);
            y += 50 * CM_TO_PX_RATIO;
        }

        layoutCtx.restore();
    }

    private void drawLayout(List<Plant> plants) {
        // draw background image
        drawFieldLayoutBackground();

        // for some reason i have to update the ui and wait a little bit, otherwise the objects will get drawn in wrong order.
        this.getUI().get().push();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // delete the parts of the background image, which is out of the field bounds
        layoutCtx.clearRect(X_MAX_FIELD, Y_MIN_FIELD, CANVAS_WIDTH - X_MAX_FIELD, LAYOUT_CANVAS_HEIGHT - Y_MIN_FIELD);
        layoutCtx.clearRect(X_MIN_FIELD, Y_MAX_FIELD, CANVAS_WIDTH - X_MIN_FIELD, LAYOUT_CANVAS_HEIGHT - Y_MAX_FIELD);

        // draw the grid
        drawFieldLayoutGrid();

        // draw all plants
        layoutCtx.save();

        double startAngle = 0;
        double endAngle = 2 * Math.PI;
        double x = X_MIN_FIELD;
        double y = Y_MIN_FIELD;
        double radius;
        int amount;
        vegRowRegion = new double[plants.size()][];
        xPos = new int[plants.size()];

        for (int i = 0; i < plants.size(); i++) {
            radius = (plants.get(i).getSpreadDiameterInCM() / 2.0) * CM_TO_PX_RATIO;

            if (i == 0) {
                x += radius;
            } else {
                x += solution.getSpaceBetweenRowsInCM() * CM_TO_PX_RATIO + radius;
            }

            amount = Math.max(plants.get(i).getAmount(), 1);

            double yOffset = 0;
            if (plants.get(i).getSpreadDiameterInCM() * plants.get(i).getAmount() < solution.getFieldHeightInCM()) {
                yOffset = solution.getFieldHeightInCM() - plants.get(i).getSpreadDiameterInCM() * plants.get(i).getAmount();
                yOffset /= (plants.get(i).getAmount() +1);
                yOffset *= CM_TO_PX_RATIO;
            }

            double firstYValue = 0;
            double secondYValue = 0;

            for (int j = 0; j < amount; j++) {
                layoutCtx.beginPath();

                y += radius + yOffset;
                layoutCtx.arc(x, y, radius, startAngle, endAngle, false);
                layoutCtx.setFillStyle("#99ff99");
                layoutCtx.fill();
                layoutCtx.stroke();
                layoutCtx.closePath();

                long fontSize = Math.round(Math.min(radius, 25.0));
                layoutCtx.setFont(String.format("%spx Consolas", fontSize));
                layoutCtx.setFillStyle("#000000");
                layoutCtx.fillText(plants.get(i).getName().substring(0,2), x - (fontSize + fontSize/10.0)/2.0, y + fontSize/3.0);

                if (j == 0) {
                    firstYValue = y;
                } else if (j == 1) {
                    secondYValue = y;
                }

                y += radius;
            }

            vegRowRegion[i] = new double[] {x - radius, x + radius};

//            System.out.println("Setting x-pos " + ((int)Math.round((x - X_MIN_FIELD)/CM_TO_PX_RATIO)) + " to index " + i + " which is plant " + plants.get(i).getName());
            xPos[i] = (int)Math.round((x - X_MIN_FIELD)/CM_TO_PX_RATIO);
            plants.get(i).setXPos(xPos[i]);
//            plants.get(i).setXPos((int)Math.round((x - X_MIN_FIELD)/CM_TO_PX_RATIO));
            plants.get(i).setYPos((int)Math.round((firstYValue - Y_MIN_FIELD)/CM_TO_PX_RATIO));
            plants.get(i).setSpacing((int)Math.round((secondYValue-firstYValue)/CM_TO_PX_RATIO));

            x += radius;
            y = Y_MIN_FIELD;
        }

//        for (int i = 0; i < plants.size(); i++) {
//            System.out.println("Plant on index " + i + " named " + plants.get(i).getName() + " has x-pos: " + plants.get(i).getXPos());
//        }

        layoutCtx.restore();

        DomEventListener domEventListener = domEvent -> {
            double mouseX = domEvent.getEventData().getNumber("event.offsetX");
            int index = -1;

            for (int i = 0; i < vegRowRegion.length; i++) {
                if (vegRowRegion[i][0] <= mouseX && mouseX < vegRowRegion[i][1]) {
                    index = i;
                    break;
                }
            }

            if (index != -1) {
                if (domEvent.getType().equals("mousemove")) {
                    this.layoutCanvas.getStyle().set("cursor", "pointer");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(true);
                    rowDetails.setContent(plants.get(index));
                }
            } else {
                if (domEvent.getType().equals("mousemove")) {
                    this.layoutCanvas.getStyle().set("cursor", "default");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(false);
                }
            }
        };

        layoutCanvas.getElement().addEventListener("mousemove", domEventListener).addEventData("event.offsetX");
        layoutCanvas.getElement().addEventListener("click", domEventListener).addEventData("event.offsetX");
    }

    private void drawHeightGrid() {
        heightCtx.save();

        // draw lines with arrows
        // vertical
        heightCtx.beginPath();
        heightCtx.setLineWidth(1.0);
        heightCtx.setStrokeStyle("#000000");
        heightCtx.moveTo(X_MIN_FIELD, Y_0_POINT_HEIGHT_PLOT);
        heightCtx.lineTo(X_MIN_FIELD, 0);
        heightCtx.lineTo(X_MIN_FIELD - 8, 15);
        heightCtx.moveTo(X_MIN_FIELD, 0);
        heightCtx.lineTo(X_MIN_FIELD + 8, 15);

        // horizontal
        heightCtx.moveTo(X_MIN_FIELD, Y_0_POINT_HEIGHT_PLOT);
        heightCtx.lineTo(X_MAX_FIELD + 40, Y_0_POINT_HEIGHT_PLOT);
        heightCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_HEIGHT_PLOT - 8);
        heightCtx.moveTo(X_MAX_FIELD + 40, Y_0_POINT_HEIGHT_PLOT);
        heightCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_HEIGHT_PLOT + 8);
        heightCtx.closePath();
        heightCtx.stroke();

        heightCtx.setFont("20px Consolas");
        heightCtx.fillText("cm", X_MIN_FIELD + 10, 15);
        heightCtx.fillText("cm", X_MAX_FIELD + 15, Y_0_POINT_HEIGHT_PLOT - 10);


        double x = X_MIN_FIELD;
        double y = Y_0_POINT_HEIGHT_PLOT;

        // draw indicators every 50/10 cm
        heightCtx.beginPath();
        heightCtx.moveTo(x,y);

        // for horizontal indicators every 50 cm
        x += 10 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            heightCtx.moveTo(x, y);
            if (((x - X_MIN_FIELD) / CM_TO_PX_RATIO) % 50 == 0) {
                heightCtx.lineTo(x, Y_0_POINT_HEIGHT_PLOT + 10);
            } else {
                heightCtx.lineTo(x, Y_0_POINT_HEIGHT_PLOT + 5);
            }
            x += 10 * CM_TO_PX_RATIO;
        }

        // for vertical indicators every 10 cm
        x = X_MIN_FIELD;
        y -= 10 * CM_TO_PX_RATIO;
        while (y >= Y_MIN_FIELD) {
            heightCtx.moveTo(x,y);
            if (((Y_0_POINT_HEIGHT_PLOT - y) / CM_TO_PX_RATIO) % 50 == 0) {
                heightCtx.lineTo(X_MIN_FIELD - 7, y);
            } else {
                heightCtx.lineTo(X_MIN_FIELD - 5, y);
            }
            y -= 10 * CM_TO_PX_RATIO;
        }
        heightCtx.closePath();
        heightCtx.stroke();

        // Grid text
        // for the bottom line
        x = X_MIN_FIELD;
        y = Y_0_POINT_HEIGHT_PLOT + 25;
        int textValue = 0;
        heightCtx.setFont("20px Consolas");
        heightCtx.fillText("" + textValue, x - 5, y);

        int xOffset;
        String text;
        int length;
        x += 50 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();
            xOffset = (10 * length + (2 * (length-1))) / 2;

            heightCtx.fillText(text, x - xOffset, y);
            x += 50 * CM_TO_PX_RATIO;
        }

        // for the left line
        x = X_MIN_FIELD - 12;
        y = Y_0_POINT_HEIGHT_PLOT + 7;
        textValue = 0;
        heightCtx.fillText("" + textValue, x - 5, y);

        y -= 50 * CM_TO_PX_RATIO;
        while (y >= Y_MIN_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();
            xOffset = 10 * length + (2 * (length-1)) - 5;

            heightCtx.fillText(text, x - xOffset, y);
            y -= 50 * CM_TO_PX_RATIO;
        }

        heightCtx.restore();
    }

    private void drawHeightProfile(List<Plant> plants) {
        drawHeightGrid();

        heightCtx.save();

        heightCtx.setStrokeStyle("#000000");
        heightCtx.setLineWidth(1.0);
        double x = X_MIN_FIELD;
        double y = Y_0_POINT_HEIGHT_PLOT;
        for (int i = 0; i < plants.size(); i++) {
            Plant p = plants.get(i);
            heightCtx.beginPath();
            heightCtx.setFillStyle("#99ff99");
            heightCtx.rect(x, y - p.getHeightInCM() * CM_TO_PX_RATIO, p.getSpreadDiameterInCM() * CM_TO_PX_RATIO, p.getHeightInCM() * CM_TO_PX_RATIO);
            heightCtx.closePath();
            heightCtx.fill();
            heightCtx.stroke();

            long fontSize = Math.round(Math.min((p.getSpreadDiameterInCM() * CM_TO_PX_RATIO) / 2.0, 25.0));
            heightCtx.setFont(String.format("%spx Consolas", fontSize));
            heightCtx.setFillStyle("#000000");
            heightCtx.fillText(plants.get(i).getName().substring(0,2), x + (p.getSpreadDiameterInCM()*CM_TO_PX_RATIO)/2.0 - (fontSize+fontSize/10.0)/2.0, y - 4.0);

            x += (p.getSpreadDiameterInCM() + solution.getSpaceBetweenRowsInCM()) * CM_TO_PX_RATIO;
        }
        heightCtx.restore();

        DomEventListener domEventListener = domEvent -> {
            double mouseX = domEvent.getEventData().getNumber("event.offsetX");
            double mouseY = domEvent.getEventData().getNumber("event.offsetY");
            int index = -1;

            for (int i = 0; i < vegRowRegion.length; i++) {
                if (vegRowRegion[i][0] <= mouseX && mouseX < vegRowRegion[i][1] && Y_0_POINT_HEIGHT_PLOT >= mouseY && mouseY >= Y_0_POINT_HEIGHT_PLOT - plants.get(i).getHeightInCM()*CM_TO_PX_RATIO) {
                    index = i;
                    break;
                }
            }

            if (index != -1) {
                if (domEvent.getType().equals("mousemove")) {
                    this.heightCanvas.getStyle().set("cursor", "pointer");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(true);
                    rowDetails.setContent(plants.get(index));
                }
            } else {
                if (domEvent.getType().equals("mousemove")) {
                    this.heightCanvas.getStyle().set("cursor", "default");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(false);
                }
            }
        };

        heightCanvas.getElement().addEventListener("mousemove", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
        heightCanvas.getElement().addEventListener("click", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
    }

    private void drawWaterGrid() {
        waterCtx.save();

        // draw lines with arrows
        // vertical
        waterCtx.beginPath();
        waterCtx.setLineWidth(1.0);
        waterCtx.setStrokeStyle("#000000");
        waterCtx.moveTo(X_MIN_FIELD, Y_0_POINT_WATER_PLOT);
        waterCtx.lineTo(X_MIN_FIELD, 0);
        waterCtx.lineTo(X_MIN_FIELD - 8, 15);
        waterCtx.moveTo(X_MIN_FIELD, 0);
        waterCtx.lineTo(X_MIN_FIELD + 8, 15);

        // horizontal
        waterCtx.moveTo(X_MIN_FIELD, Y_0_POINT_WATER_PLOT);
        waterCtx.lineTo(X_MAX_FIELD + 40, Y_0_POINT_WATER_PLOT);
        waterCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_WATER_PLOT - 8);
        waterCtx.moveTo(X_MAX_FIELD + 40, Y_0_POINT_WATER_PLOT);
        waterCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_WATER_PLOT + 8);
        waterCtx.closePath();
        waterCtx.stroke();

        waterCtx.setFont("20px Consolas");
        waterCtx.fillText("indicator", X_MIN_FIELD + 10, 15);
        waterCtx.fillText("cm", X_MAX_FIELD + 15, Y_0_POINT_WATER_PLOT - 10);


        double x = X_MIN_FIELD;
        double y = Y_0_POINT_WATER_PLOT;

        // draw indicators every 50/10 cm
        waterCtx.beginPath();
        waterCtx.moveTo(x,y);

        x += 10 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            waterCtx.moveTo(x, y);
            if (((x - X_MIN_FIELD) / CM_TO_PX_RATIO) % 50 == 0) {
                waterCtx.lineTo(x, Y_0_POINT_WATER_PLOT + 10);
            } else {
                waterCtx.lineTo(x, Y_0_POINT_WATER_PLOT + 5);
            }
            x += 10 * CM_TO_PX_RATIO;
        }

        // for vertical indicators every 20 cm
        x = X_MIN_FIELD;
        y -= 20 * CM_TO_PX_RATIO;
        while (y >= Y_MIN_FIELD) {
            waterCtx.moveTo(x,y);
            waterCtx.lineTo(X_MIN_FIELD - 7, y);
            y -= 20 * CM_TO_PX_RATIO;
        }
        waterCtx.closePath();
        waterCtx.stroke();

        // Grid text
        // for the bottom line
        x = X_MIN_FIELD;
        y = Y_0_POINT_WATER_PLOT + 25;
        int textValue = 0;
        waterCtx.setFont("20px Consolas");
        waterCtx.fillText("" + textValue, x - 5, y);

        int xOffset;
        String text;
        int length;
        x += 50 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();
            xOffset = (10 * length + (2 * (length-1))) / 2;

            waterCtx.fillText(text, x - xOffset, y);
            x += 50 * CM_TO_PX_RATIO;
        }

        // for the left line
        x = X_MIN_FIELD - 12;
        y = Y_0_POINT_WATER_PLOT + 7;
        textValue = 0;
        waterCtx.fillText("" + textValue, x - 5, y);

        y -= 20 * CM_TO_PX_RATIO;
        while (y >= Y_MIN_FIELD) {
            textValue++;
            text = "" + textValue;
            length = text.length();
            xOffset = 10 * length + (2 * (length-1)) - 5;

            waterCtx.fillText(text, x - xOffset, y);
            y -= 20 * CM_TO_PX_RATIO;
        }

        waterCtx.restore();
    }

    private void drawWaterProfile(List<Plant> plants) {
        drawWaterGrid();

        waterCtx.save();

        waterCtx.setStrokeStyle("#000000");
        waterCtx.setLineWidth(1.0);
        double x = X_MIN_FIELD;
        double y = Y_0_POINT_WATER_PLOT;
        for (int i = 0; i < plants.size(); i++) {
            Plant p = plants.get(i);
            waterCtx.beginPath();
            waterCtx.setFillStyle("#0099ff");
            waterCtx.rect(x, y - p.getWaterConsumption() * 20 * CM_TO_PX_RATIO, p.getSpreadDiameterInCM() * CM_TO_PX_RATIO, p.getWaterConsumption() * 20 * CM_TO_PX_RATIO);
            waterCtx.closePath();
            waterCtx.fill();
            waterCtx.stroke();

            long fontSize = Math.round(Math.min((p.getSpreadDiameterInCM() * CM_TO_PX_RATIO) / 2.0, 25.0));
            waterCtx.setFont(String.format("%spx Consolas", fontSize));
            waterCtx.setFillStyle("#000000");
            waterCtx.fillText(plants.get(i).getName().substring(0,2), x + (p.getSpreadDiameterInCM()*CM_TO_PX_RATIO)/2.0 - (fontSize+fontSize/10.0)/2.0, y - 4.0);

            x += (p.getSpreadDiameterInCM() + solution.getSpaceBetweenRowsInCM()) * CM_TO_PX_RATIO;
        }
        waterCtx.restore();

        DomEventListener domEventListener = domEvent -> {
            double mouseX = domEvent.getEventData().getNumber("event.offsetX");
            double mouseY = domEvent.getEventData().getNumber("event.offsetY");
            int index = -1;

            for (int i = 0; i < vegRowRegion.length; i++) {
                if (vegRowRegion[i][0] <= mouseX && mouseX < vegRowRegion[i][1] && Y_0_POINT_WATER_PLOT >= mouseY && mouseY >= Y_0_POINT_WATER_PLOT - plants.get(i).getWaterConsumption()*20*CM_TO_PX_RATIO) {
                    index = i;
                    break;
                }
            }

            if (index != -1) {
                if (domEvent.getType().equals("mousemove")) {
                    this.waterCanvas.getStyle().set("cursor", "pointer");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(true);
                    rowDetails.setContent(plants.get(index));
                }
            } else {
                if (domEvent.getType().equals("mousemove")) {
                    this.waterCanvas.getStyle().set("cursor", "default");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(false);
                }
            }
        };

        waterCanvas.getElement().addEventListener("mousemove", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
        waterCanvas.getElement().addEventListener("click", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
    }

    private void drawBCGrid() {
        bcCtx.save();

        // draw line with arrows
        // horizontal
        bcCtx.beginPath();
        bcCtx.setLineWidth(1.0);
        bcCtx.setStrokeStyle("#000000");
        bcCtx.moveTo(X_MIN_FIELD, Y_0_POINT_BC_PLOT);
        bcCtx.lineTo(X_MAX_FIELD + 40, Y_0_POINT_BC_PLOT);
        bcCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_BC_PLOT - 8);
        bcCtx.moveTo(X_MAX_FIELD + 40, Y_0_POINT_BC_PLOT);
        bcCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_BC_PLOT + 8);
        bcCtx.closePath();
        bcCtx.stroke();

        bcCtx.setFont("20px Consolas");
        bcCtx.fillText("cm", X_MAX_FIELD + 15, Y_0_POINT_BC_PLOT - 10);


        double x = X_MIN_FIELD;
        double y = Y_0_POINT_BC_PLOT;

        // draw indicators every 50/10 cm
        bcCtx.beginPath();
        bcCtx.moveTo(x,y);

        x += 10 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            bcCtx.moveTo(x, y);
            if (((x - X_MIN_FIELD) / CM_TO_PX_RATIO) % 50 == 0) {
                bcCtx.lineTo(x, Y_0_POINT_BC_PLOT + 10);
            } else {
                bcCtx.lineTo(x, Y_0_POINT_BC_PLOT + 5);
            }
            x += 10 * CM_TO_PX_RATIO;
        }
        bcCtx.closePath();
        bcCtx.stroke();

        // Grid text
        // for the bottom line
        x = X_MIN_FIELD;
        y = Y_0_POINT_BC_PLOT + 25;
        int textValue = 0;
        bcCtx.setFont("20px Consolas");
        bcCtx.fillText("" + textValue, x - 5, y);

        int xOffset;
        String text;
        int length;
        x += 50 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();
            xOffset = (10 * length + (2 * (length-1))) / 2;

            bcCtx.fillText(text, x - xOffset, y);
            x += 50 * CM_TO_PX_RATIO;
        }
        bcCtx.restore();
    }

    private void drawGCGrid() {
        gcCtx.save();

        // draw line with arrows
        // horizontal
        gcCtx.beginPath();
        gcCtx.setLineWidth(1.0);
        gcCtx.setStrokeStyle("#000000");
        gcCtx.moveTo(X_MIN_FIELD, Y_0_POINT_GC_PLOT);
        gcCtx.lineTo(X_MAX_FIELD + 40, Y_0_POINT_GC_PLOT);
        gcCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_GC_PLOT - 8);
        gcCtx.moveTo(X_MAX_FIELD + 40, Y_0_POINT_GC_PLOT);
        gcCtx.lineTo(X_MAX_FIELD + 15, Y_0_POINT_GC_PLOT + 8);
        gcCtx.closePath();
        gcCtx.stroke();

        gcCtx.setFont("20px Consolas");
        gcCtx.fillText("cm", X_MAX_FIELD + 15, Y_0_POINT_GC_PLOT - 10);


        double x = X_MIN_FIELD;
        double y = Y_0_POINT_GC_PLOT;

        // draw indicators every 50/10 cm
        gcCtx.beginPath();
        gcCtx.moveTo(x,y);

        x += 10 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            gcCtx.moveTo(x, y);
            if (((x - X_MIN_FIELD) / CM_TO_PX_RATIO) % 50 == 0) {
                gcCtx.lineTo(x, Y_0_POINT_GC_PLOT + 10);
            } else {
                gcCtx.lineTo(x, Y_0_POINT_GC_PLOT + 5);
            }
            x += 10 * CM_TO_PX_RATIO;
        }
        gcCtx.closePath();
        gcCtx.stroke();

        // Grid text
        // for the bottom line
        x = X_MIN_FIELD;
        y = Y_0_POINT_GC_PLOT + 25;
        int textValue = 0;
        gcCtx.setFont("20px Consolas");
        gcCtx.fillText("" + textValue, x - 5, y);

        int xOffset;
        String text;
        int length;
        x += 50 * CM_TO_PX_RATIO;
        while (x <= X_MAX_FIELD) {
            textValue += 50;
            text = "" + textValue;
            length = text.length();
            xOffset = (10 * length + (2 * (length-1))) / 2;

            gcCtx.fillText(text, x - xOffset, y);
            x += 50 * CM_TO_PX_RATIO;
        }
        gcCtx.restore();
    }

    private void drawBCProfile(List<Plant> plants) {
        drawBCGrid();

        bcCtx.save();
        double redLineOffset = 2 * CM_TO_PX_RATIO;
        double x;
        double y = Y_0_POINT_BC_PLOT;
        bcCtx.setLineWidth(1.0);

        // for each plant draw the red lines to its bad companions.
        for (int i = 0; i < plants.size(); i++) {
            Plant p = plants.get(i);
            x = X_MIN_FIELD + xPos[i] * CM_TO_PX_RATIO;
//            x = X_MIN_FIELD + plants.get(i).getXPos() * CM_TO_PX_RATIO;

//            System.out.println("Plant " + p.getName() + " with the id " + p.getId() + " has x-pos: " + x);

            bcCtx.beginPath();
            bcCtx.setStrokeStyle("#ff0000");
            for (int j = 0; j < plants.size(); j++) {
                int finalJ = j;
                if (IntStream.of(p.getBadCompanionIDs()).anyMatch(n -> n == plants.get(finalJ).getId())) {
//                    bcCtx.moveTo(X_MIN_FIELD + plants.get(finalJ).getXPos() * CM_TO_PX_RATIO, y);
                    bcCtx.moveTo(X_MIN_FIELD + xPos[finalJ] * CM_TO_PX_RATIO, y);
                    if (j < i) {
                        bcCtx.lineTo(x - 10 * CM_TO_PX_RATIO, y);
                        bcCtx.lineTo(x - 15 * CM_TO_PX_RATIO, y - 5 * CM_TO_PX_RATIO);
                        bcCtx.moveTo(x - 10 * CM_TO_PX_RATIO, y);
                        bcCtx.lineTo(x - 15 * CM_TO_PX_RATIO, y + 5 * CM_TO_PX_RATIO);
                    }
                    else if (j > i) {
                        bcCtx.lineTo(x + 10 * CM_TO_PX_RATIO, y);
                        bcCtx.lineTo(x + 15 * CM_TO_PX_RATIO, y - 5 * CM_TO_PX_RATIO);
                        bcCtx.moveTo(x + 10 * CM_TO_PX_RATIO, y);
                        bcCtx.lineTo(x + 15 * CM_TO_PX_RATIO, y + 5 * CM_TO_PX_RATIO);
                    }
                }

                if (j < plants.size()-1) {
                    y -= redLineOffset;
                }
            }
            bcCtx.closePath();
            bcCtx.stroke();

            // draw a rectangle filled green where the plant position is and add some descriptive text.
            bcCtx.beginPath();
            bcCtx.setStrokeStyle("#000000");
            bcCtx.setFillStyle("#99ff99");
            if (plants.size() * 2 < 20) {
                y = Y_0_POINT_BC_PLOT - (i + 1) * 20 * CM_TO_PX_RATIO;
            }
            // to give some vertical space between 2 plants
            y -= redLineOffset / 2.0;
            bcCtx.rect(x - 10*CM_TO_PX_RATIO, y, 20*CM_TO_PX_RATIO, Math.max(2 * (plants.size() - 1), 20) * CM_TO_PX_RATIO);
            bcCtx.closePath();
            bcCtx.fill();
            bcCtx.stroke();

            bcCtx.setFont("20px Consolas");
            bcCtx.setFillStyle("#000000");
            bcCtx.fillText(p.getName().substring(0,2), x - 11, y + 10*CM_TO_PX_RATIO + 7);

            // to give some vertical space between 2 plants
            y -= redLineOffset / 2.0;
        }
        bcCtx.restore();

        DomEventListener domEventListener = domEvent -> {
            double mouseX = domEvent.getEventData().getNumber("event.offsetX");
            double mouseY = domEvent.getEventData().getNumber("event.offsetY");
            int index = -1;

            double offset = 10 * CM_TO_PX_RATIO;

            for (int i = 0; i < vegRowRegion.length; i++) {
                double xCoord = vegRowRegion[i][0] + (vegRowRegion[i][1] - vegRowRegion[i][0]) / 2.0;
                double heightPerPlant = (BC_CANVAS_HEIGHT - 80)/Double.parseDouble("" + plants.size());
                double yCoord = Y_0_POINT_BC_PLOT - (i + 1) * heightPerPlant + heightPerPlant/2.0;

                if (xCoord - offset <= mouseX && mouseX <= xCoord + offset && yCoord + offset >= mouseY && mouseY >= yCoord - offset) {
                    index = i;
                    break;
                }
            }

            if (index != -1) {
                if (domEvent.getType().equals("mousemove")) {
                    this.bcCanvas.getStyle().set("cursor", "pointer");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(true);
                    rowDetails.setContent(plants.get(index));
                }
            } else {
                if (domEvent.getType().equals("mousemove")) {
                    this.bcCanvas.getStyle().set("cursor", "default");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(false);
                }
            }
        };

        bcCanvas.getElement().addEventListener("mousemove", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
        bcCanvas.getElement().addEventListener("click", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
    }

    private void drawGCProfile(List<Plant> plants) {
        drawGCGrid();

        gcCtx.save();
        double greenLineOffset = 2 * CM_TO_PX_RATIO;
        double x;
        double y = Y_0_POINT_GC_PLOT /*- 20 * CM_TO_PX_RATIO*/;
//        double yLine = y - 10 * CM_TO_PX_RATIO;
        gcCtx.setLineWidth(1.0);

        // for each plant draw the green lines to its neighbor, if it is a good companion.
        for (int i = 0; i < plants.size(); i++) {
            Plant p = plants.get(i);
            x = X_MIN_FIELD + xPos[i] * CM_TO_PX_RATIO;
//            x = X_MIN_FIELD + p.getXPos() * CM_TO_PX_RATIO;

            gcCtx.beginPath();
            gcCtx.setStrokeStyle("#00ff00");

            int finalI = i;
            double yLine = y - 10 * CM_TO_PX_RATIO;
            if (i > 0) {
                if (IntStream.of(p.getGoodCompanionIDs()).anyMatch(n -> n == plants.get(finalI - 1).getId())) {
                    gcCtx.moveTo(X_MIN_FIELD + xPos[finalI-1] * CM_TO_PX_RATIO, yLine);
//                    gcCtx.moveTo(X_MIN_FIELD + plants.get(finalI-1).getXPos() * CM_TO_PX_RATIO, yLine);
                    double deltaX = x - 10*CM_TO_PX_RATIO;
                    gcCtx.lineTo(deltaX, yLine);
                    gcCtx.lineTo(deltaX - 5 * CM_TO_PX_RATIO, yLine - 5 * CM_TO_PX_RATIO);
                    gcCtx.moveTo(deltaX, yLine);
                    gcCtx.lineTo(deltaX - 5 * CM_TO_PX_RATIO, yLine + 5 * CM_TO_PX_RATIO);
                }
            }
            if (i < plants.size() - 1) {
                if (IntStream.of(p.getGoodCompanionIDs()).anyMatch(n -> n == plants.get(finalI + 1).getId())) {
                    gcCtx.moveTo(X_MIN_FIELD + xPos[finalI+1] * CM_TO_PX_RATIO, yLine);
//                    gcCtx.moveTo(X_MIN_FIELD + plants.get(finalI + 1).getXPos() * CM_TO_PX_RATIO, yLine);
                    double deltaX = x + 10*CM_TO_PX_RATIO;
                    gcCtx.lineTo(deltaX, yLine);
                    gcCtx.lineTo(deltaX + 5 * CM_TO_PX_RATIO, yLine - 5 * CM_TO_PX_RATIO);
                    gcCtx.moveTo(deltaX, yLine);
                    gcCtx.lineTo(deltaX + 5 * CM_TO_PX_RATIO, yLine + 5 * CM_TO_PX_RATIO);
                }
            }

            gcCtx.closePath();
            gcCtx.stroke();

            // draw a rectangle filled green where the plant position is and add some descriptive text.
            gcCtx.beginPath();
            gcCtx.setStrokeStyle("#000000");
            gcCtx.setFillStyle("#99ff99");

            y = Y_0_POINT_GC_PLOT - (i + 1) * 20 * CM_TO_PX_RATIO;

            // to give some vertical space between 2 plants
            y -= greenLineOffset / 2.0;

            gcCtx.rect(x - 10*CM_TO_PX_RATIO, y, 20*CM_TO_PX_RATIO, Math.max(2 * (plants.size() - 1), 20) * CM_TO_PX_RATIO);
            gcCtx.closePath();
            gcCtx.fill();
            gcCtx.stroke();

            gcCtx.setFont("20px Consolas");
            gcCtx.setFillStyle("#000000");
            gcCtx.fillText(p.getName().substring(0,2), x - 11, y + 10*CM_TO_PX_RATIO + 7);

            // to give some vertical space between 2 plants
            y -= greenLineOffset / 2.0;
        }
        gcCtx.restore();

        DomEventListener domEventListener = domEvent -> {
            double mouseX = domEvent.getEventData().getNumber("event.offsetX");
            double mouseY = domEvent.getEventData().getNumber("event.offsetY");
            int index = -1;

            double offset = 10 * CM_TO_PX_RATIO;

            for (int i = 0; i < vegRowRegion.length; i++) {
                double xCoord = vegRowRegion[i][0] + (vegRowRegion[i][1] - vegRowRegion[i][0]) / 2.0;
                double heightPerPlant = (GC_CANVAS_HEIGHT - 80)/Double.parseDouble("" + plants.size());
                double yCoord = Y_0_POINT_GC_PLOT - (i + 1) * heightPerPlant + heightPerPlant/2.0;

                if (xCoord - offset <= mouseX && mouseX <= xCoord + offset && yCoord + offset >= mouseY && mouseY >= yCoord - offset) {
                    index = i;
                    break;
                }
            }

            if (index != -1) {
                if (domEvent.getType().equals("mousemove")) {
                    this.gcCanvas.getStyle().set("cursor", "pointer");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(true);
                    rowDetails.setContent(plants.get(index));
                }
            } else {
                if (domEvent.getType().equals("mousemove")) {
                    this.gcCanvas.getStyle().set("cursor", "default");
                } else if (domEvent.getType().equals("click")) {
                    rowDetailContainer.setVisible(false);
                }
            }
        };

        gcCanvas.getElement().addEventListener("mousemove", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
        gcCanvas.getElement().addEventListener("click", domEventListener).addEventData("event.offsetX").addEventData("event.offsetY");
    }

    public Button getHeightOptBtn() {
        return heightOptBtn;
    }

    public Button getWaterOptBtn() {
        return waterOptBtn;
    }

    public Button getBcOptBtn() {
        return bcOptBtn;
    }

    public Button getGcOptBtn() {
        return gcOptBtn;
    }

    public Button getFindAllOptSolutionsBtn() {
        return findAllOptSolutionsBtn;
    }

    public void setFindAllOptSolutionsBtn(Button findAllOptSolutionsBtn) {
        this.findAllOptSolutionsBtn = findAllOptSolutionsBtn;
    }

//    public Label getSunValueText() {
//        return sunValueText;
//    }
}
