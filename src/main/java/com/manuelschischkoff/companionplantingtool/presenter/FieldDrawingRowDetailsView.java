package com.manuelschischkoff.companionplantingtool.presenter;

import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class FieldDrawingRowDetailsView extends VerticalLayout {

    private H3 title = new H3();

    private HorizontalLayout amountLayout = new HorizontalLayout();
    private Label amountText = new Label("Amount: ");
    private Label amountValue = new Label();

    private VerticalLayout positioningLayout = new VerticalLayout();
    private Label posHeaderText = new Label("Positioning:");
    private Label posDetails = new Label();

    private HorizontalLayout spreadLayout = new HorizontalLayout();
    private Label spreadText = new Label("Spread: ");
    private Label spreadValue = new Label();

    private HorizontalLayout heightLayout = new HorizontalLayout();
    private Label heightText = new Label("Height: ");
    private Label heightValue = new Label();

    private HorizontalLayout wciLayout = new HorizontalLayout();
    private Label wciText = new Label("Water Cost Indicator: ");
    private Label wciValue = new Label();

    private VerticalLayout gcLayout = new VerticalLayout();
    private Label gcText = new Label("Good Companions:");
    private Label gcValue = new Label();

    private VerticalLayout bcLayout = new VerticalLayout();
    private Label bcText = new Label("Bad Companions:");
    private Label bcValue = new Label();

    public FieldDrawingRowDetailsView() {
        Button closeBtn = new Button();
        closeBtn.setText("Close");
        closeBtn.addClickListener(event -> {
           this.getParent().get().setVisible(false);
        });
        add(closeBtn);

        add(title);

        amountText.getStyle().set("font-weight", "bold");
        amountLayout.add(amountText, amountValue);
        add(amountLayout);

        posHeaderText.getStyle().set("font-weight", "bold");
        positioningLayout.add(posHeaderText, posDetails);
        add(positioningLayout);

        spreadText.getStyle().set("font-weight", "bold");
        spreadLayout.add(spreadText, spreadValue);
        add(spreadLayout);

        heightText.getStyle().set("font-weight", "bold");
        heightLayout.add(heightText, heightValue);
        add(heightLayout);

        wciText.getStyle().set("font-weight", "bold");
        wciLayout.add(wciText, wciValue);
        add(wciLayout);

        gcText.getStyle().set("font-weight", "bold");
        gcLayout.add(gcText, gcValue);
        add(gcLayout);

        bcText.getStyle().set("font-weight", "bold");
        bcLayout.add(bcText, bcValue);
        add(bcLayout);
    }

    public void setContent(Plant p) {
        title.setText(p.getName() + "-row");

        amountValue.setText("" + p.getAmount());

        posDetails.setText("Row-start at " + p.getXPos() + " cm from W and " + p.getYPos() + " cm from N. In-Row-Spacing is " + p.getSpacing() + " cm.");

        spreadValue.setText(p.getSpreadDiameterInCM() + " cm");

        heightValue.setText(p.getHeightInCM() + " cm");

        wciValue.setText("" + p.getWaterConsumption());

        String text = getCompanionNames(p.getGoodCompanionIDs());
        gcValue.setText(text);

        text = getCompanionNames(p.getBadCompanionIDs());
        bcValue.setText(text);
    }

    private String getCompanionNames(int[] ids) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ids.length; i++) {
            sb.append(PlantService.getInstance().find(ids[i]).getName());
            if (i < ids.length-1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

}
