package com.manuelschischkoff.companionplantingtool.presenter;

import com.google.gson.Gson;
import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantService;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.PlantEditorDialog;
import com.manuelschischkoff.companionplantingtool.model.utils.MessageBean;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PlantSelectionView extends VerticalLayout {

    // Toolbar
    private TextField filterText = new TextField();
    private Button restoreDefaultPlantlistBtn;

    // Plant selection
    private Grid<Plant> availablePlantsGrid = new Grid<>();
    private Grid<Plant> selectedPlantsGrid = new Grid<>();
    private List<Plant> selectedPlants = new ArrayList<>();

    private MainController controller;

    public PlantSelectionView(MainController controller) {
        this.controller = controller;

        setupHeader();
        setupToolbar();
        setupPlantList();

        updateList();
    }

    private void setupHeader() {
        add(new H2("Select the vegetables you want to grow"));
    }

    private void setupToolbar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setValueChangeMode(ValueChangeMode.EAGER);
        filterText.addValueChangeListener(event -> updateList());
        filterText.addKeyUpListener(Key.ENTER, keyUpEvent -> addPlantselectionToSelectedGrid(controller.findAll(filterText.getValue())));

        Button clearFilterTextBtn = new Button(new Icon(VaadinIcon.CLOSE_CIRCLE));
        clearFilterTextBtn.addClickListener(click -> filterText.clear());

        HorizontalLayout filtering = new HorizontalLayout(filterText, clearFilterTextBtn);

        Button openLocalPlantList = new Button("Open list", click -> openLocalPlantList());

        Button editPlantList = new Button("Edit list", click -> openEditorOverlay());

        restoreDefaultPlantlistBtn = new Button("Restore default list", click -> restoreDefaultList());
        restoreDefaultPlantlistBtn.setEnabled(false);

        HorizontalLayout plantListActionBtns = new HorizontalLayout(openLocalPlantList, editPlantList, restoreDefaultPlantlistBtn);

        HorizontalLayout toolbar = new HorizontalLayout(filtering, plantListActionBtns);

        add(toolbar);
    }

    private void openLocalPlantList() {
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        Button browse = new Button("Browse ...");
        upload.setUploadButton(browse);
        upload.setAcceptedFileTypes(".json");
        upload.addStartedListener(startedEvent -> System.out.println("Started uploading: " + startedEvent.getFileName()));

        upload.addProgressListener(progressUpdateEvent -> System.out.println(String.format("Reading %s bytes.", progressUpdateEvent.getReadBytes())));

        upload.addFailedListener(failedEvent -> {
            System.out.println("Failed uploading: " + failedEvent.getReason());
            MessageBean.showMessage("This file is not of the form which matches the Plant Object. " +
                    "Please choose a file which was saved with the Companion Planting Tool, or is correctly structured.", 10);
        });

        Dialog uploadDialog = new Dialog();

        upload.addSucceededListener(succeededEvent -> {
            Gson gson = new Gson();
            Plant[] ps;
            ps = gson.fromJson(buffer.getFileData().getOutputBuffer().toString(), Plant[].class);
            if (ps != null) {
                System.out.println("Succeeded in uploading: " + succeededEvent.getFileName());
                System.out.println("Fetched " + ps.length + " plants from uploaded File.");
                controller.getPlantService().loadPlantListFromClient(ps);
                restoreDefaultPlantlistBtn.setEnabled(true);
                uploadDialog.close();
            } else {
                MessageBean.showMessage("This file is not correctly structured. " +
                        "Please choose a file which was saved with the Companion Planting Tool, or is correctly structured.", 10);
            }
        });

        uploadDialog.setCloseOnOutsideClick(false);
        VerticalLayout diagContainer = new VerticalLayout();
        diagContainer.setAlignItems(Alignment.CENTER);
        diagContainer.add(new H4("Upload"));
        diagContainer.add(new Label("Choose a json file, created and downloaded with the Companion Planting Tool, from your device."));
        Button close = new Button("Close", click -> uploadDialog.close());
        HorizontalLayout btnContainer = new HorizontalLayout();
        btnContainer.setAlignItems(Alignment.CENTER);
        btnContainer.add(upload, close);
        diagContainer.add(btnContainer);
        uploadDialog.add(diagContainer);
        uploadDialog.open();
    }

    private void openEditorOverlay() {
        List<Plant> clonedPlants = new ArrayList<>();
        PlantService pService =  PlantService.getInstance();
        List<Plant> ps = pService.findAll();
        for (Plant p : ps) {
            try {
                clonedPlants.add(p.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        if (clonedPlants.size() > 0) {
            PlantEditorDialog dialog = new PlantEditorDialog(controller,clonedPlants);
            dialog.open();
        }

        restoreDefaultPlantlistBtn.setEnabled(true);
    }

    private void restoreDefaultList() {
        clearSelectedGrid();
        controller.removeSolutionView();
        controller.restoreDefaultPlantList();
        restoreDefaultPlantlistBtn.setEnabled(false);
    }

    private void setupPlantList() {
        availablePlantsGrid.setWidth("40vw");
        availablePlantsGrid.setHeight("60vh");
        availablePlantsGrid.addColumn(Plant::getName)
                .setComparator(
                        (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName())
                )
                .setHeader("Name");
        availablePlantsGrid.addColumn(Plant::getGerName)
                .setComparator(
                        (p1, p2) -> p1.getGerName().compareToIgnoreCase(p2.getGerName())
                ).setHeader("German name");
//        availablePlantsGrid.setSelectionMode(Grid.SelectionMode.NONE);

        availablePlantsGrid.setItemDetailsRenderer(TemplateRenderer.<Plant> of (
                "<div style='padding: 10px; width: 100%; box-sizing: border-box;'>"
                        + "<div><b>Spread:</b> [[item.spread]] cm; "
                        + "<b>Height:</b> [[item.height]] cm; "
                        + "<b>Water Cost Indicator:</b> [[item.water]];</div>"
                        + "<div><b>Good Companions:</b></div>"
                        + "<div>[[item.GC]];</div>"
                        + "<div><b>Bad Companions:</b></div>"
                        + "<div>[[item.BC]];</div>"
                        + "</div>"
        )
        .withProperty("spread", Plant::getSpreadDiameterInCM)
        .withProperty("height", Plant::getHeightInCM)
        .withProperty("water", Plant::getWaterConsumption)
        .withProperty("GC", plant -> {
            int[] ids = plant.getGoodCompanionIDs();
            return getCompanionNames(ids);
        })
        .withProperty("BC", plant -> {
            int[] ids = plant.getBadCompanionIDs();
            return getCompanionNames(ids);
        })
        .withEventHandler("handleClick", plant -> availablePlantsGrid.getDataProvider().refreshItem(plant)));

        selectedPlantsGrid.setWidth("40vw");
        selectedPlantsGrid.setHeight("60vh");
        selectedPlantsGrid.addColumn(Plant::getName).setHeader("Name");
        selectedPlantsGrid.addColumn(Plant::getGerName).setHeader("German name");

        Button selectBtn = new Button(new Icon(VaadinIcon.PLUS));
        selectBtn.addClickListener(click -> addPlantselectionToSelectedGrid(availablePlantsGrid.getSelectedItems()));

        Button removeBtn = new Button(new Icon(VaadinIcon.MINUS));
        removeBtn.addClickListener(click -> removePlantselectionFromSelectedGrid(selectedPlantsGrid.getSelectedItems()));

        Button calculateBtn = new Button(new Icon(VaadinIcon.COMPILE));
        calculateBtn.addClickListener(click -> {
            List<Plant> uniqueCheck = new ArrayList<>();
            for (Plant p : selectedPlants) {
                if (!uniqueCheck.contains(p)) {
                    uniqueCheck.add(p);
                }
            }

            if (uniqueCheck.size() < 2) {
                MessageBean.showMessage("Please select at least 2 different vegetable plants.");
                return;
            }
            calculateFieldLayouts();
        });

        Button clearSelectedPlantsGrid = new Button("");
        clearSelectedPlantsGrid.setIcon(new Icon(VaadinIcon.BAN));
        clearSelectedPlantsGrid.addClickListener(click -> clearSelectedGrid());

        VerticalLayout selectionBtns = new VerticalLayout();
        selectionBtns.add(selectBtn, removeBtn, calculateBtn, clearSelectedPlantsGrid);
        selectionBtns.setWidth("5vw");

        HorizontalLayout plantListSelector = new HorizontalLayout();
        plantListSelector.add(availablePlantsGrid, selectionBtns, selectedPlantsGrid);

        add(plantListSelector);
    }

    private String getCompanionNames(int[] ids) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ids.length; i++) {
            sb.append(PlantService.getInstance().find(ids[i]).getName());
            if (i < ids.length-1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    private void addPlantselectionToSelectedGrid(Collection<Plant> currentSelectedPlants) {
        selectedPlants.addAll(currentSelectedPlants);
        selectedPlantsGrid.setItems(selectedPlants);
    }

    private void removePlantselectionFromSelectedGrid(Collection<Plant> currentSelectedPlants) {
        for (Plant p : currentSelectedPlants) {
            selectedPlants.remove(p);
        }
        selectedPlantsGrid.setItems(selectedPlants);
    }

    private void calculateFieldLayouts() {
        controller.stopCalculations();
        controller.calculateFieldLayout(selectedPlants);
    }

    private void clearSelectedGrid() {
        selectedPlants = new ArrayList<>();
        selectedPlantsGrid.setItems(selectedPlants);
    }

    private void updateList() {
        availablePlantsGrid.setItems(controller.findAll(filterText.getValue()));
    }

    public void updateListAfterItemChanges() {
        filterText.clear();
        availablePlantsGrid.setItems(controller.findAll(filterText.getValue()));
        this.getUI().get().push();
    }

    public void deactivateRestoreBtn() {
        this.restoreDefaultPlantlistBtn.setEnabled(false);
    }
}
