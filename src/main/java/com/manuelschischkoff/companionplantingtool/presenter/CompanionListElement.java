package com.manuelschischkoff.companionplantingtool.presenter;

import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.presenter.dialogs.PlantEditorDialog;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class CompanionListElement extends HorizontalLayout {

    private Button deletePlantBan;
    private Plant p;
    private PlantEditorDialog ped;
    private String bgColor;

    public CompanionListElement(Plant plant, String optionalBackgroundColor) {
        this.p = plant;
        this.bgColor = optionalBackgroundColor;
        getStyle().set("border", "1px solid#e6e6e6");
        getStyle().set("border-radius", "5px");
        getStyle().set("margin-left", "0px");
        getStyle().set("padding-left", "5px");
        getStyle().set("padding-right", "5px");
        getStyle().set("background-color", bgColor);
        setAlignItems(Alignment.CENTER);
        add(new Label(p.getName()));
        deletePlantBan = new Button(new Icon(VaadinIcon.CLOSE_CIRCLE));
        deletePlantBan.addClickListener(click -> ped.removePlantFromCompanionList(this.p));
        deletePlantBan.setVisible(false);
        add(deletePlantBan);
    }

    public void enableDeletion(PlantEditorDialog callingDialog) {
        this.ped = callingDialog;
        deletePlantBan.setVisible(true);
    }

    public void  disableDeletion() {
        deletePlantBan.setVisible(false);
    }

    public Plant getPlant(){
        return p;
    }
}
