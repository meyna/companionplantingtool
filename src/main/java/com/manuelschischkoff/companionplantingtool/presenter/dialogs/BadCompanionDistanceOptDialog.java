package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class BadCompanionDistanceOptDialog extends Dialog {

    private MainController controller;

    public BadCompanionDistanceOptDialog(MainController controller) {
        this.controller = controller;

        setWidth("50vw");

        VerticalLayout dialogLayout = new VerticalLayout();

        H3 header = new H3("Maximize the distances between Bad Companions.");
        header.getStyle().set("margin", "auto");
        dialogLayout.add(header);
        dialogLayout.add(new Label("With this optimization, the distance between each Bad Companion will be evaluated." +
                " The layout in which the average distance between Bad Companions is the maximum, will be selected."));

        HorizontalLayout buttonsContainer = new HorizontalLayout();
        buttonsContainer.getStyle().set("margin", "auto");
        buttonsContainer.getStyle().set("padding-top", "2vh");

        Button cancelBtn = new Button("Cancel");
        Button okBtn = new Button("Ok");

        cancelBtn.addClickListener(event -> this.close());

        okBtn.addClickListener(event -> {
            this.controller.setShowFilterBtnBCDistances(false);
            this.close();
            this.controller.optimizeBCDist();
        });

        buttonsContainer.add(cancelBtn, okBtn);
        dialogLayout.add(buttonsContainer);
        add(dialogLayout);
    }

}
