package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class GCOptDialog extends Dialog {

    private MainController controller;

    public GCOptDialog(MainController controller) {
        this.controller = controller;

        setWidth("50vw");

        VerticalLayout dialogLayout = new VerticalLayout();

        H3 header = new H3("Ensure that all neighbours have a Good Companion relationship");
        header.getStyle().set("margin", "auto");
        dialogLayout.add(header);
        dialogLayout.add(new Label("With this filter it will be ensured, that either a neighbour of a plant A occurs in plant A its" +
                " Good Companion List, or that Plant A is part of its neighbour Good Companion List." +
                " This will hold for all rows on the field."));

        HorizontalLayout buttonsContainer = new HorizontalLayout();
        buttonsContainer.getStyle().set("margin", "auto");
        buttonsContainer.getStyle().set("padding-top", "2vh");

        Button cancelBtn = new Button("Cancel");
        Button okBtn = new Button("Ok");

        cancelBtn.addClickListener(event -> this.close());

        okBtn.addClickListener(event -> {
            this.controller.setShowFilterBtnGCNH(false);
            this.close();
            this.controller.ensureGCNH();
        });

        buttonsContainer.add(cancelBtn, okBtn);
        dialogLayout.add(buttonsContainer);
        add(dialogLayout);
    }

}
