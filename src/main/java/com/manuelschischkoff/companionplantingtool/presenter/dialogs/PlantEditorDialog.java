package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantService;
import com.manuelschischkoff.companionplantingtool.presenter.CompanionListElement;
import com.manuelschischkoff.companionplantingtool.model.utils.MessageBean;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.shared.Registration;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PlantEditorDialog extends Dialog {

    private final MainController controller;
    private Grid<Plant> availablePlantsGrid = new Grid<>();
    private TextField filterText = new TextField();
    private List<Plant> clonedPlants = new ArrayList<>();
    private VerticalLayout form = new VerticalLayout();
    private Button addNewPlantToListBtn;
    private Button deletePlantFromListBtn;
    private Registration gridRegistration;
    private List<Plant> sortedList = new ArrayList<>();
    private int sortClicked = 0;
    private Button saveNewPlantListToClientDeviceBtn;
    private Button closePlantEditorBtn;
    private List<Plant> changedPlants;
    private List<Plant> deletedPlants;
    private Registration nameRegistration;
    private Registration spreadRegistration;
    private Registration heightRegistration;
    private Registration wciRegistration;
    private boolean currentlyInputListenerAttachedToFormElements;
    private TextField nameOfFileField;

    public PlantEditorDialog(MainController controller, List<Plant> clonedPlants) {
        this.controller = controller;
        this.clonedPlants.addAll(clonedPlants);
        this.sortedList.addAll(clonedPlants);
        this.changedPlants = new ArrayList<>();
        this.deletedPlants = new ArrayList<>();
        this.currentlyInputListenerAttachedToFormElements = false;

        setCloseOnEsc(false);
        setCloseOnOutsideClick(false);

        setUpViews();
    }

    private void setUpViews() {
        setHeight("90vh");
        setWidth("70vw");

        VerticalLayout container = new VerticalLayout();
        container.setAlignItems(FlexComponent.Alignment.CENTER);
        add(container);

        container.add(new H3("Plant Editor"));

        filterText.setPlaceholder("Filter by name...");
        filterText.setValueChangeMode(ValueChangeMode.EAGER);
        filterText.addValueChangeListener(event -> {
            List<Plant> filteredList = findPlants(filterText.getValue());
            if (currentlyInAddToCompanionListMode) {
                filteredList.removeAll(temporaryPlantList);
                availablePlantsGrid.setItems(filteredList);
                sortedList = filteredList;
            } else if (changesWereMade) {
                askToSaveChangesMadeOnCurrentSelectedPlant();
            } else {
                availablePlantsGrid.setItems(filteredList);
                sortedList = filteredList;
            }
        });

        availablePlantsGrid.addSortListener(event -> {
            sortClicked++;
            sortClicked = sortClicked % 3;
        });

        filterText.addKeyUpListener(Key.ENTER, keyUpEvent -> {
            sortList();
            availablePlantsGrid.select(sortedList.get(0));
            if (currentlyInAddToCompanionListMode) {
                filterText.clear();
            }
        });

        Button clearFilterTextBtn = new Button(new Icon(VaadinIcon.CLOSE_CIRCLE));
        clearFilterTextBtn.addClickListener(click -> filterText.clear());

        addNewPlantToListBtn = new Button("New Plant");
        addNewPlantToListBtn.addClickListener(click -> {
            if (changesWereMade) {
                askToSaveChangesMadeOnCurrentSelectedPlant();
            } else {
                availablePlantsGrid.deselectAll();
                form.removeAll();
                setupForm();
                name.setInvalid(true);
                spread.setInvalid(true);
                height.setInvalid(true);
                wci.setInvalid(true);
                name.focus();
                switchListenerOfFormElements();
                currentlyEditingANewPlant = true;
                applyChangesToPlantBtn.setVisible(false);
                saveNewPlantBtn.setVisible(true);
                deletePlantFromListBtn.setEnabled(false);
                disableCompanionEditing();
                checkForValidForm();
            }
        });

        deletePlantFromListBtn = new Button("Delete Plant");
        deletePlantFromListBtn.addClickListener(event -> {
            disableCompanionEditing();
            Dialog confirmDialog = new Dialog();
            confirmDialog.setCloseOnOutsideClick(false);
            VerticalLayout content = new VerticalLayout();
            content.setAlignItems(FlexComponent.Alignment.CENTER);
            confirmDialog.add(content);
            content.add(new Label("Confirm deletion of plant:"));
            content.add(new H3(currentlySelectedPlantInForm.getName()));
            Button confirm = new Button("Confirm");
            confirm.addClickListener(event1 -> {
               deletedPlants.add(currentlySelectedPlantInForm);
               changedPlants.remove(currentlySelectedPlantInForm);
               removeDeletedPlantFromAllCompanionsLists(currentlySelectedPlantInForm.getId());
               clonedPlants.remove(currentlySelectedPlantInForm);
               availablePlantsGrid.getDataProvider().refreshAll();
                form.removeAll();
                setupForm();
                currentlyEditingANewPlant = true;
                applyChangesToPlantBtn.setVisible(false);
                saveNewPlantBtn.setVisible(true);
                deletePlantFromListBtn.setEnabled(false);
                saveNewPlantListToClientDeviceBtn.setEnabled(true);
                nameOfFileField.setEnabled(true);
               confirmDialog.close();
            });
            Button cancel = new Button("Cancel");
            cancel.addClickListener(event1 -> {
               confirmDialog.close();
            });
            HorizontalLayout btnContainer = new HorizontalLayout();
            btnContainer.add(confirm, cancel);
            content.add(btnContainer);
            confirmDialog.open();
        });
        deletePlantFromListBtn.setEnabled(false);

        HorizontalLayout plantListHeader = new HorizontalLayout();
        plantListHeader.add(filterText, clearFilterTextBtn, addNewPlantToListBtn, deletePlantFromListBtn);

        availablePlantsGrid.addColumn(Plant::getName).setComparator(
                        (p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName())
                ).setHeader("Name");
        availablePlantsGrid.addColumn(Plant::getGerName).setComparator(
                        (p1, p2) -> p1.getGerName().compareToIgnoreCase(p2.getGerName())
                ).setHeader("German name");
        changeGridselectionModeToDefault();
        availablePlantsGrid.setItems(clonedPlants);

        VerticalLayout plantListLayout = new VerticalLayout();
        plantListLayout.add(plantListHeader, availablePlantsGrid);

        HorizontalLayout horizontalSplitContainer = new HorizontalLayout();
        plantListLayout.setWidth("30vw");
        form.setWidth("30vw");
        form.getStyle().set("margin-top", "6vh");
        horizontalSplitContainer.add(plantListLayout, form);
        container.add(horizontalSplitContainer);

        nameOfFileField = new TextField();
        nameOfFileField.setPlaceholder("myNewPlantList");
        nameOfFileField.setEnabled(false);

        saveNewPlantListToClientDeviceBtn = new Button("Save");
        saveNewPlantListToClientDeviceBtn.addClickListener(event -> {
            saveNewPlantListToClientDevice(nameOfFileField.getValue());
        });
        saveNewPlantListToClientDeviceBtn.setEnabled(false);

        closePlantEditorBtn = new Button("Close Plant Editor");
        closePlantEditorBtn.addClickListener(event -> {
           Dialog youSureQuestion = new Dialog();
           youSureQuestion.setCloseOnOutsideClick(false);
           VerticalLayout dialogContainer = new VerticalLayout();
           dialogContainer.setAlignItems(FlexComponent.Alignment.CENTER);
           dialogContainer.add(new Label("Are you sure to exit the plant editor without saving the changes?"));

           Button yes = new Button("Yes");
           yes.addClickListener(event1 -> {
               youSureQuestion.close();
//               controller.restoreFormerPlantList();

               if (!plantService.isDefaultListChanged()) {
                   controller.deactivateRestoreListBtn();
               }

               this.close();
           });

           Button no = new Button("No");
           no.addClickListener(event1 -> {
               youSureQuestion.close();
           });

           HorizontalLayout btnContainer = new HorizontalLayout();
           btnContainer.add(yes, no);

           dialogContainer.add(btnContainer);
           youSureQuestion.add(dialogContainer);

           if (saveNewPlantListToClientDeviceBtn.isEnabled() || applyChangesToPlantBtn.isEnabled() || saveNewPlantBtn.isEnabled() ||
                   currentlyEditingANewPlant && (!name.isInvalid() || !spread.isInvalid() || !height.isInvalid() || !wci.isInvalid() || gc.size()>0)) {
               youSureQuestion.open();
           } else {
               this.close();
           }
        });

        HorizontalLayout mainBtnContainer = new HorizontalLayout();
        mainBtnContainer.add(new Label("Filename:"), nameOfFileField, saveNewPlantListToClientDeviceBtn, closePlantEditorBtn);
        mainBtnContainer.setAlignItems(FlexComponent.Alignment.CENTER);
        container.add(mainBtnContainer);

        setupForm();
    }

    private void removeDeletedPlantFromAllCompanionsLists(int id) {
        for (Plant p : clonedPlants) {
            List<Integer> currentGoodCompanions = Arrays.stream(p.getGoodCompanionIDs()).boxed().collect(Collectors.toList());
            if (currentGoodCompanions.contains(id)) {
                currentGoodCompanions.remove(Integer.valueOf(id));
                p.setGoodCompanionIDs(currentGoodCompanions.stream().mapToInt(i -> i).toArray());
            }

            List<Integer> currentBadCompanions = Arrays.stream(p.getBadCompanionIDs()).boxed().collect(Collectors.toList());
            if (currentBadCompanions.contains(id)) {
                currentBadCompanions.remove(Integer.valueOf(id));
                p.setBadCompanionIDs(currentBadCompanions.stream().mapToInt(i -> i).toArray());
            }
        }
    }

    // duplicate filter fct. to work on already changed plants
    private List<Plant> findPlants(String stringFilter) {
        ArrayList<Plant> arrayList = new ArrayList<>();
        for (Plant plant : clonedPlants) {
            try {
                boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
                        || plant.toSearchString().toLowerCase().contains(stringFilter.toLowerCase());
                if (passesFilter) {
                    arrayList.add(plant.clone());
                }
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(PlantService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Collections.sort(arrayList, new Comparator<Plant>() {

            @Override
            public int compare(Plant o1, Plant o2) {
                return (int) (o2.getId() - o1.getId());
            }
        });
        return arrayList;
    }

    private Plant findPlant(int id) {
        for (Plant p : clonedPlants) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    private void sortList() {
        if (sortClicked == 1) {
            sortedList.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
        } else if (sortClicked == 2){
            sortedList.sort((o1, o2) -> o2.getName().compareToIgnoreCase(o1.getName()));
        }
    }

    // FOR THE FORM
    private TextField name;
    private TextField spread;
    private TextField height;
    private TextField wci;
    private List<Plant> gc;
    private List<Plant> bc;
    private List<CompanionListElement> gcElements = new ArrayList<>();
    private List<CompanionListElement> bcElements = new ArrayList<>();
    private PlantService plantService;
    private HorizontalLayout gcContainer;
    private HorizontalLayout bcContainer;
    private Button addGcPlantBtn;
    private Button stopAddModeOfGcBtn;
    private Button addBcPlantBtn;
    private Button stopAddModeOfBcBtn;
    private Button saveNewPlantBtn;
    private Button applyChangesToPlantBtn;
    private boolean currentlyEditingGCList = false;
    private boolean currentlyEditingBCList = false;
    private Button editBcsBtn;
    private Button doneEditingBCsBtn;
    private Button editGcsBtn;
    private Button doneEditingGCsBtn;
    private boolean changesWereMade = false;
    private boolean currentlyInAddToCompanionListMode = false;
    private boolean currentlyEditingANewPlant = true;
    private Plant currentlySelectedPlantInForm;


    private void setupForm() {
        plantService = PlantService.getInstance();

        name = new TextField();
        spread = new TextField();
        height= new TextField();
        wci = new TextField();

        form.add(new H4("Here you can edit the plant details:"));

        form.add(new Label("Name:"));
        name.setRequired(true);
        name.getStyle().set("margin-top", "0.1vh");
        nameRegistration = addInputListenerToFormElement(name, nameRegistration);
        form.add(name);

        form.add(new Label("Spread in cm:"));
        spread.setRequired(true);
        spread.setPattern("^[1-9][0-9]{0,3}$"); // not starting with 0, followed by 0 up to 3 more digits of the kind 0-9
        spread.setPreventInvalidInput(true);
        spread.getStyle().set("margin-top", "0.1vh");
        spreadRegistration = addInputListenerToFormElement(spread, spreadRegistration);
        form.add(spread);

        form.add(new Label("Height in cm:"));
        height.setRequired(true);
        height.setPattern("^[1-9][0-9]{0,3}$");
        height.setPreventInvalidInput(true);
        height.getStyle().set("margin-top", "0.1vh");
        heightRegistration = addInputListenerToFormElement(height, heightRegistration);
        form.add(height);

        form.add(new Label("Water Cost Indicator [1-5]:"));
        wci.setRequired(true);
        wci.setPattern("^[1-5]$");
        wci.setPreventInvalidInput(true);
        wci.getStyle().set("margin-top", "0.1vh");
        wciRegistration = addInputListenerToFormElement(wci, wciRegistration);
        form.add(wci);

        currentlyInputListenerAttachedToFormElements = true;

        HorizontalLayout gcHeader = new HorizontalLayout();
        gcHeader.setAlignItems(FlexComponent.Alignment.CENTER);
        gcHeader.add(new Label("Good Companions:"));
        editGcsBtn = new Button("Edit");
        doneEditingGCsBtn = new Button("Done");
        editGcsBtn.addClickListener(event -> {
            if (currentlyEditingBCList) {
                disableCompanionEditing();
            }
            currentlyEditingGCList = true;
            enableDeletion(gcElements);
            gcContainer.add(addGcPlantBtn);
            editGcsBtn.setVisible(false);
            doneEditingGCsBtn.setVisible(true);
        });
        doneEditingGCsBtn.addClickListener(event -> {
            disableCompanionEditing();
        });
        doneEditingGCsBtn.setVisible(false);
        gcHeader.add(editGcsBtn,doneEditingGCsBtn);
        form.add(gcHeader);
        gcContainer = new HorizontalLayout();
        gcContainer.getStyle().set("margin-top", "0.1vh");
        gcContainer.getStyle().set("display", "flex");
        gcContainer.getStyle().set("flex-wrap", "wrap");
        form.add(gcContainer);
        addGcPlantBtn = new Button();
        addGcPlantBtn.setIcon(new Icon(VaadinIcon.PLUS));
        addGcPlantBtn.addClickListener(event -> {
            currentlyInAddToCompanionListMode = true;
            deletePlantFromListBtn.setEnabled(false);
            changeGridselectionModelToAdd();
            gcContainer.remove(addGcPlantBtn);
            gcContainer.add(stopAddModeOfGcBtn);
            MessageBean.showMessage("You can now choose from the list on the left side.");
        });
        stopAddModeOfGcBtn = new Button();
        stopAddModeOfGcBtn.setIcon(new Icon(VaadinIcon.BAN));
        stopAddModeOfGcBtn.addClickListener(event -> {
            changeGridselectionModeToDefault();
            gcContainer.remove(stopAddModeOfGcBtn);
            gcContainer.add(addGcPlantBtn);
            currentlyInAddToCompanionListMode = false;
            checkForValidForm();
        });
        gc = new ArrayList<>();

        HorizontalLayout bcHeader = new HorizontalLayout();
        bcHeader.setAlignItems(FlexComponent.Alignment.CENTER);
        bcHeader.add(new Label("Bad Companions:"));
        editBcsBtn = new Button("Edit");
        doneEditingBCsBtn = new Button("Done");
        editBcsBtn.addClickListener(event -> {
            if (currentlyEditingGCList) {
                disableCompanionEditing();
            }
            currentlyEditingBCList = true;
            enableDeletion(bcElements);
            bcContainer.add(addBcPlantBtn);
            editBcsBtn.setVisible(false);
            doneEditingBCsBtn.setVisible(true);
        });
        doneEditingBCsBtn.addClickListener(event -> {
            disableCompanionEditing();
        });
        doneEditingBCsBtn.setVisible(false);
        bcHeader.add(editBcsBtn,doneEditingBCsBtn);
        form.add(bcHeader);
        bcContainer = new HorizontalLayout();
        bcContainer.getStyle().set("margin-top", "0.1vh");
        bcContainer.getStyle().set("display", "flex");
        bcContainer.getStyle().set("flex-wrap", "wrap");
        form.add(bcContainer);
        addBcPlantBtn = new Button();
        addBcPlantBtn.setIcon(new Icon(VaadinIcon.PLUS));
        addBcPlantBtn.addClickListener(event -> {
            currentlyInAddToCompanionListMode = true;
            deletePlantFromListBtn.setEnabled(false);
            changeGridselectionModelToAdd();
            bcContainer.remove(addBcPlantBtn);
            bcContainer.add(stopAddModeOfBcBtn);
            MessageBean.showMessage("You can now choose from the list on the left side.");
        });
        stopAddModeOfBcBtn = new Button();
        stopAddModeOfBcBtn.setIcon(new Icon(VaadinIcon.BAN));
        stopAddModeOfBcBtn.addClickListener(event -> {
            changeGridselectionModeToDefault();
            bcContainer.remove(stopAddModeOfBcBtn);
            bcContainer.add(addBcPlantBtn);
            currentlyInAddToCompanionListMode = false;
            checkForValidForm();
        });
        bc = new ArrayList<>();

        saveNewPlantBtn = new Button("Save New Plant");
        saveNewPlantBtn.setEnabled(false);
        saveNewPlantBtn.addClickListener(event -> {
            disableCompanionEditing();
            askToSaveChangesMadeOnCurrentSelectedPlant();
            saveNewPlantBtn.setEnabled(false);
            deletePlantFromListBtn.setEnabled(true);
        });
        form.add(saveNewPlantBtn);

        applyChangesToPlantBtn = new Button("Apply Changes");
        applyChangesToPlantBtn.setEnabled(false);
        applyChangesToPlantBtn.addClickListener(event -> {
            disableCompanionEditing();
            askToSaveChangesMadeOnCurrentSelectedPlant();
            applyChangesToPlantBtn.setEnabled(false);
            deletePlantFromListBtn.setEnabled(true);
        });
        form.add(applyChangesToPlantBtn);
        applyChangesToPlantBtn.setVisible(false);
    }

    private void disableCompanionEditing() {
        disableDeletion(gcElements);
        disableDeletion(bcElements);
        gcContainer.remove(addGcPlantBtn);
        bcContainer.remove(addBcPlantBtn);
        gcContainer.remove(stopAddModeOfGcBtn);
        bcContainer.remove(stopAddModeOfBcBtn);
        doneEditingGCsBtn.setVisible(false);
        doneEditingBCsBtn.setVisible(false);
        editGcsBtn.setVisible(true);
        editBcsBtn.setVisible(true);
        currentlyEditingGCList = false;
        currentlyEditingBCList = false;
        currentlyInAddToCompanionListMode = false;
//        changesWereMade = true;
        changeGridselectionModeToDefault();
        checkForValidForm();
    }

    private void switchListenerOfFormElements() {
        if (currentlyInputListenerAttachedToFormElements) {
            nameRegistration = addValueChangeListenerToFormElement(name, nameRegistration);
            spreadRegistration = addValueChangeListenerToFormElement(spread, spreadRegistration);
            heightRegistration = addValueChangeListenerToFormElement(height, heightRegistration);
            wciRegistration = addValueChangeListenerToFormElement(wci, wciRegistration);
            currentlyInputListenerAttachedToFormElements = false;
        } else {
            nameRegistration = addInputListenerToFormElement(name, nameRegistration);
            spreadRegistration = addInputListenerToFormElement(spread, spreadRegistration);
            heightRegistration = addInputListenerToFormElement(height, heightRegistration);
            wciRegistration = addInputListenerToFormElement(wci, wciRegistration);
            currentlyInputListenerAttachedToFormElements = true;
        }
    }

    private Registration addInputListenerToFormElement(TextField textField, Registration registration) {
        if (registration != null) {
            registration.remove();
        }
        return  textField.addInputListener(event -> {
            disableCompanionEditing();
            changesWereMade = true;
            checkForValidForm();
        });
    }

    private Registration addValueChangeListenerToFormElement(TextField textField, Registration registration) {
        if (registration != null) {
            registration.remove();
        }
        return textField.addValueChangeListener(event -> {
            disableCompanionEditing();
           changesWereMade = true;
           checkForValidForm();
        });
    }

    private Dialog confirmationDialog;
    private boolean checkComesFromDeselection = false;
    private void askToSaveChangesMadeOnCurrentSelectedPlant() {
        disableCompanionEditing();
        if (!checkForValidForm() && !checkComesFromDeselection /*&& !currentlyEditingGCList && !currentlyEditingBCList*/) {
            Dialog infoDialog = new Dialog();
            infoDialog.setCloseOnOutsideClick(false);
            VerticalLayout infoDialogContainer = new VerticalLayout();
            infoDialogContainer.setAlignItems(FlexComponent.Alignment.CENTER);
            infoDialog.add(infoDialogContainer);
            infoDialogContainer.add(new H4("The form is missing some details, please check them for correctness first."));
            HorizontalLayout btnContainer = new HorizontalLayout();
            infoDialogContainer.add(btnContainer);
            Button undoBtn = new Button("Discard Changes");
            undoBtn.addClickListener(event -> {
                changesWereMade = false;
                availablePlantsGrid.deselectAll();
                availablePlantsGrid.select(currentlySelectedPlantInForm);
                infoDialog.close();
            });
            Button showWrongInput = new Button("Add Missing Details");
            showWrongInput.addClickListener(event -> {
                checkComesFromDeselection = true;
                availablePlantsGrid.deselectAll();
                if (!currentlyEditingANewPlant) {
                    if (name.isInvalid()) {
                        name.setPlaceholder("was " + currentlySelectedPlantInForm.getName());
                    }
                    if (spread.isInvalid()) {
                        spread.setPlaceholder("was " + currentlySelectedPlantInForm.getSpreadDiameterInCM());
                    }
                    if (height.isInvalid()) {
                        height.setPlaceholder("was " + currentlySelectedPlantInForm.getHeightInCM());
                    }
                    if (wci.isInvalid()) {
                        wci.setPlaceholder("was " + currentlySelectedPlantInForm.getWaterConsumption());
                    }
                }
                if (gc.size() == 0) {
                    MessageBean.showMessage("The Good Companions List needs to have at least 1 Element.");
                }
                infoDialog.close();
            });
            btnContainer.add(undoBtn, showWrongInput);
            infoDialog.open();
            return;
        } else if (!checkForValidForm() && checkComesFromDeselection && !currentlyEditingGCList && !currentlyEditingBCList){
            checkComesFromDeselection = false;
            return;
        }

        if (confirmationDialog == null) {
            confirmationDialog = new Dialog();
            confirmationDialog.setCloseOnOutsideClick(false);
            Label confirmText = new Label("Do you want to save the changes?");
            Button yes = new Button("Yes");
            yes.addClickListener(event -> {
                if (currentlyEditingANewPlant) {
                    saveNewPlantToList();
                    switchListenerOfFormElements();
                    saveNewPlantBtn.setEnabled(false);
                } else {
                    applyChangesMadeToPlantDetails();
                    applyChangesToPlantBtn.setEnabled(false);
                    deletePlantFromListBtn.setEnabled(true);
                }
                saveNewPlantListToClientDeviceBtn.setEnabled(true);
                nameOfFileField.setEnabled(true);
                availablePlantsGrid.deselectAll();
                availablePlantsGrid.select(currentlySelectedPlantInForm);
                confirmationDialog.close();
            });
            Button no = new Button("No");
            no.addClickListener(event -> {
                changesWereMade = false;
                availablePlantsGrid.deselectAll();

                if (currentlyEditingANewPlant) {
                    switchListenerOfFormElements();
                    saveNewPlantBtn.setEnabled(false);
                } else {
                    applyChangesToPlantBtn.setEnabled(false);
                    availablePlantsGrid.select(currentlySelectedPlantInForm);
                    deletePlantFromListBtn.setEnabled(true);
                }
                confirmationDialog.close();
            });
            HorizontalLayout hl = new HorizontalLayout();
            hl.add(no,yes);
            hl.setAlignItems(FlexComponent.Alignment.CENTER);
            VerticalLayout vl = new VerticalLayout();
            vl.add(confirmText, hl);
            vl.setAlignItems(FlexComponent.Alignment.CENTER);
            confirmationDialog.add(vl);
        } else if (confirmationDialog.isOpened()) {
            return;
        }
        confirmationDialog.open();
    }

    private Plant formerSelectedPlant;

    private void changeGridselectionModeToDefault() {
        if (gridRegistration != null) {
            gridRegistration.remove();
            restorePlantsInGrid();
            availablePlantsGrid.select(formerSelectedPlant);
        }
        gridRegistration = availablePlantsGrid.asSingleSelect().addValueChangeListener(event -> {
            if (changesWereMade && !currentlyInAddToCompanionListMode) {
                askToSaveChangesMadeOnCurrentSelectedPlant();
            } else {
                if(!currentlyInputListenerAttachedToFormElements) {
                    switchListenerOfFormElements();
                }
                Set<Plant> ps = this.availablePlantsGrid.getSelectedItems();
                if (ps.size() > 0) {
                    formerSelectedPlant = ps.stream().findFirst().get();
                    addPlantselectionToForm(ps.stream().findFirst().get());
                }
            }
        });
    }

    private void changeGridselectionModelToAdd() {
        if (currentlyEditingGCList) {
            disablePlantsInGrid(gc);
        } else if (currentlyEditingBCList) {
            disablePlantsInGrid(bc);
        }


        if (gridRegistration != null) {
            gridRegistration.remove();
            availablePlantsGrid.deselectAll();
        }
        gridRegistration = availablePlantsGrid.asSingleSelect().addValueChangeListener(event -> {
            Set<Plant> ps = this.availablePlantsGrid.getSelectedItems();
            if (ps.size() > 0) {
                Plant p = ps.stream().findFirst().get();
                if(currentlyEditingGCList) {
                    CompanionListElement cle = new CompanionListElement(p, "#99ff99");
                    gcElements.add(cle);
                    gcContainer.remove(stopAddModeOfGcBtn);
                    gcContainer.add(cle);
                    gcContainer.add(stopAddModeOfGcBtn);
                    cle.enableDeletion(this);
                    gc.add(p);
                    disablePlantsInGrid(p);
                    changesWereMade = true;
                    checkForValidForm();
                } else if (currentlyEditingBCList){
                    CompanionListElement cle = new CompanionListElement(p, "#ff9999");
                    bcElements.add(cle);
                    bcContainer.remove(stopAddModeOfBcBtn);
                    bcContainer.add(cle);
                    bcContainer.add(stopAddModeOfBcBtn);
                    cle.enableDeletion(this);
                    bc.add(p);
                    disablePlantsInGrid(p);
                    changesWereMade = true;
                    checkForValidForm();
                }
            }
        });
    }

    private List<Plant> temporaryPlantList = new ArrayList<>();

    private void disablePlantsInGrid(List<Plant> plantsToDisable) {
        temporaryPlantList.addAll(plantsToDisable);

        clonedPlants.removeAll(plantsToDisable);
        availablePlantsGrid.setItems(clonedPlants);
    }

    private void disablePlantsInGrid(Plant plant) {
        temporaryPlantList.add(plant);

        clonedPlants.remove(plant);
        availablePlantsGrid.setItems(clonedPlants);
    }

    private void enablePlantsInGrid(Plant plant) {
        temporaryPlantList.remove(plant);

        clonedPlants.add(plant);
        availablePlantsGrid.setItems(clonedPlants);
    }

    private void restorePlantsInGrid() {
        clonedPlants.addAll(temporaryPlantList);
        availablePlantsGrid.setItems(clonedPlants);

        temporaryPlantList = new ArrayList<>();
    }

    private void enableDeletion(List<CompanionListElement> elems) {
        for (CompanionListElement elem : elems) {
            elem.enableDeletion(this);
        }
    }

    private void disableDeletion(List<CompanionListElement> elems) {
        for (CompanionListElement elem : elems) {
            elem.disableDeletion();
        }
    }

    private void saveNewPlantToList() {
        Plant newPlant = new Plant();
        newPlant.setId(plantService.getNextID());
        newPlant.setName(name.getValue());
        newPlant.setSpreadDiameterInCM(Integer.valueOf(spread.getValue()));
        newPlant.setHeightInCM(Integer.valueOf(height.getValue()));
        newPlant.setWaterConsumption(Integer.valueOf(wci.getValue()));
        int[] gcIds = new int[gc.size()];
        for (int i = 0; i < gcIds.length; i++) {
            gcIds[i] = gc.get(i).getId();
        }
        newPlant.setGoodCompanionIDs(gcIds);
        int[] bcIds = new int[bc.size()];
        for (int i = 0; i < bcIds.length; i++) {
            bcIds[i] = bc.get(i).getId();
        }
        newPlant.setBadCompanionIDs(bcIds);

        clonedPlants.add(newPlant);
        changedPlants.add(newPlant);
        saveNewPlantListToClientDeviceBtn.setEnabled(true);
        nameOfFileField.setEnabled(true);
        changesWereMade = false;
        currentlyEditingANewPlant = false;
        availablePlantsGrid.deselectAll();
        form.removeAll();
        setupForm();
        availablePlantsGrid.setItems(clonedPlants);
        availablePlantsGrid.select(newPlant);
    }

    private void addPlantselectionToForm(Plant plant) {
        currentlyEditingANewPlant = false;
        currentlySelectedPlantInForm = null;

        try {
            currentlySelectedPlantInForm = plant.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        if (currentlySelectedPlantInForm != null) {
            name.setValue(currentlySelectedPlantInForm.getName());
            spread.setValue(String.valueOf(currentlySelectedPlantInForm.getSpreadDiameterInCM()));
            height.setValue(String.valueOf(currentlySelectedPlantInForm.getHeightInCM()));
            wci.setValue(String.valueOf(currentlySelectedPlantInForm.getWaterConsumption()));
            gcElements = new ArrayList<>();
            gc = new ArrayList<>();
            gcContainer.removeAll();
            for (int i : currentlySelectedPlantInForm.getGoodCompanionIDs()) {
                CompanionListElement elem = new CompanionListElement(findPlant(i), "#99ff99");
                gcElements.add(elem);
                gcContainer.add(elem);
                gc.add(elem.getPlant());
            }
            bcElements = new ArrayList<>();
            bc = new ArrayList<>();
            bcContainer.removeAll();
            for (int i : currentlySelectedPlantInForm.getBadCompanionIDs()) {
                CompanionListElement elem = new CompanionListElement(findPlant(i), "#ff9999");
                bcElements.add(elem);
                bcContainer.add(elem);
                bc.add(elem.getPlant());
            }
        }

        editGcsBtn.setVisible(true);
        editBcsBtn.setVisible(true);
        doneEditingGCsBtn.setVisible(false);
        doneEditingBCsBtn.setVisible(false);
        saveNewPlantBtn.setVisible(false);
        applyChangesToPlantBtn.setVisible(true);
        deletePlantFromListBtn.setEnabled(true);
    }

    private void applyChangesMadeToPlantDetails() {
        currentlySelectedPlantInForm.setName(name.getValue());
        currentlySelectedPlantInForm.setSpreadDiameterInCM(Integer.valueOf(spread.getValue()));
        currentlySelectedPlantInForm.setHeightInCM(Integer.valueOf(height.getValue()));
        currentlySelectedPlantInForm.setWaterConsumption(Integer.valueOf(wci.getValue()));
        int[] gcIds = new int[gc.size()];
        for (int i = 0; i < gcIds.length; i++) {
            gcIds[i] = gc.get(i).getId();
        }
        currentlySelectedPlantInForm.setGoodCompanionIDs(gcIds);
        int[] bcIds = new int[bc.size()];
        for (int i = 0; i < bcIds.length; i++) {
            bcIds[i] = bc.get(i).getId();
        }
        currentlySelectedPlantInForm.setBadCompanionIDs(bcIds);

        for (int i = 0; i < clonedPlants.size(); i++) {
            if (clonedPlants.get(i).getId() == currentlySelectedPlantInForm.getId()) {
                clonedPlants.set(i, currentlySelectedPlantInForm);
                if (!changedPlants.contains(currentlySelectedPlantInForm)) {
                    changedPlants.add(currentlySelectedPlantInForm);
                } else {
                    changedPlants.remove(currentlySelectedPlantInForm);
                    changedPlants.add(currentlySelectedPlantInForm);
                }
                availablePlantsGrid.setItems(clonedPlants);
                saveNewPlantListToClientDeviceBtn.setEnabled(true);
                nameOfFileField.setEnabled(true);
                break;
            }
        }

        changesWereMade = false;
    }

    private String fileName;
    private void saveNewPlantListToClientDevice(String nameOfFile) {
        for (Plant p : changedPlants) {
            if (clonedPlants.contains(p)) {
                plantService.save(p);
                plantService.setDefaultListHasChanged(true);
            }
        }
        for (Plant p : deletedPlants) {
            plantService.delete(p);
            plantService.setDefaultListHasChanged(true);
        }

        if(nameOfFile == null || nameOfFile.equals("")) {
            fileName = "myNewPlantList";
        } else {
            fileName = nameOfFile;
        }
        Dialog downloadDialog = new Dialog();
        downloadDialog.setCloseOnOutsideClick(false);
        VerticalLayout  dDL = new VerticalLayout();
        dDL.setAlignItems(FlexComponent.Alignment.CENTER);
        dDL.add(new H4("Download"));
        dDL.add(new Label("You can now choose to download a JSON-file named '" + fileName + "', to reuse your modifications in the future."));
        dDL.add(new Label("After hitting the Close button, you can use your modifications in this session."));
        Anchor download = new Anchor(new StreamResource( fileName + ".json", () -> getJsonInputStream()), "");
        download.getElement().setAttribute("download", true);
        download.add( new Button(new Icon(VaadinIcon.DOWNLOAD_ALT)));
        Button close = new Button("close");
        close.addClickListener(event -> {
            downloadDialog.close();
            this.close();
        });
        HorizontalLayout btnContainer = new HorizontalLayout();
        btnContainer.add(download, close);
        dDL.add(btnContainer);
        downloadDialog.add(dDL);
        downloadDialog.open();

        saveNewPlantListToClientDeviceBtn.setEnabled(false);
        nameOfFileField.setEnabled(false);
    }

    private InputStream getJsonInputStream() {
        String jsonString = plantService.fetchVegetablesToJsonString();
        return new ByteArrayInputStream(jsonString.getBytes(StandardCharsets.UTF_8));
    }

    public void removePlantFromCompanionList(Plant p) {
        if (currentlyEditingGCList) {
            gc.remove(p);

            for (CompanionListElement elem : gcElements) {
                if (elem.getPlant().getId() == p.getId()) {
                    gcElements.remove(elem);
                    gcContainer.remove(elem);
                    break;
                }
            }
        } else if (currentlyEditingBCList) {
            bc.remove(p);

            for (CompanionListElement elem : bcElements) {
                if (elem.getPlant().getId() == p.getId()) {
                    bcElements.remove(elem);
                    bcContainer.remove(elem);
                    break;
                }
            }
        }

        if ((currentlyEditingGCList || currentlyEditingBCList) && currentlyInAddToCompanionListMode) {
            enablePlantsInGrid(p);
        }
        changesWereMade = true;
        checkForValidForm();
    }

    /**
     * If the form is filled with valid data, enable save new plant or apply changes to current plant
     */
    private boolean checkForValidForm() {
        boolean noMissingFormDetails = !name.isInvalid() && !spread.isInvalid() && !height.isInvalid() && !wci.isInvalid() && gc.size() > 0;

        if (noMissingFormDetails) {
            if (currentlyEditingANewPlant) {
                saveNewPlantBtn.setEnabled(true);
            } else {
                applyChangesToPlantBtn.setEnabled(true);
                deletePlantFromListBtn.setEnabled(false);
            }
            return true;
        } else {
            if (currentlyEditingANewPlant) {
                saveNewPlantBtn.setEnabled(false);
            } else {
                applyChangesToPlantBtn.setEnabled(false);
                deletePlantFromListBtn.setEnabled(true);
            }
            return false;
        }
    }

    @Override
    public void close(){
        if (changedPlants.size() == 0 && deletedPlants.size() == 0 && !plantService.isDefaultListChanged()) {
            controller.deactivateRestoreListBtn();
        }
        this.controller.updateMainList();
        super.close();

    }

    @Override
    public void open() {
        super.open();
        availablePlantsGrid.select(clonedPlants.get(0));
    }
}
