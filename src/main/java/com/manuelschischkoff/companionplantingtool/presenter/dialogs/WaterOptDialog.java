package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class WaterOptDialog extends Dialog {

    private MainController controller;
    private ComboBox<FieldPositionValues> fieldPosCombobox;
    private Label errorMsg;

    public WaterOptDialog(MainController controller) {
        this.controller = controller;

        setWidth("50vw");

        VerticalLayout dialogLayout = new VerticalLayout();

        H3 header = new H3("Optimize the water supply.");
        header.getStyle().set("margin", "auto");
        dialogLayout.add(header);
        dialogLayout.add(new Label("With this optimization, the distances between thirsty plants and your water supply" +
                " should get as short as possible."));

        fieldPosCombobox = new ComboBox<>("Here you can choose which part of the field is closest to your water supply:");
        fieldPosCombobox.setPlaceholder("nothing selected");
        fieldPosCombobox.setItems(FieldPositionValues.values());
        fieldPosCombobox.setRequired(true);
        fieldPosCombobox.setWidth("30vw");
        fieldPosCombobox.getStyle().set("margin", "auto");
        dialogLayout.add(fieldPosCombobox);

        errorMsg = new Label("Please choose an option from the drop down menu above or click cancel.");
        errorMsg.getStyle().set("color", "#ff0000");
        errorMsg.getStyle().set("margin", "auto");
        errorMsg.getStyle().set("padding-top", "5vh");
        errorMsg.getStyle().set("padding-bottom", "5vh");
        errorMsg.setVisible(false);

        dialogLayout.add(errorMsg);


        HorizontalLayout buttonsContainer = new HorizontalLayout();
        buttonsContainer.getStyle().set("margin", "auto");
        buttonsContainer.getStyle().set("padding-top", "2vh");

        Button cancelBtn = new Button("Cancel");
        Button okBtn = new Button("Ok");

        cancelBtn.addClickListener(event -> this.close());

        okBtn.addClickListener(event -> {
            if (this.fieldPosCombobox.getValue() == null) {
                errorMsg.setVisible(true);
                return;
            }
            this.controller.setShowFilterBtnWaterDistances(false);
            this.close();
            this.controller.optimizeWaterSuppDist(fieldPosCombobox.getValue());
        });

        buttonsContainer.add(cancelBtn, okBtn);
        dialogLayout.add(buttonsContainer);
        add(dialogLayout);
    }

}
