package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OptionalParametersDialog extends Dialog {

    private final MainController controller;
    private VerticalLayout dialogLayout;
    private boolean maxBCD = false;
    private boolean maxSun = false;
    private boolean minWSD = false;
    private boolean maxGCRs = false;
    private ComboBox<FieldPositionValues> fieldPosCombobox;
    private TextField northsouthdist;
    private TextField spacebetweenrows;
    private List<Plant> selectedPlants;
    private ComboBox<Integer> vegetableRowsComboBox;
    private HashMap<Integer, Integer> plantRowLimitation;
    private Label textOfLimitationListLabel;


    public OptionalParametersDialog(MainController controller, List<Plant> selectedPlants) {
        this.selectedPlants = selectedPlants;
        this.controller = controller;
        this.setCloseOnOutsideClick(false);

        dialogLayout = new VerticalLayout();

        setupHeader();
        setupLimitations();
        setupOptimizationOptions();
        setupFieldsizeDetails();

        AtomicBoolean allInputsSet = new AtomicBoolean(false);
        HorizontalLayout btnContainer = new HorizontalLayout();

        btnContainer.add(new Button("OK", buttonClickEvent -> {
            controller.setFixedVegetablePositions(plantRowLimitation);
            controller.setOptimizedBCDistances(maxBCD);
            controller.setOptimizedAmountOfSun(maxSun);
            controller.setOptimizedWaterDistances(minWSD);
            controller.setWaterSupplyPosition(fieldPosCombobox.getValue());
            controller.setOptimizeGCRs(maxGCRs);
            controller.setNorthSouthDistance(parseInput(northsouthdist));
            controller.setSpaceBetweenRows(parseInput(spacebetweenrows));
            controller.refreshSolutions(true);
            controller.setFindAllOptimalSolutions(false);
            allInputsSet.set(true);
            this.close();
        }));

        btnContainer.add(new Button("Cancel", click -> {
            allInputsSet.set(false);
            this.close();
        }));

        dialogLayout.add(btnContainer);

        this.addDetachListener(detachEvent -> {
            if (allInputsSet.get()) {
                controller.prepareSolver();
            }
        });

        add(dialogLayout);
    }

    private void setupHeader() {
        dialogLayout.add(new H3("Choose optional parameters for the constraint solver."));
        dialogLayout.add(new Label(" In Order to get results faster, especially when you want to grow a lot of different vegetables, you can now choose limitations and optimizations."));
    }

    private Integer currentlySelectedPlantID;
    private List<Plant> limitationPlantListWithUniqueNames;
    private void setupLimitations() {
        dialogLayout.add(new H4("Limitations"));
        dialogLayout.add(new Label("Here you can restrict the scope, by fixing positions for certain vegetable rows."));
        plantRowLimitation = new HashMap<>();

        ComboBox<Plant> plantComboBox = new ComboBox<>();
        plantComboBox.setLabel("Vegetable selection");
        plantComboBox.setItemLabelGenerator(Plant::getName);

        limitationPlantListWithUniqueNames = new ArrayList<>();
        for (Plant p : selectedPlants) {
            if (!limitationPlantListWithUniqueNames.contains(p)) {
                limitationPlantListWithUniqueNames.add(p);
            } else {
                int uniqueID = p.getId();
                int counter = 0;
                counter++;
                uniqueID += 100;
                while (limitationPlantListWithUniqueNames.contains(uniqueID)) {
                    counter++;
                    uniqueID += 100;
                }


                try {
                    Plant newP = p.clone();
                    newP.setName(p.getName() + "" + counter);
                    newP.setId(uniqueID);
                    limitationPlantListWithUniqueNames.add(newP);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
        plantComboBox.setItems(limitationPlantListWithUniqueNames);

        vegetableRowsComboBox = new ComboBox<>();
        vegetableRowsComboBox.setLabel("Rows (W-E)");
        List<Integer> rows = IntStream.rangeClosed(1, selectedPlants.size()).boxed().collect(Collectors.toList());
        vegetableRowsComboBox.setItems(rows);
        vegetableRowsComboBox.setEnabled(false);

        plantComboBox.addValueChangeListener(event -> {
            if (!event.getSource().isEmpty()) {
                currentlySelectedPlantID = event.getValue().getId();

                Integer i = plantRowLimitation.get(currentlySelectedPlantID);
                if (i != null) {
                    rows.add(i);
                    vegetableRowsComboBox.setItems(rows);
                    plantRowLimitation.remove(currentlySelectedPlantID);
                    updateTextOfLimitationList();
                }
                vegetableRowsComboBox.setEnabled(true);
            } else {
                vegetableRowsComboBox.setEnabled(false);
            }
        });

        vegetableRowsComboBox.addValueChangeListener(event -> {
            if (!event.getSource().isEmpty()) {
                plantRowLimitation.put(currentlySelectedPlantID, event.getValue());
                vegetableRowsComboBox.setEnabled(false);
                rows.remove(event.getValue());
                vegetableRowsComboBox.setItems(rows);
            }
            updateTextOfLimitationList();
        });

        HorizontalLayout plantRowLimitContainer = new HorizontalLayout();
        plantRowLimitContainer.add(plantComboBox, vegetableRowsComboBox);
        textOfLimitationListLabel = new Label();
        dialogLayout.add(plantRowLimitContainer, textOfLimitationListLabel);
    }

    private void updateTextOfLimitationList() {
        textOfLimitationListLabel.setText("");
        StringBuilder sb = new StringBuilder();
        for (Integer i : plantRowLimitation.keySet()) {
           String name = "";

           for (Plant p: limitationPlantListWithUniqueNames) {
               if (p.getId() == i) {
                   name = p.getName();
                   break;
               }
           }

            sb.append(name + ": row ");
            sb.append(plantRowLimitation.get(i)).append(";\t");
        }
        textOfLimitationListLabel.setText(sb.toString());
    }

    private void setupOptimizationOptions() {
        dialogLayout.add(new H4("Optimizations"));
        dialogLayout.add(new Label("Here you can choose different optimizations:"));

        Checkbox ensureGCNH = new Checkbox("Maximize the number of good companion neighbors");
        Checkbox maxBCD = new Checkbox("Maximize the distances between bad companions");
        Checkbox maxSun = new Checkbox("Maximize the sun exposure of each plant");
        Checkbox minWSD = new Checkbox("Minimize the watering distances");
        VerticalLayout checkboxContainer = new VerticalLayout();
        checkboxContainer.add(ensureGCNH, maxBCD, maxSun, minWSD);

        ensureGCNH.addValueChangeListener(event -> this.maxGCRs = event.getValue());
        maxBCD.addValueChangeListener(event -> this.maxBCD = event.getValue());
        maxSun.addValueChangeListener(event -> this.maxSun = event.getValue());
        minWSD.addValueChangeListener(event -> {
            boolean eventValue = event.getValue();

            if (eventValue) {
                checkboxContainer.add(fieldPosCombobox);
                fieldPosCombobox.setRequired(true);
                fieldPosCombobox.setPreventInvalidInput(true);
                fieldPosCombobox.setValue(FieldPositionValues.East);
            } else {
                checkboxContainer.remove(fieldPosCombobox);
                fieldPosCombobox.setRequired(false);
                fieldPosCombobox.setPreventInvalidInput(false);
            }

            this.minWSD = eventValue;
        });

        fieldPosCombobox = new ComboBox<>("Water supply position:");
        fieldPosCombobox.setItems(FieldPositionValues.values());

        dialogLayout.add(checkboxContainer);
    }

    private void setupFieldsizeDetails() {
        dialogLayout.add(new H4("Field Stats"));
        dialogLayout.add(new Label("Here you can define the size of your field and whether you want to have some space between the rows to walk through your field, or not." +
                " If you enter the height ('North-South-Distance'), the solver will calculate the amount of plants you will be able to grow per row." +
                " The width of the field ('West-East-Distance'), will be calculated by the spread of each vegetable including the space between two rows."));

        northsouthdist = new TextField("North-South-Distance");
        northsouthdist.setPattern("[0-9]*");
        northsouthdist.setPreventInvalidInput(true);
        northsouthdist.setPlaceholder("Height in ...");
        northsouthdist.setSuffixComponent(new Span(" cm"));


        spacebetweenrows= new TextField("Space between rows");
        spacebetweenrows.setPattern("[0-9]*");
        spacebetweenrows.setPreventInvalidInput(true);
        spacebetweenrows.setPlaceholder("i.e. 10 ");
        spacebetweenrows.setSuffixComponent(new Span(" cm"));

        HorizontalLayout fieldSizes = new HorizontalLayout();
        fieldSizes.add(northsouthdist, spacebetweenrows);
        dialogLayout.add(fieldSizes);
    }

    private int parseInput(TextField input) {
        if (!input.getValue().isEmpty() && Integer.parseInt(input.getValue()) > 0) {
            return Integer.parseInt(input.getValue());
        }
        return 0;
    }
}
