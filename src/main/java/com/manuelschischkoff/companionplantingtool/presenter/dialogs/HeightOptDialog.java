package com.manuelschischkoff.companionplantingtool.presenter.dialogs;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class HeightOptDialog extends Dialog {

    private MainController controller;

    public HeightOptDialog(MainController controller) {
        this.controller = controller;

        setWidth("50vw");

        VerticalLayout dialogLayout = new VerticalLayout();

        H3 header = new H3("Maximize the amount of sun per plant.");
        header.getStyle().set("margin", "auto");
        dialogLayout.add(header);
        dialogLayout.add(new Label("With this optimization, the amount of sun will be maximized." +
                " For every layout from the result list the amount of sun each plant receives in average will be calculated." +
                " The plant which will have the most sun-hours and the plant which will receive the least amount of sun, " +
                " will be found and the layout where this difference is the smallest, will be selected." +
                " That will ensure that each plant will receive as many hours of sun as possible."));

        HorizontalLayout buttonsContainer = new HorizontalLayout();
        buttonsContainer.getStyle().set("margin", "auto");
        buttonsContainer.getStyle().set("padding-top", "2vh");

        Button cancelBtn = new Button("Cancel");
        Button okBtn = new Button("Ok");

        cancelBtn.addClickListener(event -> this.close());

        okBtn.addClickListener(event -> {
            this.controller.setShowFilterBtnAmountOfSun(false);
            this.close();
            this.controller.optimizeAmountOfSun();
        });

        buttonsContainer.add(cancelBtn, okBtn);
        dialogLayout.add(buttonsContainer);
        add(dialogLayout);
    }

}
