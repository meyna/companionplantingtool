package com.manuelschischkoff.companionplantingtool.presenter;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;

import java.util.ArrayList;

@Push
public class FieldSolutionDetailsView extends VerticalLayout {

    private MainController controller;

    private Label name = new Label("");
    private Grid<Plant> fieldLayoutList = new Grid<>();

    public FieldSolutionDetailsView(MainController controller) {
        this.controller = controller;

        fieldLayoutList.addColumn(Plant::getName).setHeader("Plantorder (W-E)");
        fieldLayoutList.addColumn(Plant::getAmount).setHeader("Amount");
        add(name, fieldLayoutList);
    }

    public void setSolutionDetails(PlantOrder plantOrder) {
        if (plantOrder == null) {
            name.setText("Nothing selected.");
            fieldLayoutList.setItems(new ArrayList<>());
            return;
        }

        name.setText(plantOrder.getName());
        fieldLayoutList.setItems(plantOrder.getPlants());
    }
}
