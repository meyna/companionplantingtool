package com.manuelschischkoff.companionplantingtool.presenter;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.manuelschischkoff.companionplantingtool.model.Solution;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.shared.Registration;

import java.util.List;

@Push
public class FieldSolutionView extends VerticalLayout {

    private final MainController controller;


    private Grid<PlantOrder> solutionsGrid = new Grid<>();
    private FieldSolutionDetailsView solutionDetailsView;
    private FieldDrawingAndStatisticsView fieldDrawing;
    private VerticalLayout solutionView;
    private Label solutionLabel;
    private VerticalLayout progressView;
    private Label foundSolutionLabel;
    private List<PlantOrder> plantOrders;
    private boolean firstTimeSetup = true;
    private boolean firstTimeSelection = true;
    private HorizontalLayout solutionLists;
    private Solution solution;

    // drawingHeader of drawings
    private HorizontalLayout drawingHeader = new HorizontalLayout();
    private H3 layoutNr = new H3();
    private Button prevOrderBtn =  new Button();
    private Button nextOrderBtn =  new Button();
    private Button restoreInitialResults = new Button();
    private int solutionIndex = -1;
    private Registration registration;

    public FieldSolutionView(MainController controller) {
        this.controller = controller;

        setupHeader();
        setupSolutionsView();
    }

    private void setupHeader() {
        add(new H2("Field Layouts"));
    }

    private void setupSolutionsView() {
        if (!firstTimeSetup) {
            remove(solutionLists, drawingHeader, fieldDrawing);
            firstTimeSelection = true;
        }

        solutionView = new VerticalLayout();
        progressView = new VerticalLayout();
        HorizontalLayout calculationInfo  = new HorizontalLayout();

        Button stopCalcBtn = new Button();
        stopCalcBtn.setText("Stop");

        stopCalcBtn.addClickListener(buttonClickEvent -> controller.stopCalculations());
        calculationInfo.add(stopCalcBtn);

        Label foundSolutionsTextLabel = new Label("Solutions found: ");
        foundSolutionLabel = new Label("0");
        calculationInfo.add(foundSolutionsTextLabel, foundSolutionLabel);

        progressView.add(calculationInfo);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        progressView.add(progressBar);

        solutionView.add(progressView);

        solutionLabel = new Label("Select a solution in the list to see the details");

        solutionsGrid = new Grid<>();
        solutionDetailsView = new FieldSolutionDetailsView(controller);
        fieldDrawing = new FieldDrawingAndStatisticsView(controller);
        fieldDrawing.setVisible(false);

        registration = addGridSelectionModel();

        solutionView.add(solutionLabel, solutionsGrid);

        solutionLists = new HorizontalLayout();
        solutionLists.add(solutionView, solutionDetailsView);
        solutionLists.setWidth("85vw");

        prevOrderBtn.setText("Prev");
        nextOrderBtn.setText("Next");
        restoreInitialResults.setText("Restore All Results");

        if (firstTimeSetup) {
            /* needed this check.
                For some reason the click listener would get registered +1 on every setup, although we remove the component on refresh
                (works with value change listener and stop calculation btn click listener)
             */
            prevOrderBtn.addClickListener(event -> {
                solutionIndex--;
                solutionsGrid.select(plantOrders.get(solutionIndex));
            });
            nextOrderBtn.addClickListener(event -> {
                solutionIndex++;
                solutionsGrid.select(plantOrders.get(solutionIndex));
            });

            restoreInitialResults.addClickListener(event -> this.controller.restoreResults());
        }

        prevOrderBtn.getStyle().set("margin", "auto");
        layoutNr.getStyle().set("padding-left", "2vw");
        layoutNr.getStyle().set("padding-right", "2vw");
        layoutNr.getStyle().set("margin", "auto");
        nextOrderBtn.getStyle().set("margin", "auto");
        drawingHeader.add(prevOrderBtn, layoutNr, nextOrderBtn);
        drawingHeader.getStyle().set("margin", "auto");
        drawingHeader.setVisible(false);

        restoreInitialResults.getStyle().set("margin", "auto");
        restoreInitialResults.setVisible(false);

        add(solutionLists, restoreInitialResults, drawingHeader, fieldDrawing);
    }

    private Registration addGridSelectionModel() {
        return solutionsGrid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null) {
                fieldDrawing.setVisible(false);
                drawingHeader.setVisible(false);
                solutionIndex = -1;
                solutionsGrid.deselectAll();
                this.getElement().callFunction("scrollIntoView");
            } else {
                solutionDetailsView.setSolutionDetails(event.getValue());
                solutionIndex = plantOrders.indexOf(event.getValue());
                if (solutionIndex == 0 && solutionIndex == plantOrders.size() - 1) {
                    prevOrderBtn.setEnabled(false);
                    nextOrderBtn.setEnabled(false);
                } else if(solutionIndex == 0) {
                    prevOrderBtn.setEnabled(false);
                    nextOrderBtn.setEnabled(true);
                } else if (solutionIndex == plantOrders.size() - 1) {
                    nextOrderBtn.setEnabled(false);
                    prevOrderBtn.setEnabled(true);
                } else {
                    prevOrderBtn.setEnabled(true);
                    nextOrderBtn.setEnabled(true);
                }
                fieldDrawing.setVisible(true);
                drawingHeader.setVisible(true);

                layoutNr.setText(event.getValue().getName());
                fieldDrawing.drawLayoutAndStatistics(event.getValue().getPlants(), solution/*, solutionIndex == plantOrders.size() - 1*/);
                if (firstTimeSelection) {
                    drawingHeader.getElement().callFunction("scrollIntoView");
                    firstTimeSelection = false;
                }
            }
        });
    }

    public void setSolution(Solution solution, boolean isInitialCall) {
        this.solution = solution;
        solutionsGrid.addColumn(PlantOrder::getName).setHeader("Solutions");
        plantOrders = solution.getPlantOrders();
        solutionsGrid.addColumn(PlantOrder::getOptimizationValue).setHeader("Optimization value");
        solutionsGrid.setItems(plantOrders);

        if (!isInitialCall) {
            removeProgressBar();
        }
        firstTimeSetup = false;
    }

    private double timeDiffInSecs = 3.0;
    private double startTime = 0.0;
    private boolean restartInterval = true;
    public void updateSolutions(PlantOrder newPlantorder, int solutionNR) {
        if (restartInterval) {
            startTime = System.currentTimeMillis() / 1000.0;
            restartInterval = false;
        }
        plantOrders.add(newPlantorder);
        nextOrderBtn.setEnabled(true);

        foundSolutionLabel.setText("" + solutionNR);
        solutionsGrid.getDataProvider().refreshAll();
        if (System.currentTimeMillis() / 1000.0 - startTime> timeDiffInSecs) {
            solutionView.getUI().ifPresent(UI::push);
            restartInterval = true;
        }
    }

    public void resetSolutions(List<PlantOrder> newPlantOrders) {
        plantOrders = newPlantOrders;

        foundSolutionLabel.setText("" + plantOrders.size());
        solution.setPlantOrders(plantOrders);
        solutionsGrid.setItems(plantOrders);
        solutionView.getUI().ifPresent(UI::push);

//        if (restartInterval) {
//            startTime = System.currentTimeMillis() / 1000.0;
//            solutionView.getUI().get().push();
//            restartInterval = false;
//        }
//        if (System.currentTimeMillis() / 1000.0 - startTime> timeDiffInSecs) {
//            solutionView.getUI().get().push();
//            restartInterval = true;
//        }
    }

    public void filterSolutions(Solution filteredSolution) {
        refreshSolutions();
        setupSolutionsView();
        setSolution(filteredSolution, false);
        restoreInitialResults.setVisible(true);
    }

    public void removeProgressBar() {
        progressView.setVisible(false);
        solutionLabel.setText("Select a solution in the list to see the details (" + plantOrders.size() + " solutions found).");
        solutionView.getUI().ifPresent(UI::push); // force ui to update
        restartInterval = true;
    }

    public void refreshSolutions() {
        if (!firstTimeSetup) {
            setupSolutionsView();
        }
    }

    public Button getHeightOptBtn() {
        return fieldDrawing.getHeightOptBtn();
    }

    public Button getWaterOptBtn() {
        return fieldDrawing.getWaterOptBtn();
    }

    public Button getBcOptBtn() {
        return fieldDrawing.getBcOptBtn();
    }

    public Button getGcOptBtn() {
        return fieldDrawing.getGcOptBtn();
    }

    public void addProgressBar() {
        progressView.setVisible(true);
        solutionView.getUI().ifPresent(UI::push); // force ui to update
    }
}
