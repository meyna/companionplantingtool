package com.manuelschischkoff.companionplantingtool.model;

public enum FieldPositionValues {
    East, West, Middle
}
