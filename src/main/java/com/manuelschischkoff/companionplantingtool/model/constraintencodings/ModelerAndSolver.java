package com.manuelschischkoff.companionplantingtool.model.constraintencodings;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.manuelschischkoff.companionplantingtool.presenter.FieldSolutionView;
import com.manuelschischkoff.companionplantingtool.model.utils.LayoutSolver;
import com.manuelschischkoff.companionplantingtool.model.utils.MessageBean;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.*;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ModelerAndSolver extends Thread{

    private LayoutSolver layoutSolver;

    private List<Plant> selectedPlants;
    private final HashMap<Integer, Integer> plantRowLimitations;
    private List<PlantOrder> plantOrders;
    private final FieldSolutionView fieldSolutionView;
    private int spaceBetweenRows;
    private HashMap<Integer, Plant> idToPlantMap;

    private AtomicBoolean running = new AtomicBoolean(false);

    // for optimization
    private boolean optimizeAmountOfSun;
    private boolean optimizeBCDistances;
    private boolean optimizeWSDistances;
    private FieldPositionValues waterSupplyPosition;
    private boolean optimizeGCRs;


    // for Choco Lib
//    private final int MAX_ALLOWED_SECONDS_BETWEEN_SOLUTIONS  = 3 * 60;
    @SuppressWarnings("FieldCanBeLocal")
    private int MULTIPLIER_GCR = 1000;
    @SuppressWarnings("FieldCanBeLocal")
    private int MULTIPLIER_BCD = 10;
    @SuppressWarnings("FieldCanBeLocal")
    private int MULTIPLIER_SE = 50; //
    @SuppressWarnings("FieldCanBeLocal")
    private int MULTIPLIER_WD = 100;
    private int[] uniqueIDArray;
    private Model model;
    private IntVar[] fieldLayout;
    private IntVar optimumWaterDistToCostRatio;
    private IntVar[] combinedOptimizationValues;
    private IntVar combinedOptimum;

    // choco optimization targets
    private IntVar gcnSum; // the number of good companion neighbors, asymmetric -> 2n - 2 max.
    private IntVar bcSum; // the sum of all distances between bad companions, asymmetric -> number of pairs * field_with max.
    private IntVar maxSunExposureValues; // the difference from max reachable sun exposure value minus the corresponding values, a method to maximize a original minimization goal


    // For further calculations
    private BlockingQueue<PlantOrder> heightWorkingQueue;
    private BlockingQueue<PlantOrder> waterWorkingQueue;
    private BlockingQueue<PlantOrder> bcWorkingQueue;
    private BlockingQueue<PlantOrder> gcWorkingQueue;
    @SuppressWarnings("FieldCanBeLocal")
    private boolean optimizationSelected = false;
    private boolean combinedOptimizationSelected = false;

    //  Encoding Classes
    private AllCombinations allComb;
    private GoodCompanionRelationshipOptimization gcrOpt;
    private BadCompanionDistanceOptimization bcdOpt;
    private SunExposureOptimization seOpt;
    private WateringDistanceOptimization wdOpt;


    // CONSTRUCTOR
    public ModelerAndSolver(MainController controller, LayoutSolver layoutSolver, List<Plant> selectedPlants, HashMap<Integer, Integer> plantRowLimitations, HashMap<Integer, Plant> idToPlantMap, int spaceBetweenRows, FieldSolutionView fieldSolutionView, HeightDataPreparation heightOptThread, WaterDataPreparation waterOptThread, BadCompanionDistanceDataPreparation bcOptThread, GoodCompanionNeighbourhoodDataPreparation gcOptThread) {
        this.layoutSolver = layoutSolver;
        this.selectedPlants = selectedPlants;
        this.plantRowLimitations = plantRowLimitations;
        this.idToPlantMap = idToPlantMap;
        this.spaceBetweenRows = spaceBetweenRows;
        this.fieldSolutionView = fieldSolutionView;


        uniqueIDArray = vegetableEncoder(selectedPlants);

        allComb = new AllCombinations();

        if (heightOptThread != null) {
            this.heightWorkingQueue = heightOptThread.getWorkingQueue();
        }
        else if (controller.isOptimizedAmountOfSun()){
            this.optimizeAmountOfSun = true;
            seOpt = new SunExposureOptimization();
        }
        if (waterOptThread != null) {
            this.waterWorkingQueue = waterOptThread.getWorkingQueue();
        }
        else if (controller.isOptimizedWaterDistances()){
            optimizeWSDistances = true;
            waterSupplyPosition = controller.getWaterSupplyPosition();
            wdOpt = new WateringDistanceOptimization();
        }
        if (bcOptThread != null) {
            this.bcWorkingQueue = bcOptThread.getWorkingQueue();
        }
        else if (controller.isOptimizedBCDistances()){
            optimizeBCDistances = true;
            bcdOpt = new BadCompanionDistanceOptimization();
        }
        if (gcOptThread != null) {
            this.gcWorkingQueue = gcOptThread.getWorkingQueue();
        }
        else if (controller.isOptimizeGCRs()){
            optimizeGCRs = true;
            gcrOpt = new GoodCompanionRelationshipOptimization();
        }
    }

    // MODEL AND CONSTRAINTS FUNCTIONS

    private void setupModel() {
        model = new Model("Companion Planting Problem Solver");

        // VARIABLES
        fieldLayout = model.intVarArray(selectedPlants.size(), uniqueIDArray);

        // setup constraints to ensure that there are maximum all n! layouts
        allComb.setupAllCombConstraints(model, fieldLayout, plantRowLimitations);

        // Optimizations
        // check for combined optimizations
        combinedOptimizationSelected = (optimizeBCDistances && optimizeAmountOfSun)
                            || (optimizeBCDistances && optimizeWSDistances)
                            || (optimizeBCDistances && optimizeGCRs)
                            || (optimizeAmountOfSun && optimizeWSDistances)
                            || (optimizeAmountOfSun && optimizeGCRs)
                            || (optimizeWSDistances && optimizeGCRs);

        if (combinedOptimizationSelected) {
            combinedOptimizationValues = model.intVarArray(4, -50000, 50000);
            combinedOptimum = model.intVar(-30000, 200000);
        }

        if (optimizeGCRs) {
            gcnSum = model.intVar(0, (fieldLayout.length - 1) * 2);
            gcrOpt.setupGoodCompanionConstraints(gcnSum, model, fieldLayout, uniqueIDArray, this, idToPlantMap, selectedPlants);
            if (combinedOptimizationSelected) {
                model.times(gcnSum, MULTIPLIER_GCR, combinedOptimizationValues[0]).post();
            }
        } else if (combinedOptimizationSelected) {
            gcnSum = model.intVar(0);
            model.arithm(combinedOptimizationValues[0], "=", 0).post();
        }

        // variables needed for BCD and SE Optimization
        int field_width = layoutSolver.getFieldWidthInCM();
        int radiusLB = field_width; // smallest vegetable plant radius
        int radiusUB = 0; // biggest vegetable plant radius
        int heightUB = 0; // highest vegetable plant
        if (optimizeBCDistances || optimizeAmountOfSun) {
            // find lower and upper bounds for IntVars
            for (int i : uniqueIDArray) {
                Plant currentP = decodeVegetableUniqueID(i);

                if (radiusLB > currentP.getSpreadDiameterInCM()) {
                    radiusLB = currentP.getSpreadDiameterInCM();
                }

                if (radiusUB < currentP.getSpreadDiameterInCM()) {
                    radiusUB = currentP.getSpreadDiameterInCM();
                }

                if (heightUB < currentP.getHeightInCM()) {
                    heightUB = currentP.getHeightInCM();
                }
            }

            // at this point we have stored the spread in the radius values, so we have to adjust that.
            radiusLB = Math.floorDiv(radiusLB, 2);
            radiusUB = Math.floorDiv(radiusUB, 2);
        }

        if (optimizeBCDistances) {
            List<BadCompanionPair> badCompanionPairs = findAllBCCombs(); // find all BC-Pairs, which exists among the selected Plants.
            bcSum = model.intVar(spaceBetweenRows * badCompanionPairs.size(), (field_width - 2 * radiusLB) * badCompanionPairs.size());
            bcdOpt.setupMaxDistOfBadCompanionConstraints(model, field_width, fieldLayout, uniqueIDArray, bcSum, this, spaceBetweenRows, optimizeBCDistances, badCompanionPairs, fieldSolutionView, idToPlantMap, radiusLB, radiusUB);

            if (combinedOptimizationSelected) {
                //adapt the multiplier
                if ((selectedPlants.size() >= 6 || field_width >= 330) && badCompanionPairs.size() > 3) {
                    MULTIPLIER_BCD = 6;
                }
                model.times(bcSum, MULTIPLIER_BCD, combinedOptimizationValues[1]).post();
            }
        } else if (combinedOptimizationSelected) {
            bcSum = model.intVar(0);
            model.arithm(combinedOptimizationValues[1], "=", 0).post();
        }

        if (optimizeAmountOfSun) {
            maxSunExposureValues = model.intVar(-seOpt.MAX_REACHABLE_CIRCUMFERENCES_PER_PLANT, 0);
            seOpt.simplerSetupSunExposureMaximization(model, fieldLayout, maxSunExposureValues, layoutSolver.getFieldWidthInCM(), uniqueIDArray, spaceBetweenRows, this, radiusLB, radiusUB, heightUB);

            if (combinedOptimizationSelected) {
                model.times(maxSunExposureValues, MULTIPLIER_SE, combinedOptimizationValues[2]).post();
            }
        } else if (combinedOptimizationSelected) {
            maxSunExposureValues = model.intVar(0);
            model.arithm(combinedOptimizationValues[2], "=", 0).post();
        }

        if (optimizeWSDistances) {
            optimumWaterDistToCostRatio = model.intVar(0, fieldLayout.length * 5 * fieldLayout.length); // variable to maximize.
            wdOpt.setupMinWaterDistConstraints(model, optimumWaterDistToCostRatio, fieldLayout, waterSupplyPosition, uniqueIDArray, this);

            if (combinedOptimizationSelected) {
                model.times(optimumWaterDistToCostRatio, MULTIPLIER_WD, combinedOptimizationValues[3]).post();
            }
        } else if (combinedOptimizationSelected) {
            optimumWaterDistToCostRatio = model.intVar(0);
            model.arithm(combinedOptimizationValues[3], "=", 0).post();
        }
    }

    private Map<Integer, Plant> uniqueIDToRegularPlantMap;
    private int[] vegetableEncoder (List<Plant> vegetablesToUniquify) {
        uniqueIDToRegularPlantMap = new HashMap<>();

        for (Plant p : vegetablesToUniquify) {
            Integer uniqueID = p.getId();

            while (uniqueIDToRegularPlantMap.get(uniqueID) != null) {
                uniqueID += 100;
            }

            uniqueIDToRegularPlantMap.put(uniqueID, p);
        }

        return uniqueIDToRegularPlantMap.keySet().stream().mapToInt(i -> i).toArray();
    }

    Plant decodeVegetableUniqueID(int uniqueID) {
        return uniqueIDToRegularPlantMap.get(uniqueID);
    }

    private List<BadCompanionPair> findAllBCCombs() {
        List<BadCompanionPair> bcpList = new ArrayList<>();
        for (Plant p : selectedPlants) {
            List<Integer> currentBCArray = Arrays.stream(p.getBadCompanionIDs()).boxed().collect(Collectors.toList());

            for (Plant p2 : selectedPlants) {
                if (currentBCArray.contains(p2.getId())) {
                    bcpList.add(new BadCompanionPair(p, p2));
                }
            }
        }
        return bcpList;
    }

    // SOLVING AND SOLUTION FUNCTION
    private int currentMax = 0;
    private Solver solver;
//    private float timeOfLastSolution = 0;
    @SuppressWarnings({"Duplicates"})
    @Override
    public void run() {
        running.set(true);
        setupModel();
        fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("Everything is set up, starting the solver now.")));
        fieldSolutionView.getUI().ifPresent(uil -> uil.access(uil::push));
        plantOrders = new ArrayList<>();
        solver = model.getSolver();
        solver.showShortStatistics();
//        solver.showDecisions();

        AtomicInteger resultNr = new AtomicInteger();

        if (optimizeGCRs && !combinedOptimizationSelected) {
            solver.plugMonitor((IMonitorSolution) () -> {
                if (currentMax < gcnSum.getValue() || currentMax == 0) {
                    currentMax = gcnSum.getValue();
                    fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("The solver found a solution with " + currentMax + " good companion relationships.", 5)));
                    newOrder(resultNr.incrementAndGet(), currentMax);
                }
            });
            solver.findOptimalSolution(gcnSum, true);
        }
        if (optimizeBCDistances && !combinedOptimizationSelected) {
            solver.plugMonitor((IMonitorSolution) () -> {
                if (currentMax < bcSum.getValue() || currentMax == 0) {
                    currentMax = bcSum.getValue();
                    fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("The solver found a solution with an overall sum of " + currentMax + " cm between bad companions.", 5)));
                    newOrder(resultNr.incrementAndGet(), currentMax);
                }
            });
            solver.findOptimalSolution(bcSum, true);
        }
        if (optimizeAmountOfSun && !combinedOptimizationSelected) {
            solver.plugMonitor((IMonitorSolution) () -> {
//                timeOfLastSolution = solver.getTimeCount();
                if (currentMax < maxSunExposureValues.getValue() || currentMax == 0 && maxSunExposureValues.getValue() != 0) {
                    currentMax = maxSunExposureValues.getValue();
                    fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("The solver found a solution with " + (-1*currentMax) + " sun exposure value between the plant which is the most and least exposed to the sun.", 5)));
                    newOrder(resultNr.incrementAndGet(), (-1*currentMax));
                }
            });
            // to restrict the solver to 3 min. of solving after the last layout was found.
//            solver.addStopCriterion(() -> solver.getTimeCount() - timeOfLastSolution > MAX_ALLOWED_SECONDS_BETWEEN_SOLUTIONS /*solver.getDecisionCount() > MAX_ALLOWED_DECISIONS*/);
            solver.findOptimalSolution(maxSunExposureValues, true);
        }
        if (optimizeWSDistances && !combinedOptimizationSelected) {
            solver.plugMonitor((IMonitorSolution) () -> {
                if (currentMax < optimumWaterDistToCostRatio.getValue() || currentMax == 0) {
                    currentMax = optimumWaterDistToCostRatio.getValue();
                    fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("The solver found a solution with an overall watering distance to consumption ratio of " + currentMax + ".", 5)));
                    newOrder(resultNr.incrementAndGet(), currentMax);
                }
            });
            solver.findOptimalSolution(optimumWaterDistToCostRatio, true);
        }

        optimizationSelected = optimizeBCDistances || optimizeAmountOfSun || optimizeWSDistances || optimizeGCRs;

        if (!optimizationSelected){
            while (solver.solve() && running.get()) {
                    newOrder(resultNr.incrementAndGet(), -1);

                // If we apply more constraints here, then we get a checker
            }
        } else if (combinedOptimizationSelected) {
            model.sum(combinedOptimizationValues, "=", combinedOptimum).post();

            solver.plugMonitor((IMonitorSolution) () -> {
//                timeOfLastSolution = solver.getTimeCount();
//                System.out.println("Solution found: current value = " + currentMax + "; combinedOptValue = " + combinedOptimum.getValue());
                System.out.println("single Values: GCR = " + gcnSum.getValue() + ", BCD = " + bcSum.getValue() + ", SE = " + maxSunExposureValues.getValue() + ", WD = " + optimumWaterDistToCostRatio.getValue());
                System.out.println("combined Values: GCR = " + combinedOptimizationValues[0].getValue() + ", BCD = " + combinedOptimizationValues[1].getValue() + ", SE = " + combinedOptimizationValues[2].getValue() + ", WD = " + combinedOptimizationValues[3].getValue());
                if (currentMax < combinedOptimum.getValue() || currentMax == 0) {
                    currentMax = combinedOptimum.getValue();
                    fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("The solver found a solution with a combined value of " + currentMax + ".", 3)));
                    newOrder(resultNr.incrementAndGet(), currentMax);
                }
            });
            // to restrict the solver to 3 min. of solving after the last layout was found.
//            solver.addStopCriterion(() -> solver.getTimeCount() - timeOfLastSolution > MAX_ALLOWED_SECONDS_BETWEEN_SOLUTIONS /*solver.getDecisionCount() > MAX_ALLOWED_DECISIONS*/);
            solver.findOptimalSolution(combinedOptimum, true);
        }

        if (resultNr.get() == 0) {

            fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> MessageBean.showMessage("There is no solution with your setup.", 10)));
            System.out.println("NO SOLUTION FOUND!");
        }

        fieldSolutionView.getUI().ifPresent(uil -> uil.access(fieldSolutionView::removeProgressBar));
    }

    private void newOrder(int orderNr, int value) {
        String name = "Field Layout " + orderNr;
        List<Plant> plants = new ArrayList<>();


        for (IntVar integers : fieldLayout) {
//            Plant p = null;
//            try {
//                p = decodeVegetableUniqueID(integers.getValue()).clone();
//            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
//            }
//            if (p != null) {
//                plants.add(p);
//            }
            plants.add(decodeVegetableUniqueID(integers.getValue()));
        }


        PlantOrder po = new PlantOrder(orderNr - 1, name, plants);
        if (value != -1) {
            po.setOptimizationValue(value);
        }
        plantOrders.add(po);

        fieldSolutionView.getUI().ifPresent(uil -> uil.access(() -> fieldSolutionView.updateSolutions(po, orderNr)));


        if (!optimizeGCRs && !optimizeBCDistances && !optimizeAmountOfSun && !optimizeWSDistances) {
            try {
                if (heightWorkingQueue != null) {
                    heightWorkingQueue.put(po);
                }
                if (waterWorkingQueue != null) {
                    waterWorkingQueue.put(po);
                }
                if (bcWorkingQueue != null) {
                    bcWorkingQueue.put(po);
                }
                if (gcWorkingQueue != null) {
                    gcWorkingQueue.put(po);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public List<PlantOrder> getPlantOrders() {
        return plantOrders;
    }

    public void stopCalculations() {
        running.set(false);

        if (solver != null) {
            solver.limitTime("1s");
        }

        this.interrupt();

        fieldSolutionView.getUI().ifPresent(ui1 -> ui1.access(fieldSolutionView::removeProgressBar));

        // >> HACK << within the choco library there is no function to stop the solver immediately,
        //  so force the thread to stop. Otherwise, the solver is able to continue its work, while the user may
        //  start a new request, which will result in a mess if the old solver finds further solutions.
        //  Besides that the old request, which has no use anymore, needs resources which won't be available
        //  to a new requests, unless the old solver finishes on regular basis.
        //noinspection deprecation
        this.stop();
    }

    @SuppressWarnings("unused")
    public boolean isCalculating() {
        return this.running.get();
    }
}