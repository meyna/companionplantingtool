package com.manuelschischkoff.companionplantingtool.model.constraintencodings;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;

@SuppressWarnings("Duplicates")
class SunExposureOptimization {

    public int MAX_REACHABLE_CIRCUMFERENCES_PER_PLANT = 9999+9999+9999; // 2 * max allowed height + max allowed spread in cm

    SunExposureOptimization() {
    }

    void simplerSetupSunExposureMaximization(Model model, IntVar[] fieldLayout, IntVar minDeltaCircumferences,
                                             int field_width, int[] uniqueIDArray, int spaceBetweenRows,
                                             ModelerAndSolver modeler, int radiusLB, int radiusUB, int heightUB) {

        // all needed information for the constraint solver
        IntVar[] dist_row = model.intVarArray(fieldLayout.length, radiusLB, field_width);
        IntVar[] radius_row = model.intVarArray(fieldLayout.length, radiusLB, radiusUB+1);
        IntVar[] spread_row = model.intVarArray(fieldLayout.length, 2*radiusLB, 2*radiusUB+1);
        IntVar[] height_row = model.intVarArray(fieldLayout.length, 0, heightUB);

        // assign the information needed according to the different layouts being checked
        for (int i = 0; i < fieldLayout.length; i++) {
            for (int j = 0; j < uniqueIDArray.length; j++) {
                Constraint[] constraints = new Constraint[4];
                int currentSpread = modeler.decodeVegetableUniqueID(uniqueIDArray[j]).getSpreadDiameterInCM();
                int currentRadius = Math.floorDiv(currentSpread, 2);

                if (i == 0) {
                    constraints[0] = model.arithm(dist_row[i], "=", currentRadius);
                    constraints[1] = model.arithm(radius_row[i], "=", currentRadius);
                } else {
                    IntVar[] tmpDistVals = {dist_row[i - 1], radius_row[i-1] , model.intVar(spaceBetweenRows + currentRadius)};
                    constraints[0] = model.sum(tmpDistVals, "=", dist_row[i]);
                    constraints[1] = model.arithm(radius_row[i], "=", currentRadius);
                }

                int currentHeight = modeler.decodeVegetableUniqueID(uniqueIDArray[j]).getHeightInCM();
                constraints[2] = model.arithm(height_row[i], "=", currentHeight);
                constraints[3] = model.arithm(spread_row[i], "=", currentSpread);

                model.ifThen(
                        model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                        Constraint.merge("mergedDistHeightSpreadValues[" + i + "," + j + "]", constraints)
                );
            }
        }

        // using the circumferences, which describes the area that will be exposed to the sun, instead of using some
        // sun hours to calculate "realistic" sun-exposure-values improves the process, since there are less calculations
        // for the solver, than we would have if we calculate "realistic" sun-exposure-values.

        // 2 * heightUB (Left and Right side of a plant), 2*radiusUB + (2*radiusUB = maxDiameter, and +1, since we floorDiv the diameter to the radius)
        IntVar[] circumferences = model.intVarArray(fieldLayout.length, -2, 2*heightUB + 2*radiusUB + 1);

        for (int i = 0; i < fieldLayout.length; i++) {
            IntVar leftSide = model.intVar(-1,heightUB);
            IntVar rightSide = model.intVar(-1,heightUB);
            // left top, top and right top, holds the multiplication factor for the dividend.
            // at the end the top part of the circumference  gets calculated by the diameter * sum(topSides) / 3
            IntVar[] topSides = model.intVarArray(3,0,1);
            // every plant should have at least 1/3 of their diameter being exposed to the sun.
            // this approach will more likely place larger plants in diameter, rather than smaller ones, close to tall plants, since their guaranteed part, which
            // will be exposed to the sun, is larger thant the one from plants with smaller diameters.
            model.arithm(topSides[1], "=", 1).post();

            // left side
            if (i > 0) {
                // the height difference of the current plant to the left neighbor plant
                IntVar deltaYLeftNeighbor = model.intVar(-heightUB, heightUB);
                // if the left neighbor plant is taller this will result in a negative value ( -> 0 cm for the total circumference);
                // if it is of the same height, this will result in 0 cm;
                // and if it is of smaller, this will result in a positive value (which will be taken into account,
                // when calculating the circumference);
                model.arithm(deltaYLeftNeighbor, "=", height_row[i], "-", height_row[i-1]).post();

                // to hold the values needed of the tallest plant left to the current plant.
                IntVar maxHeightLeft = model.intVar(0, heightUB);
                IntVar rightEdgeOfHighestLeft = model.intVar(0, field_width); // lb and ub may can be updated to radius bounds

                // a temporary sub array holding the height values of all plants left of the current
                IntVar[] tmpHeightArrayLeft = model.intVarArray(i, 0, heightUB);

                Constraint[] leftPrepareConstraints = new Constraint[i+1];

                for (int j = 0; j < i; j++) {
                    leftPrepareConstraints[j]=
                        model.arithm(tmpHeightArrayLeft[j], "=", height_row[j]);
                }

                // finding the tallest plant in the temporary sub array
                leftPrepareConstraints[i] =
                    model.max(maxHeightLeft, tmpHeightArrayLeft);

                Constraint.merge("leftPrepareConstraints"+i, leftPrepareConstraints).post();

                // find the tallest plant to the left of the current plant, which is the closest to the current.
                // save the x position of its right edge.
                // this approach is a little bit hacky but works quite well for all the test cases
                IntVar[] allMaxHeightLeft = model.intVarArray(i, 0, field_width);
                int index = 0;
                for (int j = i-1; j >= 0; j--) {
                    model.ifThen(
                            model.arithm(maxHeightLeft, "=", height_row[j]).reify(),
                            model.arithm(allMaxHeightLeft[index++], "=", dist_row[j], "+", radius_row[j])
                    );
                }

                model.arithm(rightEdgeOfHighestLeft, "=", allMaxHeightLeft[0]).post();

                Constraint[] constraintsForSpecialcaseLeft = new Constraint[5];

                // get the deltaY and deltaX to the highest plant on the field which is left of the current.
                IntVar deltaYLeftMax = model.intVar(-heightUB, heightUB);
                constraintsForSpecialcaseLeft[0] = model.arithm(deltaYLeftMax, "=", height_row[i], "-", maxHeightLeft); // we subtract that way, to get a negative value for deltaY, if the target plant is higher than the current

                IntVar deltaXLeftMax = model.intVar(-field_width,0);
                IntVar leftEdgeOfCurrent = model.intVar(0, field_width);
                constraintsForSpecialcaseLeft[1] = model.arithm(leftEdgeOfCurrent, "=", dist_row[i], "-", radius_row[i]);
                constraintsForSpecialcaseLeft[2] = model.arithm(deltaXLeftMax, "=", rightEdgeOfHighestLeft, "-", leftEdgeOfCurrent ); // we need a negative deltaX to compare it with deltY

                IntVar doubleDeltaYLeftMax = model.intVar(-2*heightUB, 2*heightUB);
                constraintsForSpecialcaseLeft[3] = model.times(deltaYLeftMax, 2, doubleDeltaYLeftMax);

                // check the ratio of deltaYNeighbor, deltaYMaxLeft, deltaXMaxLeft and assign adequate values to the circumference parts.
                // it seems to have a huge impact on the time until the solver terminates, when using layouts with
                // big height differences. if we change the order of the case checks here, this improves the termination
                // time extremely (check min and max cases first, then mid range cases)
                constraintsForSpecialcaseLeft[4] =
                model.or(
                        model.and(
                                model.or(
                                        model.arithm(deltaYLeftNeighbor, "=", 0).reify(),
                                        model.and(
                                                model.arithm(deltaYLeftNeighbor, ">", 0).reify(),
                                                model.arithm(deltaYLeftMax, "<=", deltaXLeftMax).reify(),
                                                model.arithm(doubleDeltaYLeftMax, "<=", deltaXLeftMax).reify()
                                        ).reify()
                                ),
                                model.arithm(topSides[0], "=", 0),
                                model.arithm(leftSide, "=", 0)
                        ),
                        model.and(
                                model.and(
                                        model.arithm(deltaYLeftNeighbor, ">", 0).reify(),
                                        model.arithm(deltaYLeftMax, ">", 0).reify()
                                ),
                                model.arithm(topSides[0], "=", 1),
                                model.arithm(leftSide, "=", deltaYLeftNeighbor)
                        ),
                        model.and(
                                model.and(
                                        model.arithm(deltaYLeftNeighbor, ">", 0).reify(),
                                        model.arithm(deltaYLeftMax, ">", deltaXLeftMax).reify(),
                                        model.arithm(doubleDeltaYLeftMax, "<=", deltaXLeftMax).reify()
                                ),
                                model.arithm(topSides[0], "=", 1),
                                model.div(deltaYLeftNeighbor, model.intVar(2), leftSide)
                        )
                );

                model.ifThenElse(
                        model.arithm(deltaYLeftNeighbor, "<", 0).reify(),
                        model.or(
                                model.and(
                                        model.and(
                                                model.arithm(deltaYLeftNeighbor, ">", -1*spaceBetweenRows).reify()
                                        ),
                                        model.arithm(topSides[0], "=", 1),
                                        model.arithm(leftSide, "=", 0)
                                ),
                                model.and(
                                        model.and(
                                                model.arithm(deltaYLeftNeighbor, "<=", -1*spaceBetweenRows).reify()
                                        ),
                                        model.arithm(topSides[0], "=", 0),
                                        model.arithm(leftSide, "=", -1)
                                )
                        ),
                        Constraint.merge("mergedLeftSpecialcase[" + i + "]", constraintsForSpecialcaseLeft)
                );
            } else {
                model.and(
                        model.arithm(topSides[0], "=", 1),
                        model.arithm(leftSide, "=", height_row[i])
                ).post();
            }

            // right side (for further comments see left side
            if (i < fieldLayout.length - 1) {
                IntVar deltaYRightNeighbor = model.intVar(-heightUB, heightUB);
                model.arithm(deltaYRightNeighbor, "=", height_row[i], "-", height_row[i+1]).post();

                IntVar maxHeightRight = model.intVar(0, heightUB);
                IntVar leftEdgeOfHighestRight = model.intVar(0, field_width);

                IntVar[] tmpHeightArrayRight = model.intVarArray(fieldLayout.length - 1 - i, 0, heightUB);

                Constraint[] rightPreparingConstraints = new Constraint[fieldLayout.length-i];

                for (int j = i+1; j < fieldLayout.length; j++) {
                    rightPreparingConstraints[j-(i+1)] =
                            model.arithm(tmpHeightArrayRight[j-(i+1)], "=", height_row[j]);
                }

                rightPreparingConstraints[fieldLayout.length-i-1] =
                        model.max(maxHeightRight, tmpHeightArrayRight);

                Constraint.merge("rightPreparingConstraints"+i, rightPreparingConstraints).post();

                // this approach is a little bit hacky but works quite well with all test cases
                IntVar[] allMaxHeightRight = model.intVarArray(fieldLayout.length - i - 1, 0, field_width);
                int index = 0;
                for (int j = i+1; j < fieldLayout.length; j++) {
                    model.ifThen(
                            model.arithm(maxHeightRight, "=", height_row[j]).reify(),
                            model.arithm(allMaxHeightRight[index++], "=", dist_row[j], "-", radius_row[j])
                    );
                }

                model.arithm(leftEdgeOfHighestRight, "=", allMaxHeightRight[0]).post();

                Constraint[] constraintsForSpecialcaseRight = new Constraint[5];
                IntVar deltaYRightMax = model.intVar(-heightUB, heightUB);
                constraintsForSpecialcaseRight[0] = model.arithm(deltaYRightMax, "=", height_row[i], "-", maxHeightRight);

                IntVar deltaXRightMax = model.intVar(-field_width, 0);
                IntVar rightEdgeOfCurrent = model.intVar(0, field_width);
                constraintsForSpecialcaseRight[1] = model.arithm(rightEdgeOfCurrent, "=", dist_row[i], "+", radius_row[i]);
                constraintsForSpecialcaseRight[2] = model.arithm(deltaXRightMax, "=", rightEdgeOfCurrent, "-", leftEdgeOfHighestRight);

                IntVar doubleDeltaYRightMax = model.intVar(-2*heightUB, 2*heightUB);
                constraintsForSpecialcaseRight[3] = model.times(deltaYRightMax, 2, doubleDeltaYRightMax);
                constraintsForSpecialcaseRight[4] =
                        model.or(
                                model.and(
                                        model.or(
                                                model.arithm(deltaYRightNeighbor, "=", 0).reify(),
                                                model.and(
                                                        model.arithm(deltaYRightNeighbor, ">", 0).reify(),
                                                        model.arithm(deltaYRightMax, "<=", deltaXRightMax).reify(),
                                                        model.arithm(doubleDeltaYRightMax, "<=", deltaXRightMax).reify()
                                                ).reify()
                                        ),
                                        model.arithm(topSides[2], "=", 0),
                                        model.arithm(rightSide, "=", 0)
                                ),
                                model.and(
                                        model.and(
                                                model.arithm(deltaYRightNeighbor, ">", 0).reify(),
                                                model.arithm(deltaYRightMax, ">", 0).reify()
                                        ),
                                        model.arithm(topSides[2], "=", 1),
                                        model.arithm(rightSide, "=", deltaYRightNeighbor)
                                ),
                                model.and(
                                        model.and(
                                                model.arithm(deltaYRightNeighbor, ">", 0).reify(),
                                                model.arithm(deltaYRightMax, ">", deltaXRightMax).reify(),
                                                model.arithm(doubleDeltaYRightMax, "<=", deltaXRightMax).reify()
                                        ),
                                        model.arithm(topSides[2], "=", 1),
                                        model.div(deltaYRightNeighbor, model.intVar(2), rightSide)
                                )
                        );

                model.ifThenElse(
                        model.arithm(deltaYRightNeighbor, "<", 0).reify(),
                        model.or(
                                model.and(
                                        model.and(
                                                model.arithm(deltaYRightNeighbor, ">", -1*spaceBetweenRows).reify()
                                        ),
                                        model.arithm(topSides[2], "=", 1),
                                        model.arithm(rightSide, "=", 0)
                                ),
                                model.and(
                                        model.and(
                                                model.arithm(deltaYRightNeighbor, "<=", -1*spaceBetweenRows).reify()
                                        ),
                                        model.arithm(topSides[2], "=", 0),
                                        model.arithm(rightSide, "=", -1)
                                )
                        ),
                        Constraint.merge("mergedRightSpecialcase[" + i + "]", constraintsForSpecialcaseRight)
                );
            } else {
                model.and(
                        model.arithm(topSides[2], "=", 1),
                        model.arithm(rightSide, "=", height_row[i])
                ).post();
            }

            // to weight the negative effect of taller plants standing next to plants with smaller diameters
            IntVar topSide = model.intVar(0, 2*radiusUB+1);
            IntVar tmpMultiplicator = model.intVar(1,3);

            // sum up all the circumference parts of the current plant and assign the result to the corresponding array entry
            Constraint[] perPlantConstraint = new Constraint[4];
            perPlantConstraint[0] =
                model.sum(topSides, "=", tmpMultiplicator);

            IntVar tmpDividend = model.intVar(0,(2*radiusUB+1)*3);
            perPlantConstraint[1] =
                    model.times(spread_row[i], tmpMultiplicator, tmpDividend);
            perPlantConstraint[2] =
                    model.div(tmpDividend, model.intVar(3), topSide);

            IntVar[] tmpArray = {leftSide, topSide, rightSide};

            // sum up all relevant circumference parts per plant in the current layout
            perPlantConstraint[3] =
                    model.sum(tmpArray, "=", circumferences[i]);

            Constraint.merge("perPlantConstraints"+i, perPlantConstraint).post();
        }

        // find the plants with the maximum and minimum value of its circumference for this layout
        IntVar maxTmpValue = model.intVar(0, 2*heightUB+radiusUB*2+1);
        Constraint[] perLayoutConstraints = new Constraint[3];
        perLayoutConstraints[0] =
            model.max(maxTmpValue, circumferences);

        IntVar minTmpValue = model.intVar(0, 2*heightUB+radiusUB*2+1);
        perLayoutConstraints[1] =
                model.min(minTmpValue, circumferences);

        // since we want to have a minimal difference of the plant which has the most and least value in its circumference,
        // we subtract the max value from the min value to get a negative value, in order to be able to use a maximization
        // approach instead of an minimization approach, due to the fact that we need a maximization when it comes to
        // combined optimizations (since all other optimizations are maximization).
        perLayoutConstraints[2] =
            model.arithm(minDeltaCircumferences, "=", minTmpValue, "-", maxTmpValue);

        Constraint.merge("perLayoutConstraint", perLayoutConstraints).post();
    }
}