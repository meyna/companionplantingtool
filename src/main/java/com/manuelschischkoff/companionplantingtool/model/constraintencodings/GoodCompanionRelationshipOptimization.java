package com.manuelschischkoff.companionplantingtool.model.constraintencodings;

import com.manuelschischkoff.companionplantingtool.model.Plant;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

class GoodCompanionRelationshipOptimization {

    GoodCompanionRelationshipOptimization() {
    }

    void setupGoodCompanionConstraints(IntVar gcnSum, Model model, IntVar[] fieldLayout, int[] uniqueIDArray,
                                       ModelerAndSolver modeler, HashMap<Integer, Plant> idToPlantMap,
                                       List<Plant> selectedPlants) {
        // reduce the gc arrays to a intersection set of GC(p_i) and selectedPlants
        int[][] intersectionSetOfGCsAndP = new int[uniqueIDArray.length][];
        // the same with the bc arrays, since we want to maximize the number of gc neighbors, we dont want to have bc as neighbors
        int[][] intersectionSetOfBCsAndP = new int[uniqueIDArray.length][];

        getIntersectionSets(intersectionSetOfBCsAndP, intersectionSetOfGCsAndP, modeler, uniqueIDArray, idToPlantMap, selectedPlants);

        // the array holding the number of gc neighbors per field position
        IntVar[] limitedGCNArray = model.intVarArray(fieldLayout.length, 0, 2);

        // for every field position there has to hold one of the following ors:
        // - no gc neighbor
        // - left gc neighbor and no right gc neighbor
        // - vice versa
        // - both gc neighbors
        BoolVar[] ands = new BoolVar[fieldLayout.length];

        for (int i = 0; i < fieldLayout.length; i++) {
            // one of the unique ids has to be on the field position i
            BoolVar[] ors = new BoolVar[uniqueIDArray.length];

            for (int j = 0; j < uniqueIDArray.length; j++) {
                // if we are checking the first field row we can skip the left (western) neighbor
                if (i == 0) {
                    ors[j] = model.or(
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.arithm(limitedGCNArray[i], "=", 0).reify()
                            ).reify(),
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.member(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.arithm(limitedGCNArray[i], "=", 1).reify()
                            ).reify()
                    ).reify();
                }
                // if we are checking the last field row we can skip the right (eastern) neighbor check
                else if (i == fieldLayout.length -1) {
                    ors[j] = model.or(
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.notMember(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i-1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.arithm(limitedGCNArray[i], "=", 0).reify()
                            ).reify(),
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.member(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.arithm(limitedGCNArray[i], "=", 1).reify()
                            ).reify()
                    ).reify();
                }
                // for a regular row with a left (western) and a right (eastern) neighbor
                else {
                    ors[j] = model.or(
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.notMember(fieldLayout[i-1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.arithm(limitedGCNArray[i], "=", 0).reify()
                            ).reify(),
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.member(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i-1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.arithm(limitedGCNArray[i], "=", 1).reify()
                            ).reify(),
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.notMember(fieldLayout[i+1], intersectionSetOfBCsAndP[j]).reify(), // always prevent bcs as neighbors
                                    model.member(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.arithm(limitedGCNArray[i], "=", 1).reify()
                            ).reify(),
                            model.and(
                                    model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                                    model.member(fieldLayout[i+1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.member(fieldLayout[i-1], intersectionSetOfGCsAndP[j]).reify(),
                                    model.arithm(limitedGCNArray[i], "=", 2).reify()
                            ).reify()
                    ).reify();
                }
            }

            ands[i] = model.or(ors).reify();
        }

        model.and(ands).post();

        // finally sum up all good companion relationships to a single IntVar variable which the constraint solver can maximize
        model.sum(limitedGCNArray, "=", gcnSum).post();
    }

    private void getIntersectionSets(int[][] intersectionSetOfBCsAndP, int[][] intersectionSetOfGCsAndP, ModelerAndSolver modeler, int[] uniqueIDArray, HashMap<Integer, Plant> idToPlantMap, List<Plant> selectedPlants) {
        for (int i = 0; i < uniqueIDArray.length; i++) {
            int[] gcIDs = modeler.decodeVegetableUniqueID(uniqueIDArray[i]).getGoodCompanionIDs();
            int[] bcIDs = modeler.decodeVegetableUniqueID(uniqueIDArray[i]).getBadCompanionIDs();

            List<Plant> GCi = new ArrayList<>();
            for (int gcID : gcIDs) {
                GCi.add(idToPlantMap.get(gcID));
            }

            List<Plant> BCi = new ArrayList<>();
            for (int bcID : bcIDs) {
                BCi.add(idToPlantMap.get(bcID));
            }

            List<Plant> intersectionSetGCs = GCi.stream().filter(selectedPlants::contains).collect(Collectors.toList());
            List<Plant> intersectionSetBCs = BCi.stream().filter(selectedPlants::contains).collect(Collectors.toList());

            findSpecialIDOccurencesInTheIntersectionSet(intersectionSetGCs, intersectionSetOfGCsAndP, modeler, i);
            findSpecialIDOccurencesInTheIntersectionSet(intersectionSetBCs, intersectionSetOfBCsAndP, modeler, i);
        }
    }

    private void findSpecialIDOccurencesInTheIntersectionSet(List<Plant> sourceIntersectionSet, int[][] targetIntersectionSet, ModelerAndSolver modeler, int targetIndex) {
        // check if there should occur duplicated ids in the intersection set
        List<Integer> specialIDsToAdd = new ArrayList<>();

        for (Plant p: sourceIntersectionSet) {
            int specialID = p.getId() + 100;

            while (modeler.decodeVegetableUniqueID(specialID) != null) {
                specialIDsToAdd.add(specialID);
                specialID += 100;
            }
        }

        int[] interSectionSetArrayGC = new int[sourceIntersectionSet.size() + specialIDsToAdd.size()];
        for (int j = 0; j < sourceIntersectionSet.size(); j++) {
            interSectionSetArrayGC[j] = sourceIntersectionSet.get(j).getId();
        }
        for (int j = sourceIntersectionSet.size(); j < sourceIntersectionSet.size() + specialIDsToAdd.size(); j++) {
            interSectionSetArrayGC[j] = specialIDsToAdd.get(j - sourceIntersectionSet.size());
        }
        targetIntersectionSet[targetIndex] = interSectionSetArrayGC;
    }
}
