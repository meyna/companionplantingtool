package com.manuelschischkoff.companionplantingtool.model.constraintencodings;


import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;

import java.util.HashMap;

class AllCombinations {

    AllCombinations() {
    }

    void setupAllCombConstraints(Model model, IntVar[] fieldLayout, HashMap<Integer, Integer> plantRowLimitations) {
        // since we are working with pseudo unique ids now, we can use the all different constraint of the library to prevent duplicates.
        model.allDifferent(fieldLayout).post();

        // if there are limitations to the position of certain vegetable rows we have to ensure the restrictions
        if (!plantRowLimitations.isEmpty()) {
            for (Integer i : plantRowLimitations.keySet()) {
                model.arithm(fieldLayout[plantRowLimitations.get(i) - 1], "=", i).post();
            }
        }
    }
}
