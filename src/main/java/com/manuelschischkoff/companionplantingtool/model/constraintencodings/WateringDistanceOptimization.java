package com.manuelschischkoff.companionplantingtool.model.constraintencodings;

import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;

class WateringDistanceOptimization {

    // Array to hold the distance to water consumption ratio according to the water supply position
    private IntVar[] wciDistToWaterRatios;

    WateringDistanceOptimization() {
    }

    void  setupMinWaterDistConstraints(Model model, IntVar optimumWaterDistToCostRatio, IntVar[] fieldLayout,
                                                  FieldPositionValues waterSupplyPosition, int[] uniqueIDArray,
                                                  ModelerAndSolver modeler) {
        // currently using the row order to evaluate minimal watering distances and not real centimeters.
        // this is a faster way to find solutions, the profit of using real centimeters will cause way longer solving times
        // and the profit of less walking distances is minimal (in the range of a few m tops)

        // array to hold the water consumption values of the selected plants in the correct order, according to the fieldLayout assignment
        IntVar[] waterConsumptionValues = model.intVarArray(fieldLayout.length, 1,5);
        wciDistToWaterRatios = model.intVarArray("WATER_DIST", waterConsumptionValues.length, 1, waterConsumptionValues.length * 5);

        // side constraints to build up correct water consumption values
        for (int i = 0; i < fieldLayout.length; i++) {
            for (int j = 0; j < uniqueIDArray.length; j++) {
                model.ifThen(
                        model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                        model.arithm(waterConsumptionValues[i], "=", modeler.decodeVegetableUniqueID(uniqueIDArray[j]).getWaterConsumption())
                );
            }
        }

        // main constraints to calculate the overall distance to water cost ratios
        int multiplier = 1;
        if (!waterSupplyPosition.equals(FieldPositionValues.Middle)) {
            // if the water supply is in the west, start summing up the water distance cost ratio with the factor 1 in the east
            if (waterSupplyPosition.equals(FieldPositionValues.West)) {
                for (int i = waterConsumptionValues.length-1; i >= 0; i--) {
                    model.times(waterConsumptionValues[i], multiplier++, wciDistToWaterRatios[i]).post();
                }
            }
            // otherwise the water supply is in the east, therefore the initialization of the index variables is normal.
            else {
                for (int i = 0; i < waterConsumptionValues.length; i++) {
                    model.times(waterConsumptionValues[i], multiplier++, wciDistToWaterRatios[i]).post();
                }
            }
        }
        // for the middle water supply position we iterate simultaneously from west and east towards the middle
        else {
            int index = 0;
            int reversedIndex = waterConsumptionValues.length-1;
            while (index < reversedIndex || index == reversedIndex) {
                model.times(waterConsumptionValues[index], multiplier, wciDistToWaterRatios[index]).post();

                if (index != reversedIndex) {
                    model.times(waterConsumptionValues[reversedIndex], multiplier, wciDistToWaterRatios[index]).post();
                }

                index++;
                reversedIndex--;
                multiplier++;
            }
        }


        // sum up all ratio entries of the array.
        model.sum(wciDistToWaterRatios, "=", optimumWaterDistToCostRatio).post();
    }
}
