package com.manuelschischkoff.companionplantingtool.model.constraintencodings;

import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.utils.MessageBean;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.BadCompanionPair;
import com.manuelschischkoff.companionplantingtool.presenter.FieldSolutionView;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.IntVar;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("Duplicates")
class BadCompanionDistanceOptimization {

    BadCompanionDistanceOptimization() {
    }

    @SuppressWarnings({"UnusedAssignment", "ParameterCanBeLocal"})
    void setupMaxDistOfBadCompanionConstraints(Model model, int field_width, IntVar[] fieldLayout, int[] uniqueIDArray,
                                               IntVar bcSum, ModelerAndSolver modeler, int spaceBetweenRows,
                                               boolean optimizeBCDistances, List<BadCompanionPair> badCompanionPairs,
                                               FieldSolutionView fieldSolutionView, HashMap<Integer, Plant> idToPlantMap,
                                               int radiusLB, int radiusUB) {
        // check if there are bad companions in the user selected multiset of vegetable plants.
        if (badCompanionPairs.size() == 0) {
            fieldSolutionView.getUI().ifPresent(uil -> uil.access(() ->
                    MessageBean.showMessage("There are no bad companions, continuing without distance optimization", 10)));

            // if not we have to assign 0 to the bcSum variable in the case there were combined optimization goals selected.
            // otherwise we will get NullPointException
            bcSum = model.intVar(0);

            // set to false, that if combined optimization were selected, there will be no access code to BCD specific data.
            optimizeBCDistances = false;
            return;
        }

        // for the distances we have the same LB as with the radius, and the field width minus the lowest radius for the UB
        int distLB = radiusLB;
        int distUB = field_width - radiusLB;


        // holding the distances from the west end of the field to the rows of the fieldLayout
        IntVar[] plantDists = model.intVarArray(uniqueIDArray.length, distLB, distUB);

        // holds the distances from the west end of the field to the rows of the vegetables
        IntVar[] rowDists = model.intVarArray(fieldLayout.length, distLB, distUB);

        // holds the radius of a plant at the corresponding fieldLayout positions
        IntVar[] radius = model.intVarArray(fieldLayout.length, radiusLB, radiusUB);

        // side constraints to build up data structure, in order to assign the rowDists and radius Arrays in the correct order, if a vegetable plant is assigned to a field row.
        for (int i = 0; i < fieldLayout.length; i++) {
            for (int j = 0; j < uniqueIDArray.length; j++) {

                int currentRadius = Math.floorDiv(modeler.decodeVegetableUniqueID(uniqueIDArray[j]).getSpreadDiameterInCM(), 2);

                // holds all Constraints, which should be posted if a plant is assigned to a field row
                Constraint[] constraints = new Constraint[3];

                if (i == 0) {
                    // assign first dist value, the very first row of the field in the west
                    constraints[0] = model.arithm(plantDists[j], "=", currentRadius);
                    constraints[1] = model.arithm(rowDists[i], "=", plantDists[j]);
                    constraints[2] =  model.arithm(radius[i], "=", plantDists[j]);
                } else {
                    // store all relevant distances from the last entry into an array to sum them up for the next entry
                    IntVar[] tmpValues = {
                            rowDists[i-1],          // the distance value of the previous field row
                            radius[i-1],            // the radius of the previous field row
                            model.intVar(spaceBetweenRows + currentRadius) // the space between rows (user chosen, can be 0) + the radius of the current plant from the user selection
                    };

                    // assign distance values for regular cases
                    constraints[0] = model.sum(tmpValues, "=", plantDists[j]);
                    constraints[1] = model.arithm(rowDists[i], "=", plantDists[j]);
                    constraints[2] = model.arithm(radius[i], "=", currentRadius);
                }

                // post the constraints from above if a vegetable plant gets planted to the corresponding field row
                model.ifThen(
                        model.arithm(fieldLayout[i], "=", uniqueIDArray[j]).reify(),
                        Constraint.merge("merged_data_constraints", constraints)
                );
            }
        }

        // main constraints
        // array to hold the distance between bad companions
        IntVar[] bcDistanceDiffs = model.intVarArray(badCompanionPairs.size(), spaceBetweenRows, field_width - 2 * radiusLB);
        int bcCount = 0;

        // for all known bad companion pairs
        for (int j = 0; j < uniqueIDArray.length; j++) {
            int id_j = modeler.decodeVegetableUniqueID(uniqueIDArray[j]).getId();

            for (int k = j+1; k < uniqueIDArray.length; k++) {
                int id_k = modeler.decodeVegetableUniqueID(uniqueIDArray[k]).getId();

                // check if we have a bad companion pair (for both directions)
                boolean isBadCompanion = false;
                boolean isSymmetricBCRS = false;
                if (Arrays.stream(idToPlantMap.get(id_j).getBadCompanionIDs()).anyMatch(x -> x == id_k)) {
                    isBadCompanion = true;
                }
                if (Arrays.stream(idToPlantMap.get(id_k).getBadCompanionIDs()).anyMatch(x -> x == id_j)) {
                    if(isBadCompanion) {
                        isSymmetricBCRS = true;
                    } else {
                        isBadCompanion = true;
                    }
                }

                // if we have a bad companion pair evaluate the distances and post them to the corresponding array entry
                if (isBadCompanion) {
                    // holding the difference of the corresponding distances
                    IntVar distDiff = model.intVar(radiusLB - (field_width - radiusLB), field_width - 2 * radiusLB);
                    // holding the absolute value of the distance difference
                    IntVar absSumDist = model.intVar(spaceBetweenRows, field_width - 2 * radiusLB);

                    // post the subtraction constraint
                    model.arithm(distDiff, "=", plantDists[j], "-", plantDists[k]).post();
                    // post the absolute value of the difference
                    model.absolute(absSumDist, distDiff).post();
                    // add the distance value to the array, holding all bad companion distances
                    model.arithm(bcDistanceDiffs[bcCount++], "=", absSumDist).post();

                    // if we have an symmetric bad companion pair, add this distance twice.
                    if (isSymmetricBCRS) {
                        model.arithm(bcDistanceDiffs[bcCount++], "=", absSumDist).post();
                    }
                }
            }
        }

        // bcSum, sums up the bcDistanceDiffs, the variable to be maximized
        model.sum(bcDistanceDiffs, "=", bcSum).post();
    }
}