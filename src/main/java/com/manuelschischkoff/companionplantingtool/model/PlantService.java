package com.manuelschischkoff.companionplantingtool.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.utils.MessageBean;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;

/**
 * An in memory dummy "database" for the example purposes. In a typical Java app
 * this class would be replaced by e.g. EJB or a Spring based service class.
 * <p>
 * In demos/tutorials/examples, get a reference to this service class with
 * {@link PlantService#getInstance()}.
 */
public class PlantService {

    private static PlantService instance;
    private static final Logger LOGGER = Logger.getLogger(PlantService.class.getName());

    private HashMap<Integer, Plant> plants = new HashMap<>();
    private int nextId = -1;

    private MainController controller;
    private boolean defaultListHasChanged;

    private PlantService(){
    }

    /**
     * @return a reference to an example facade for Customer objects.
     */
    public static PlantService getInstance() {
        if (instance == null) {
            instance = new PlantService();
            instance.ensureTestData();
        }
        return instance;
    }

    /**
     * @return all available Plant objects.
     */
    public synchronized List<Plant> findAll() {
        return findAll(null);
    }

    /**
     * Finds all Plant's that match given filter.
     *
     * @param stringFilter
     *            filter that returned objects should match or null/empty string
     *            if all objects should be returned.
     * @return list of Plant objects
     */
    public synchronized List<Plant> findAll(String stringFilter) {
        ArrayList<Plant> arrayList = new ArrayList<>();
        for (Plant plant : plants.values()) {
            try {
                boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
                        || plant.toSearchString().toLowerCase().contains(stringFilter.toLowerCase());
                if (passesFilter) {
                    arrayList.add(plant.clone());
                }
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(PlantService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Collections.sort(arrayList, new Comparator<Plant>() {

            @Override
            public int compare(Plant o1, Plant o2) {
                return (int) (o2.getId() - o1.getId());
            }
        });
        return arrayList;
    }

    /**
     * Finds all Plant's that match given filter and limits the resultset.
     *
     * @param stringFilter
     *            filter that returned objects should match or null/empty string
     *            if all objects should be returned.
     * @param start
     *            the index of first result
     * @param maxresults
     *            maximum result count
     * @return list of Plant objects
     */
    public synchronized List<Plant> findAll(String stringFilter, int start, int maxresults) {
        ArrayList<Plant> arrayList = new ArrayList<>();
        for (Plant plant : plants.values()) {
            try {
                boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
                        || plant.toSearchString().toLowerCase().contains(stringFilter.toLowerCase());
                if (passesFilter) {
                    arrayList.add(plant.clone());
                }
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(PlantService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Collections.sort(arrayList, new Comparator<Plant>() {

            @Override
            public int compare(Plant o1, Plant o2) {
                return (int) (o2.getId() - o1.getId());
            }
        });
        int end = start + maxresults;
        if (end > arrayList.size()) {
            end = arrayList.size();
        }
        return arrayList.subList(start, end);
    }

    public synchronized Plant find(int plantID) {
        for (Plant p : plants.values()) {
            if (p.getId() == plantID) {
                return p;
            }
        }
        return null;
    }

    /**
     * @return the amount of all plants in the system
     */
    public synchronized long count() {
        return plants.size();
    }

    /**
     * Deletes a plant from a system
     *
     * @param value
     *            the Plant to be deleted
     */
    public synchronized void delete(Plant value) {
        removeDeletedPlantFromAllCompanionsLists(value.getId());
        plants.remove(value.getId());
    }

    private void removeDeletedPlantFromAllCompanionsLists(int id) {
        for (Plant p : plants.values()) {
            List<Integer> currentGoodCompanions = Arrays.stream(p.getGoodCompanionIDs()).boxed().collect(Collectors.toList());
            if (currentGoodCompanions.contains(id)) {
                currentGoodCompanions.remove(Integer.valueOf(id));
                p.setGoodCompanionIDs(currentGoodCompanions.stream().mapToInt(i -> i).toArray());
            }

            List<Integer> currentBadCompanions = Arrays.stream(p.getBadCompanionIDs()).boxed().collect(Collectors.toList());
            if (currentBadCompanions.contains(id)) {
                currentBadCompanions.remove(Integer.valueOf(id));
                p.setBadCompanionIDs(currentBadCompanions.stream().mapToInt(i -> i).toArray());
            }
        }
    }

    /**
     * Persists or updates plant in the system. Also assigns an identifier
     * for new Plant instances.
     *
     * @param entry
     */
    public synchronized void save(Plant entry) {
        if (entry == null) {
            LOGGER.log(Level.SEVERE,
                    "Plant is null, there was an error with the backend connection.");
            return;
        }
        if (entry.getId() == -1) { // if it is -1 then it is a new plant and for that case we need to find the next possible id
            // should be dead code
            LOGGER.log(Level.SEVERE, "There is still a wrong assignment of ids in the plant editor.");
            System.out.println("There is still a wrong assignment of ids in the plant editor.");
            entry.setId(getNextID());
        }
        try {
            entry = entry.clone();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        plants.put(entry.getId(), entry);
    }

    private int findFirstHoleInIdChain() {
        for (Integer i : plants.keySet()) {
            if (plants.get(i) == null) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Sample model generation
     */
    private void ensureTestData() {
        String pathToVegetableJsonFile = "src/main/resources/vegetables.json"; // for localhost

        if (findAll().isEmpty()) {
            Plant[] fetchedPlants = fetchVegetablesFromJsonFile(pathToVegetableJsonFile);

            if (fetchedPlants != null) {
                for (Plant plant : fetchedPlants) {
                    createNewPlant(plant);
                }
            } else {
                MessageBean.showMessage("Something went wrong while reading the file from: " + pathToVegetableJsonFile);
            }
        }

        setDefaultListHasChanged(false);
    }

    private Plant[] fetchVegetablesFromJsonFile(String pathToVegetableJsonFile) {
        Gson gson = new Gson();
        JsonReader reader = null;
        Plant[] vegetables;
        ClassPathResource res = new ClassPathResource("vegetables.json");


        try {
            System.out.println("Try to open file: " + pathToVegetableJsonFile);
//            reader = new JsonReader(new FileReader(res.getFile())); // this works for localhost server, but not for aws from a jar, because you cant point to a file in a jar like this.
            reader = new JsonReader(new InputStreamReader(res.getInputStream()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (reader != null) {
            System.out.println("SUCCESS.");
            System.out.println("Try to Fetch model.");
            vegetables = gson.fromJson(reader, Plant[].class);
        } else {
            System.err.println("Could not open File!");
            return null;
        }

        if (vegetables != null) {
            System.out.println("SUCCESS\nFetched following model:");
            for (Plant vegetable : vegetables) {
                System.out.println(vegetable.toString());
            }
        } else {
            System.err.println("Could not fetch model!");
            return null;
        }
        return vegetables;
    }

    public String fetchVegetablesToJsonString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(plants.values());
    }

    public void restoreDefaultPlantList() {
        this.plants = new HashMap<>();
        this.ensureTestData();
        this.controller.updateMainList();
    }

    public void restoreFormerPlantList(Plant[] fetchedPlants) {
        if (fetchedPlants == null) {
            restoreDefaultPlantList();
            MessageBean.showMessage("Could not fetch plants from the uploaded file.\nDefault plant list restored.");
        } else {
            this.plants = new HashMap<>();
            for (Plant p : fetchedPlants) {
                createNewPlant(p);
            }
        }
        setDefaultListHasChanged(true);
        this.controller.updateMainList();
    }

    private void createNewPlant(Plant plant) {
        Plant p = new Plant();
        p.setId(plant.getId());
        p.setName(plant.getName());
        p.setGerName(plant.getGerName());
        p.setGoodCompanionIDs(plant.getGoodCompanionIDs());
        p.setBadCompanionIDs(plant.getBadCompanionIDs());
        p.setHeightInCM(plant.getHeightInCM());
        p.setSpreadDiameterInCM(plant.getSpreadDiameterInCM());
        p.setWaterConsumption(plant.getWaterConsumption());
        save(p);
    }

    public void loadPlantListFromClient(Plant[] fetchedPlants) {
        restoreFormerPlantList(fetchedPlants);
        this.controller.updateMainList();
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setDefaultListHasChanged(boolean value) {
        defaultListHasChanged = value;
    }

    public boolean isDefaultListChanged() {
        return defaultListHasChanged;
    }

    public int getNextID() {
        if (nextId == -1) {
            nextId = (Integer) plants.keySet().toArray()[plants.keySet().size() - 1];
        }
        if (nextId == Integer.MAX_VALUE - 1) {
            MessageBean.showMessage("Maximum amount of plants reached, trying to fill up holes in the id chain, this could take a while.");
            int holeID = findFirstHoleInIdChain();
            if (holeID == -1) {
                MessageBean.showMessage("Well there is no space for more plants, get a more bit system!");
            } else {
                return holeID;
            }
        } else {
            nextId++;
        }

        return nextId;
    }
}