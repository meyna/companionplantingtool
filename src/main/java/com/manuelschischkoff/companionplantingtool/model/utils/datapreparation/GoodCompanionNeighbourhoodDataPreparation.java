package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.vaadin.flow.component.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class GoodCompanionNeighbourhoodDataPreparation extends Thread {

    private final Component componentToActivate;
    private final MainController controller;
    private BlockingQueue<PlantOrder> workingQueue = new LinkedBlockingDeque<>();
    private List<Integer> resultPerLayoutIndices;
    private AtomicBoolean running = new AtomicBoolean();
    private boolean firstResult = true;

    // componentToActivate ... the component on which .setEnable(true) should be called when data is rdy.
//    in this case its the optimize button in the bc distance profile header in FieldDrawingAndStatisticsView
    public GoodCompanionNeighbourhoodDataPreparation(Component componentToActivate, MainController controller) {
        this.componentToActivate = componentToActivate;
        this.controller = controller;
        resultPerLayoutIndices = new ArrayList<>();
    }

    @Override
    public void run(){
        running.set(true);
        try {
            while (running.get()) {
                PlantOrder po = workingQueue.take();
                checkIfAllNeighboursAreGoodCompanions(po);
                if (firstResult && resultPerLayoutIndices.size() > 0 && controller.isShowFilterBtnGCNH()) {
                    activateComponent(); // activate ensure gc nh filter button in solutions view
                    firstResult = false;
                    this.controller.setLayoutsRdyToFilter(true);
                }
            }
        } catch (InterruptedException ie) {
            running.set(false);
            Thread.currentThread().interrupt();
        }
    }

    private void checkIfAllNeighboursAreGoodCompanions(PlantOrder po) {
        List<Plant> plants = po.getPlants();
        boolean allValidNeighbours = true;

        for (int i = 0; i < plants.size() - 1; i++) {
            allValidNeighbours = (
                    Arrays.stream(plants.get(i).getGoodCompanionIDs()).boxed().collect(Collectors.toList()).contains(plants.get(i + 1).getId())
                ||
                    Arrays.stream(plants.get(i + 1).getGoodCompanionIDs()).boxed().collect(Collectors.toList()).contains(plants.get(i).getId())
            );
            if(!allValidNeighbours) {
                break;
            }
        }

        if (allValidNeighbours) {
            resultPerLayoutIndices.add(po.getId());
        }
    }

    private void activateComponent() {
        componentToActivate.getElement().setEnabled(true);
    }

    public int[] getResultIndices() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutIndices.stream().mapToInt(i -> i).toArray();
    }

    public void stopCalculations() {
        this.running.set(false);
        this.interrupt();
    }

    public BlockingQueue<PlantOrder> getWorkingQueue() {
        return workingQueue;
    }


    // helper variable
    private List<Integer> filteredIndices;

    public int[] getFilteredResultIndices(List<PlantOrder> filteredOrders) {
        filteredIndices = new ArrayList<>();

        for (int i = 0; i < filteredOrders.size(); i++) {
            for (int j = 0; j < resultPerLayoutIndices.size(); j++) {
                if (resultPerLayoutIndices.get(j).equals(filteredOrders.get(i).getId())) {
                    filteredIndices.add(resultPerLayoutIndices.get(j));
                    break;
                }
            }
        }

        return filteredIndices.stream().mapToInt(i -> i).toArray();
    }
}
