package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.vaadin.flow.component.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

public class WaterDataPreparation extends Thread{

    private final Component componentToActivate;
    private final MainController controller;
    private BlockingQueue<PlantOrder> workingQueue = new LinkedBlockingDeque<>();
    private AtomicBoolean running = new AtomicBoolean();
    private boolean firstResult = true;
    private List<Double> waterCostForWest = new ArrayList<>();
    private List<Double> waterCostForEast = new ArrayList<>();
    private List<Double> waterCostForMiddle = new ArrayList<>();
    private List<Integer> resultPerLayoutIndices = new ArrayList<>();

    // componentToActivate ... the component on which .setEnable(true) should be called when data is rdy.
//    in this case its the optimize button in the water profile header in FieldDrawingAndStatisticsView
    public WaterDataPreparation(Component componentToActivate, MainController controller) {
        this.componentToActivate = componentToActivate;
        this.controller = controller;
    }

    @Override
    public void run(){
        running.set(true);
        try {
            while (running.get()) {
                PlantOrder po = workingQueue.take();
                waterCostForWest.add(calculateWaterCosts(po, FieldPositionValues.West, controller.getSpaceBetweenRows()));
                waterCostForMiddle.add(calculateWaterCosts(po, FieldPositionValues.Middle, controller.getSpaceBetweenRows()));
                waterCostForEast.add(calculateWaterCosts(po, FieldPositionValues.East, controller.getSpaceBetweenRows()));
                resultPerLayoutIndices.add(po.getId());

                if (firstResult && resultPerLayoutIndices.size() > 0 && controller.isShowFilterBtnWaterDistances()) {
                    activateComponent(); // activate optimize button for water supply distance in solutions view
                    firstResult = false;
                    this.controller.setLayoutsRdyToFilter(true);
                }
            }
        } catch (InterruptedException ie) {
            running.set(false);
            Thread.currentThread().interrupt();
        }
    }

    private double calculateWaterCosts(PlantOrder po, FieldPositionValues waterSupplyPos, int spaceBetweenRows) {
        /*
          If the water supply is in the WEST start with measuring the distance from the EAST, use it as multiplication factor and vice versa. (At the end, search for max value)
          If the water supply is in the MIDDLE, start in the EAST AND WEST and check weather they are crossing or not, if so --> abort.
          **/

        // Get all water costs in correct order
        List<Plant> currentPs = po.getPlants();
        int[] waterCostsFromWestToEast = new int[currentPs.size()];

        for (int j = 0; j < currentPs.size(); j++) {
            waterCostsFromWestToEast[j] = currentPs.get(j).getWaterConsumption();
        }

        // calculate costs
        double cost = 0;
        double distance = 0; // in meter
        double multiplier = 1;
        double sBRInM = spaceBetweenRows / 100.0;

        // check different cases of water supply position
        switch (waterSupplyPos) {
            case West:
                for (int j = waterCostsFromWestToEast.length-1; j >= 0; j--) {
                    cost += waterCostsFromWestToEast[j] * multiplier++;
//                    cost += waterCostsFromWestToEast[j] * (distance + (currentPs.get(j).getSpreadDiameterInCM() / 2.0) / 100.0);
//                    distance += (currentPs.get(j).getSpreadDiameterInCM() / 2.0) / 100.0;
//                    distance += sBRInM;
                }
                break;
            case East:
                for (int j = 0; j < waterCostsFromWestToEast.length; j++) {
                    cost += waterCostsFromWestToEast[j] * multiplier++;
//                    cost += waterCostsFromWestToEast[j] * (distance + (currentPs.get(j).getSpreadDiameterInCM() / 2.0) / 100.0);
//                    distance += (currentPs.get(j).getSpreadDiameterInCM() / 2.0) / 100.0;
//                    distance += sBRInM;
                }
                break;
            case Middle:
                int index = 0;
                int reversedIndex = waterCostsFromWestToEast.length-1;
                double reversedDistance = 0;
                while (index < reversedIndex || index == reversedIndex) {
                    cost += waterCostsFromWestToEast[index] * multiplier;
//                    cost += waterCostsFromWestToEast[index] * (distance + (currentPs.get(index).getSpreadDiameterInCM() / 2.0) / 100.0);
//                    distance += (currentPs.get(index).getSpreadDiameterInCM() / 2.0) / 100.0;
//                    distance += sBRInM;

                    if (index != reversedIndex) {
                        cost += waterCostsFromWestToEast[reversedIndex] * multiplier;
//                        cost += waterCostsFromWestToEast[reversedIndex] * (reversedDistance + (currentPs.get(reversedIndex).getSpreadDiameterInCM() / 2.0) / 100.0);
//                        reversedDistance += (currentPs.get(reversedIndex).getSpreadDiameterInCM() / 2.0) / 100.0;
//                        reversedDistance += sBRInM;
                    }

                    index++;
                    reversedIndex--;
                    multiplier++;
                }
                break;
            default:
                // should never be reached!!!
                System.out.println("REACHED DEAD CODE IN Class: WaterDataPreparation! (switch-case default)");
                break;
        }

        return cost;
    }

    private void activateComponent() {
        componentToActivate.getElement().setEnabled(true);
    }

    public double[] getResults(FieldPositionValues forCaseFieldPosValue) {
        if (running.get()) {
            return null;
        }

        switch (forCaseFieldPosValue) {
            case Middle:
                return waterCostForMiddle.stream().mapToDouble(i -> i).toArray();
            case West:
                return waterCostForWest.stream().mapToDouble(i -> i).toArray();
            case East:
                return waterCostForEast.stream().mapToDouble(i -> i).toArray();
            default:
                // should never be reached!!!
                System.out.println("REACHED DEAD CODE IN Class: WaterDataPreparation! (switch-case default)");
                return null;
        }
    }

    public int[] getResultsIndices() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutIndices.stream().mapToInt(i -> i).toArray();
    }

    public void stopCalculations() {
        this.running.set(false);
        this.interrupt();
    }

    public BlockingQueue<PlantOrder> getWorkingQueue() {
        return workingQueue;
    }

    // helper variable
    private List<Integer> filteredIndices;

    public double[] getFilteredResults(FieldPositionValues fieldPositionValue, List<PlantOrder> filteredOrders) {
        List<Double> filteredResults = new ArrayList<>();
        filteredIndices = new ArrayList<>();
        List<Double> correctPosList = null;

        switch (fieldPositionValue) {
            case East:
                correctPosList = waterCostForEast;
                break;
            case West:
                correctPosList = waterCostForWest;
                break;
            case Middle:
                correctPosList = waterCostForMiddle;
                break;
            default:
                // should never be reached!!!
                System.out.println("REACHED DEAD CODE IN Class: WaterDataPreparation! (switch-case default)");
                break;
        }

        if (correctPosList == null) {
            // should never be reached!!!
            System.out.println("REACHED DEAD CODE IN Class: WaterDataPreparation! (if list is null)");
            return null;
        }

        for (int i = 0; i < filteredOrders.size(); i++) {
            for (int j = 0; j < resultPerLayoutIndices.size(); j++) {
                if (resultPerLayoutIndices.get(j).equals(filteredOrders.get(i).getId())) {
                    filteredResults.add(correctPosList.get(j));
                    filteredIndices.add(resultPerLayoutIndices.get(j));
                    break;
                }
            }
        }

        return filteredResults.stream().mapToDouble(i -> i).toArray();
    }

    public int[] getFilteredResultsIndices() {
        return filteredIndices.stream().mapToInt(i -> i).toArray();
    }
}
