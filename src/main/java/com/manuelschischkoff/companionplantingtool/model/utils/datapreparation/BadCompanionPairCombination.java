package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import org.chocosolver.solver.Model;

import java.util.ArrayList;
import java.util.List;

public class BadCompanionPairCombination {

    private MainController controller;
    private Model model;

    private BadCompanionPair badCompanionPair;
    private List<Plant> plantsCombWithBCPair;
    private int distanceBetweenBCPair;
    private int combSize;
    private int spaceBetweenRows;

    public BadCompanionPairCombination(){
    }

    public BadCompanionPairCombination(MainController controller, Model model, BadCompanionPair badCompanionPair, List<Plant> plantsBetweenPair, int spaceBetweenRows) {
        this.controller = controller;
        this.model = model;
        this.badCompanionPair = badCompanionPair;
        this.plantsCombWithBCPair = new ArrayList<>();
        this.plantsCombWithBCPair.add(controller.getPlantService().find(badCompanionPair.getFirstID()));
        this.plantsCombWithBCPair.addAll(plantsBetweenPair);
        this.plantsCombWithBCPair.add(controller.getPlantService().find(badCompanionPair.getSecondID()));
        this.spaceBetweenRows = spaceBetweenRows;

        calculateStats();
    }

    public void calculateStats() {
        combSize = plantsCombWithBCPair.size();

        int distance = spaceBetweenRows;

        for (int i = 1; i < plantsCombWithBCPair.size() - 1; i++) {
            distance += plantsCombWithBCPair.get(i).getSpreadDiameterInCM() + spaceBetweenRows;
        }

        distanceBetweenBCPair = distance;
//        System.out.println("PlantComb: " + plantsCombWithBCPair + ", Distance = " + distance);
    }

    public MainController getController() {
        return controller;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public BadCompanionPair getBadCompanionPair() {
        return badCompanionPair;
    }

    public void setBadCompanionPair(BadCompanionPair badCompanionPair) {
        this.badCompanionPair = badCompanionPair;
    }

    public List<Plant> getPlantsCombWithBCPair() {
        return plantsCombWithBCPair;
    }

    public void setPlantsCombWithBCPair(List<Plant> plantsCombWithBCPair) {
        this.plantsCombWithBCPair = plantsCombWithBCPair;
    }

    public int getDistanceBetweenBCPair() {
        return distanceBetweenBCPair;
    }

    public void setDistanceBetweenBCPair(int distanceBetweenBCPair) {
        this.distanceBetweenBCPair = distanceBetweenBCPair;
    }

    public int getCombSize() {
        return combSize;
    }

    public void setCombSize(int combSize) {
        this.combSize = combSize;
    }

    public int getSpaceBetweenRows() {
        return spaceBetweenRows;
    }

    public void setSpaceBetweenRows(int spaceBetweenRows) {
        this.spaceBetweenRows = spaceBetweenRows;
    }
}
