package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.vaadin.flow.component.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public class BadCompanionDistanceDataPreparation extends Thread {

    private final Component componentToActivate;
    private final MainController controller;
    private BlockingQueue<PlantOrder> workingQueue = new LinkedBlockingDeque<>();
    private final int spaceBetweenRows;
    private List<Integer> resultPerLayoutIndices;
    private List<Integer> resultPerLayoutValues;
    private AtomicBoolean running = new AtomicBoolean();
    private boolean firstResult = true;

    // componentToActivate ... the component on which .setEnable(true) should be called when data is rdy.
//    in this case its the optimize button in the bc distance profile header in FieldDrawingAndStatisticsView
    public BadCompanionDistanceDataPreparation(Component componentToActivate, int spaceBetweenRows, MainController controller) {
        this.componentToActivate = componentToActivate;
        this.spaceBetweenRows = spaceBetweenRows;
        this.controller = controller;
        resultPerLayoutIndices = new ArrayList<>();
        resultPerLayoutValues = new ArrayList<>();
    }

    @Override
    public void run(){
        running.set(true);
        try {
            while (running.get()) {
                PlantOrder po = workingQueue.take();
                calculateAverageBCDsPerPlantOrder(po);
                if (firstResult && resultPerLayoutIndices.size() > 0 && controller.isShowFilterBtnBCDistances()) {
                    activateComponent(); // activate optimize button for bc distances in solutions view
                    firstResult = false;
                    this.controller.setLayoutsRdyToFilter(true);
                }
            }
        } catch (InterruptedException ie) {
            running.set(false);
            Thread.currentThread().interrupt();
        }
    }

    private void calculateAverageBCDsPerPlantOrder(PlantOrder po) {
        int avrgDist = 0;
        int nrOfPlantsWithBCs = 0;

        List<Plant> currentPlants = po.getPlants();
        for (int j = 0; j < currentPlants.size(); j++) {
            Plant currentP = currentPlants.get(j);
            int perPlantavrg = 0;
            int nrOfBCs = 0;

            for (int k = 0; k < currentPlants.size(); k++) {
                if (j == k) {
                    continue;
                }

                int finalK = k;
                // if a plant k occurs in a plant j's bad companions list
                if (IntStream.of(currentP.getBadCompanionIDs()).anyMatch(n -> n == currentPlants.get(finalK).getId())) {
                    perPlantavrg += calculateDistanceBetweenIndices(currentPlants, Math.min(j,k), Math.max(j,k));
                    nrOfBCs++;
                }
            }

            if (nrOfBCs > 0) {
                perPlantavrg = Math.floorDiv(perPlantavrg, nrOfBCs);
                avrgDist += perPlantavrg;
                nrOfPlantsWithBCs++;
            }
        }

        if (nrOfPlantsWithBCs > 0) {
            avrgDist = Math.floorDiv(avrgDist, nrOfPlantsWithBCs);
        }

        resultPerLayoutIndices.add(po.getId());
        resultPerLayoutValues.add(avrgDist);
    }

    private int calculateDistanceBetweenIndices(List<Plant> plants, int indexA, int indexB) {
        int sum = spaceBetweenRows;

        for (int i = indexA+1; i < indexB; i++) {
            sum += (plants.get(i).getSpreadDiameterInCM() + spaceBetweenRows);
        }

        return sum;
    }

    private void activateComponent() {
        componentToActivate.getElement().setEnabled(true);
    }

    public int[] getResults() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutValues.stream().mapToInt(i -> i).toArray();
    }

    public int[] getResultsIndices() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutIndices.stream().mapToInt(i -> i).toArray();
    }

    public void stopCalculations() {
        this.running.set(false);
        this.interrupt();
    }

    public BlockingQueue<PlantOrder> getWorkingQueue() {
        return workingQueue;
    }


    // helper variable
    private List<Integer> filteredIndices;

    public int[] getFilteredResults(List<PlantOrder> filteredOrders) {
        List<Integer> filteredResults = new ArrayList<>();
        filteredIndices = new ArrayList<>();

        for (int i = 0; i < filteredOrders.size(); i++) {
            for (int j = 0; j < resultPerLayoutIndices.size(); j++) {
                if (resultPerLayoutIndices.get(j).equals(filteredOrders.get(i).getId())) {
                    filteredResults.add(resultPerLayoutValues.get(j));
                    filteredIndices.add(resultPerLayoutIndices.get(j));
                    break;
                }
            }
        }

        return filteredResults.stream().mapToInt(i -> i).toArray();
    }

    public int[] getFilteredResultsIndices() {
        return filteredIndices.stream().mapToInt(i -> i).toArray();
    }
}
