package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.model.Plant;

import java.util.List;

public class BadCompanionPair {

    private Plant p1;
    private Plant p2;
    private int firstID;
    private int secondID;
    private List<BadCompanionPairCombination> allCombs;

    public BadCompanionPair(Plant p1, Plant p2) {
        this.p1 = p1;
        this.p2 = p2;
        this.firstID = p1.getId();
        this.secondID = p2.getId();
    }

    public List<BadCompanionPairCombination> getAllCombs() {
        return allCombs;
    }

    public void setAllCombs(List<BadCompanionPairCombination> allCombs) {
        this.allCombs = allCombs;
    }

    public int[] getPairArray() {
        return new int[]{firstID, secondID};
    }

    public int getFirstID() {
        return firstID;
    }

    public void setFirstID(int firstID) {
        this.firstID = firstID;
    }

    public int getSecondID() {
        return secondID;
    }

    public void setSecondID(int secondID) {
        this.secondID = secondID;
    }

    public Plant getP1() {
        return p1;
    }

    public void setP1(Plant p1) {
        this.p1 = p1;
    }

    public Plant getP2() {
        return p2;
    }

    public void setP2(Plant p2) {
        this.p2 = p2;
    }
}
