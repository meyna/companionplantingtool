package com.manuelschischkoff.companionplantingtool.model.utils.datapreparation;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.vaadin.flow.component.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

@SuppressWarnings("Duplicates")
public class HeightDataPreparation extends Thread {

    private Component componentToActivate;
    private MainController controller;
    private BlockingQueue<PlantOrder> workingQueue = new LinkedBlockingDeque<>();
    private int spaceBetweenRows;
    private List<Integer> resultPerLayoutIndices;
    private List<Integer> resultPerLayoutValues;
    private AtomicBoolean running = new AtomicBoolean();
    private boolean firstResult = true;


    // componentToActivate ... the component on which .setEnable(true) should be called when data is rdy.
//    in this case its the optimize button in the height profile header in FieldDrawingAndStatisticsView.java
    public HeightDataPreparation(Component componentToActivate, int spaceBetweenRows, MainController controller) {
        this.componentToActivate = componentToActivate;
        this.spaceBetweenRows = spaceBetweenRows;
        this.controller = controller;
        resultPerLayoutIndices = new ArrayList<>();
        resultPerLayoutValues = new ArrayList<>();
    }

    @Override
    public void run(){
        running.set(true);
        try {
            while (running.get()) {
                PlantOrder po = workingQueue.take();
                calculateAmountOfSunData(po);
                if (firstResult && resultPerLayoutIndices.size() > 0 && controller.isShowFilterBtnAmountOfSun()) {
                    activateComponent(); // activate optimize button for amount of sun in solutions view
                    firstResult = false;
                    this.controller.setLayoutsRdyToFilter(true);
                }
            }
        } catch (InterruptedException ie) {
            running.set(false);
            Thread.currentThread().interrupt();
        }
    }


    // calculate the circumferences per plant which are exposed to the sun, according to their neighbor plants in the layouts.
    // the goal is to find two plants which have the highest and lowest value, find the difference and minimize it.
    // with this approach it will be prevented that small plants being stuck between huge plants (as it would happen, if
    // a regular average calculation is used).
    private void calculateAmountOfSunData(PlantOrder po) {
        // Get the plants of the current layout
        List<Plant> currentPs = po.getPlants();

        // now calculate the circumference parts that are exposed to the sun, considering the neighbor plants
        int[] sunHoursPerPlant = new int[currentPs.size()];
        for (int j = 0; j < currentPs.size(); j++) {
            // old approach
//            double east = calculateTheAmountOfSunFromEast(currentPs, j);
//            double west = calculateTheAmountOfSunFromWest(currentPs, j);
//            double above = calculateTheAmountOfSunFromAbove(currentPs, j);
            // new approach
            double east = calculateTheEasternPart(currentPs, j);
            double west = calculateTheWesternPart(currentPs, j);
            double above = calculateTheTopPart(currentPs, j);  // NEEDS TO BE CALCULATED AT THE END (see top multiplicators)
            sunHoursPerPlant[j] = (int) Math.round(east + west + above);
        }

        // find the min and max circumferences and calculate the difference, which will then be minimized
        int minMaxDiff = calculateTheAmountOfSunDifferenceOfMaxMin(sunHoursPerPlant);
//        po.setOptimizationValue(minMaxDiff);
        resultPerLayoutIndices.add(po.getId());
        resultPerLayoutValues.add(minMaxDiff);
    }

    // indicator fort the top left, middle and top right part of the spread
    // 0 means there is a too high plant near the current plant so the corresponding part of the spread will not be
    // exposed to the sun.
    // at the end the sum of this array divided by 3 will be multiplied with the spread of the corresponding plant
    private int[] topMultiplicators = {0, 1, 0};

    // the top multiplicators will be calculated during the western and eastern parts.
    private double calculateTheTopPart(List<Plant> currentPs, int j) {
        return currentPs.get(j).getSpreadDiameterInCM() * IntStream.of(topMultiplicators).sum() / 3.0;
    }

    private double calculateTheWesternPart(List<Plant> currentPs, int j) {
        // the height of the current plant
        double amount = currentPs.get(j).getHeightInCM();

        if (j > 0) {
            // the height difference between the left neighbor and the current plant.
            // if we subtract the height from the neighbor of the current plant we gain 2 information parts:
            //      - the height difference of course
            //      - and if the difference is negative  -> the neighbor plant is higher than the current
            amount -= currentPs.get(j-1).getHeightInCM();

            // if the neighbor plant is higher
            if (amount < 0.0) {
                // and the difference is bigger than the space between two different plants
                if (amount <= -spaceBetweenRows) {
                    // the top left part on this side = 0
                    topMultiplicators[0] = 0;
                    // to weight the negative effect of small plants with small diameters standing close to tall plants
                    // the side amount is -1
                    amount = -1.0;
                }
                // if the space between rows is bigger than the height difference
                else {
                    // the top left part is 1
                    topMultiplicators[0] = 1;
                    // and the side part 0
                    amount = 0.0;
                }
            }
            // if the amount is bigger than 0 this means the current plant is higher than the left neighbor plant.
            // that means, we need to find other plants in the layout, which are placed left of the current, which are
            // bigger than the current (those may block the sun from the left side to the left part of the current circumference)
            else {
                int indexOfMaxLeft = j-1;
                // take the left neighbor as the current maximum height
                double currentMaxHeight = currentPs.get(j-1).getHeightInCM();

                // iterate from the left neighbor towards the left end of the layout, because if there are plants of
                // same height left of the current, the one closer to the current plant should be taken into account.
                for (int i = j-1; i >= 0; i--) {
                    if (currentMaxHeight < currentPs.get(i).getHeightInCM()) {
                        currentMaxHeight = currentPs.get(i).getHeightInCM();
                        indexOfMaxLeft = i;
                    }
                }

                // if it is j-1 there is no higher plant thant the one left of the current --> amount is already valid
                if (indexOfMaxLeft != j-1){
                    // otherwise calculate the deltaX (the width difference to the tallest plant left of the current).
                    double deltaX = spaceBetweenRows;

                    for (int i = j-1; i > indexOfMaxLeft; i--) {
                        deltaX += currentPs.get(i).getSpreadDiameterInCM();
                        deltaX += spaceBetweenRows;
                    }

                    // convert the deltaX into the negative pendant to compare it with the height difference
                    // (which is in this if clause negative or zero)
                    deltaX *= -1.0;

                    // get the height difference of the tallest plant to the left and the current in the same way as
                    // it was done with the left neighbor.
                    double deltaYMaxLeft = currentPs.get(j).getHeightInCM() - currentMaxHeight;

                    // if the height difference is less or equal to the width difference
                    if (deltaYMaxLeft <= deltaX) {
                        // the left side of the circumference is 0
                        amount = 0.0;
                        // and the top left side as well, this will weight the effect of tall plants close to smaller ones
                        topMultiplicators[0] = 0;
                    }
                    // if even the doubled height difference is less than the width difference, this will ...
                    else if (2.0 * deltaYMaxLeft <= deltaX) {
                        // ... only reduce the left part of the circumference by half, but ...
                        amount /= 2.0;
                        // ... don't affect the top part.
                        topMultiplicators[0] = 1;
                    }
                    // if the height difference is positive, this means the current plant is taller than the tallest on
                    // the left side of the current in the layout.
                    // therefore the amount is already valid
                    // (current plant height - left neighbor height = left part of the circumference )
                    else if (deltaYMaxLeft > 0.0) {
                        // the top left part is of course 1 if the current plant is the tallest to the left part of the layout
                        topMultiplicators[0] = 1;
                    }
                }
            }
        }

        return amount;
    }

    // the right side fo the circumference, for further documentation see the left part above.
    private double calculateTheEasternPart(List<Plant> currentPs, int j) {
        double amount = currentPs.get(j).getHeightInCM();

        if (j < currentPs.size()-1) {
            amount -= currentPs.get(j+1).getHeightInCM();

            if (amount < 0.0) {
                if (amount <= -spaceBetweenRows) {
                    topMultiplicators[2] = 0;
                    amount = -1.0;
                } else {
                    topMultiplicators[2] = 1;
                    amount = 0.0;
                }
            } else {
                int indexOfMaxRight = j+1;
                double currentMaxHeight = currentPs.get(j+1).getHeightInCM(); // to skip plants with same height or lower than the one on the right of the current

                for (int i = j+1; i < currentPs.size(); i++) {
                    if (currentMaxHeight < currentPs.get(i).getHeightInCM()) {
                        currentMaxHeight = currentPs.get(i).getHeightInCM();
                        indexOfMaxRight = i;
                    }
                }

                if (indexOfMaxRight != j+1){ // if it is j+1 there is no higher plant than the one to the left of the current --> amount is already valid
                    double deltaX = spaceBetweenRows;

                    for (int i = j+1; i < indexOfMaxRight; i++) {
                        deltaX += currentPs.get(i).getSpreadDiameterInCM();
                        deltaX += spaceBetweenRows;
                    }

                    deltaX *= -1.0;

                    double deltaYMaxRight = currentPs.get(j).getHeightInCM() - currentMaxHeight;

                    if (deltaYMaxRight <= deltaX) {
                        amount = 0.0;
                        topMultiplicators[2] = 0;
                    }
                    else if (2.0 * deltaYMaxRight <= deltaX) {
                        amount /= 2.0;
                        topMultiplicators[2] = 1;
                    }
                    else if (deltaYMaxRight > 0.0) {
                        topMultiplicators[2] = 1;
                    }
                }
            }
        }

        return amount;
    }

    // find the difference between the plant with the maximum amount of sun ant the one with the minimum amount of sun
    private int calculateTheAmountOfSunDifferenceOfMaxMin(int[] sunHoursPerPlant) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < sunHoursPerPlant.length; i++) {
            if (sunHoursPerPlant[i] > max) {
                max = sunHoursPerPlant[i];
            }

            if (sunHoursPerPlant[i] < min) {
                min = sunHoursPerPlant[i];
            }
        }

        return max - min;
    }

    private void activateComponent() {
        componentToActivate.getElement().setEnabled(true);
    }

    public int[] getResults() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutValues.stream().mapToInt(i -> i).toArray();
    }

    public int[] getResultsIndices() {
        if (running.get()) {
            return null;
        }
        return resultPerLayoutIndices.stream().mapToInt(i -> i).toArray();
    }

    public void stopCalculations() {
        this.running.set(false);
        this.interrupt();
    }

    public BlockingQueue<PlantOrder> getWorkingQueue() {
        return workingQueue;
    }


    // helper variable
    private List<Integer> filteredIndices;

    public int[] getFilteredResults(List<PlantOrder> filteredOrders) {
        List<Integer> filteredResults = new ArrayList<>();
        filteredIndices = new ArrayList<>();

        for (int i = 0; i < filteredOrders.size(); i++) {
            for (int j = 0; j < resultPerLayoutIndices.size(); j++) {
                if (resultPerLayoutIndices.get(j).equals(filteredOrders.get(i).getId())) {
                    filteredResults.add(resultPerLayoutValues.get(j));
                    filteredIndices.add(resultPerLayoutIndices.get(j));
                    break;
                }
            }
        }

        return filteredResults.stream().mapToInt(i -> i).toArray();
    }

    public int[] getFilteredResultsIndices() {
        return filteredIndices.stream().mapToInt(i -> i).toArray();
    }

    // FOR THE OLD APPROACH
/*
    public int calculateAMountOfSunDataFor(PlantOrder plantOrder) {
        // Get the plants of the current layout
        List<Plant> currentPs = plantOrder.getPlants();

        // now calculate the amount of sun received by each plant
        int[] sunHoursPerPlant = new int[currentPs.size()];
        for (int j = 0; j < currentPs.size(); j++) {
            double east = calculateTheAmountOfSunFromEast(currentPs, j);
            double west = calculateTheAmountOfSunFromWest(currentPs, j);
            double above = calculateTheAmountOfSunFromAbove(currentPs, j);
            sunHoursPerPlant[j] = (int) Math.round(east + west + above);
        }

        return calculateTheAmountOfSunDifferenceOfMaxMin(sunHoursPerPlant);
    }

    // calculating the amount of sun from east means, check for higher plants from index until last entry
    private double calculateTheAmountOfSunFromEast(List<Plant> currentPlants, int index) {
        double amountOfSun = 0;
        int heightOfCurrentP = currentPlants.get(index).getHeightInCM();

        // if index is the last entry
        if (index == currentPlants.size()-1) {
            amountOfSun = heightOfCurrentP * 3;
        }
        // if there is next to the current plant immediately a higher plant
        else if (heightOfCurrentP < currentPlants.get(index + 1).getHeightInCM()) {
            amountOfSun = 0;
        }
        // check if there are plants in the east which are higher than the current plant, if there are at least 2 more plants in the east
        else if (index < currentPlants.size()-2){
            int indexOfHeigherPlant = -1;
            int heightOfHighestPlantInTheEast = currentPlants.get(index+1).getHeightInCM();

            for (int i = index + 2; i < currentPlants.size(); i++) {
                if (currentPlants.get(i).getHeightInCM() > heightOfCurrentP && indexOfHeigherPlant == -1) {
                    indexOfHeigherPlant = i;
                }
                if (currentPlants.get(i).getHeightInCM() > heightOfHighestPlantInTheEast) {
                    heightOfHighestPlantInTheEast = currentPlants.get(i).getHeightInCM();
                }
            }

            // if there is a higher plant int the east
            if (indexOfHeigherPlant != -1) {
                // get  the deltaY of the current plant to its immediate neighbour
                int deltaYImmediate = heightOfCurrentP - currentPlants.get(index+1).getHeightInCM();

                // check the ratio of deltaY and deltaX
                int deltaY = currentPlants.get(indexOfHeigherPlant).getHeightInCM() - heightOfCurrentP;
                int deltaX = calculateDeltaX(currentPlants, index, indexOfHeigherPlant);

                // if deltaY is less than deltaX, calculate the amount of sun by taking the height of the current plant
                // not covered by the immediate neighbour, multiply it by the deltas ratio and the amount of sun from the east 3 hours.
                if (deltaY < deltaX) {
                    amountOfSun = deltaYImmediate * (deltaY / deltaX) * 3;
                }
                // if deltaY is bigger than deltaX, there will be too much sun blocked => 0
                else {
                    amountOfSun = 0;
                }
            }
            // if not, then just get deltaY of current plant and the highest plant in the east times 3
            else {
                amountOfSun = (heightOfCurrentP - heightOfHighestPlantInTheEast) * 3;
            }
        }
        return amountOfSun;
    }

    // calculating the amount of sun from west means, check for higher plants from entry 0 to index
    private double calculateTheAmountOfSunFromWest(List<Plant> currentPlants, int index) {
        double amountOfSun = 0;
        int heightOfCurrentP = currentPlants.get(index).getHeightInCM();

        // if index is the first entry
        if (index == 0) {
            amountOfSun = heightOfCurrentP * 3;
        }
        // if there is next to the current plant immediately a higher plant
        else if (heightOfCurrentP < currentPlants.get(index - 1).getHeightInCM()) {
            amountOfSun = 0;
        }
        // check if there are plants in the west which are higher than the current plant, if there are at least 2 more plants in the west
        else if (index > 1){
            int indexOfHeigherPlant = -1;
            int heightOfHighestPlantInTheEast = currentPlants.get(index-1).getHeightInCM();

            // we need the first plant which is higher, so we have to iterate from the current index to entry 0
            for (int i = index - 2; i >= 0; i--) {
                if (currentPlants.get(i).getHeightInCM() > heightOfCurrentP && indexOfHeigherPlant == -1) {
                    indexOfHeigherPlant = i;
                }
                if (currentPlants.get(i).getHeightInCM() > heightOfHighestPlantInTheEast) {
                    heightOfHighestPlantInTheEast = currentPlants.get(i).getHeightInCM();
                }
            }

            // if there is a higher plant int the east
            if (indexOfHeigherPlant != -1) {
                // get  the deltaY of the current plant to its immediate neighbour
                int deltaYImmediate = heightOfCurrentP - currentPlants.get(index-1).getHeightInCM();

                // check the ratio of deltaY and deltaX
                int deltaY = currentPlants.get(indexOfHeigherPlant).getHeightInCM() - heightOfCurrentP;
                int deltaX = calculateDeltaX(currentPlants, index, indexOfHeigherPlant);

                // if deltaY is less than deltaX, calculate the amount of sun by taking the height of the current plant
                // not covered by the immediate neighbour, multiply it by the deltas ratio and the amount of sun from the east 3 hours.
                if (deltaY < deltaX) {
                    amountOfSun = deltaYImmediate * (deltaY / deltaX) * 3;
                }
                // if deltaY is bigger than deltaX, there will be too much sun blocked => 0
                else {
                    amountOfSun = 0;
                }
            }
            // if not, then just get deltaY of current plant and the highest plant in the east times 3
            else {
                amountOfSun = (heightOfCurrentP - heightOfHighestPlantInTheEast) * 3;
            }
        }
        return amountOfSun;
    }

    // calculating the amount of sun from above means, check for higher plants in east and west,
    // check the deltaX and deltaY according to that value multiply with 6/4 of sun,
    // plus the amount from directly above which refer to the spread of the plant and 6/2 of sun
    private double calculateTheAmountOfSunFromAbove(List<Plant> currentPlants, int index) {
        double amountOfSun = 0;
        Plant currentP = currentPlants.get(index);
//        int heightOfCurrentP = currentP.getHeightInCM();

        // half of the sun from above, 3 hours, is coming straight from above
        amountOfSun += currentP.getSpreadDiameterInCM() * (6.0/2.0);

        double[] deltaRatios = calculateAllDeltaRatios(currentPlants, currentP, index);

        // now check if there is a plant to the east of the current plant which has a ratio deltaY/deltaX >= 2, or 1 < deltaratio < 2 or less
        boolean foundPlantWithRatioGreaterThan2 = false;
        int indexOfFirstMatch = -1;
        for (int i = index; i < deltaRatios.length; i++) {
            if (deltaRatios[i] >= 2) {
                foundPlantWithRatioGreaterThan2 = true;
                // no sun hours added from above-east
                break;
            }
            if (indexOfFirstMatch == -1 && 1 < deltaRatios[i] && deltaRatios[i] < 2) {
                indexOfFirstMatch = i;
            }
        }
        if (!foundPlantWithRatioGreaterThan2 && indexOfFirstMatch != -1) {
            // there is a quit high plant to the east of the current, so calculate the amount of sun from above-east concerning the deltaRatios
            // a fourth of the sun from above times the spread and the deltaratio -1
            amountOfSun += currentP.getSpreadDiameterInCM() * (6.0/4.0) * (deltaRatios[indexOfFirstMatch] - 1); // -1 because else the ratio as a multiplier would increase the amount of sun instead of decrease
        }
        else if (!foundPlantWithRatioGreaterThan2 && indexOfFirstMatch == -1) {
            // there is no plant which is remarkable higher than the current, so get full sun from above-east
            amountOfSun += currentP.getSpreadDiameterInCM() * (6.0/4.0);
        }

        // now check if there is a plant to the west of the current plant which has a ratio deltaY/deltaX >= 2, or 1 < deltaratio < 2 or less
        foundPlantWithRatioGreaterThan2 = false;
        indexOfFirstMatch = -1;
        for (int i = index-1; i >= 0; i--) {
            if (deltaRatios[i] >= 2) {
                foundPlantWithRatioGreaterThan2 = true;
                // no sun hours added from above-west
                break;
            }
            if (indexOfFirstMatch == -1 && 1 < deltaRatios[i] && deltaRatios[i] < 2) {
                indexOfFirstMatch = i;
            }
        }
        if (!foundPlantWithRatioGreaterThan2 && indexOfFirstMatch != -1) {
            // there is a quit high plant to the west of the current, so calculate the amount of sun from above-west concerning the deltaRatios
            // a fourth of the sun from above times the spread and the deltaratio -1
            amountOfSun += currentP.getSpreadDiameterInCM() * (6.0/4.0) * (deltaRatios[indexOfFirstMatch] - 1); // -1 because else the ratio as a multiplier would increase the amount of sun instead of decrease
        }
        else if (!foundPlantWithRatioGreaterThan2 && indexOfFirstMatch == -1) {
            // there is no plant which is remarkable higher than the current, so get full sun from above-west
            amountOfSun += currentP.getSpreadDiameterInCM() * (6.0/4.0);
        }

        return amountOfSun;
    }

    private double[] calculateAllDeltaRatios(List<Plant> currentPlants, Plant currentP, int index) {
        double[] ratios = new double[currentPlants.size()];
        ratios[index] = 0;

        for (int i = 0; i < currentPlants.size(); i++) {
            if (i != index) {
                double deltaY = currentPlants.get(i).getHeightInCM() - currentP.getHeightInCM();
                double deltaX = *//*Math.max(*//*calculateDeltaX(currentPlants, index, i)*//*, 1)*//*;      // div by 0 maybe still possible!!!
                ratios[i] = deltaY / deltaX;
            }
        }

        return ratios;
    }

    // delta x between 2 plants in a list of plants including the spaceBetweenRows
    private int calculateDeltaX(List<Plant> currentPlants, int index, int indexOfOtherPlant) {
        int deltaX = spaceBetweenRows;
        int minIndex = Math.min(index, indexOfOtherPlant);
        int maxIndex = Math.max(index, indexOfOtherPlant);

        for (int i = minIndex + 1; i < maxIndex; i++) {
            deltaX += currentPlants.get(i).getSpreadDiameterInCM() + spaceBetweenRows;
        }

        return deltaX;
    }*/
}
