package com.manuelschischkoff.companionplantingtool.model.utils;

import com.vaadin.flow.component.notification.Notification;
import org.springframework.stereotype.Service;

@Service
public class MessageBean {

    private static int showMsgDurationInSeconds = 5;

    public static void showMessage(String msg) {
        Notification.show(msg, showMsgDurationInSeconds * 1000, Notification.Position.BOTTOM_END);
    }

    public static void showMessage(String msg, int msgDurationInSeconds) {
        Notification.show(msg, msgDurationInSeconds * 1000, Notification.Position.BOTTOM_END);
    }
}
