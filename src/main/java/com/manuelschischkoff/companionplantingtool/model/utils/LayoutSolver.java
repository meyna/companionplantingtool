package com.manuelschischkoff.companionplantingtool.model.utils;

import com.manuelschischkoff.companionplantingtool.controller.MainController;
import com.manuelschischkoff.companionplantingtool.model.FieldPositionValues;
import com.manuelschischkoff.companionplantingtool.model.PlantOrder;
import com.manuelschischkoff.companionplantingtool.model.Plant;
import com.manuelschischkoff.companionplantingtool.model.Solution;
import com.manuelschischkoff.companionplantingtool.presenter.FieldSolutionView;
import com.manuelschischkoff.companionplantingtool.model.constraintencodings.ModelerAndSolver;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.BadCompanionDistanceDataPreparation;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.GoodCompanionNeighbourhoodDataPreparation;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.HeightDataPreparation;
import com.manuelschischkoff.companionplantingtool.model.utils.datapreparation.WaterDataPreparation;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("FieldCanBeLocal")
public class LayoutSolver {

    // VARIABLES

    private MainController controller;

    private List<Plant> selectedPlants;
    private final HashMap<Integer, Integer> plantRowLimitations;
    private final boolean maxBadCompanionsDistance;
    private final boolean maxAmountOfSun;
    private final boolean optimizeWaterDistances;
    private final boolean ensureGCNH;
    private final int northSouthDistance;
    private final int spaceBetweenRows;
    private Solution solution;
    private List<PlantOrder> plantOrders;

    private HashMap<Integer, Plant> idToPlantMap = new HashMap<>();
    private HashMap<Integer, Integer> idToAmountMap = new HashMap<>();
    private List<Integer> selectedPlantIdsDomain = new ArrayList<>();
    @SuppressWarnings("unused")
    private int[] wantedVegIDs;

    private ModelerAndSolver modelerAndSolver;

    // for filtering
    private List<PlantOrder> initialPlantOrders;
    private List<PlantOrder> filteredOrders;

    private HeightDataPreparation hdPrep;
    private int[] heightData;
    private int[] heightDataIndices;

    private WaterDataPreparation wdPrep;
    private double[] waterData;
    private int[] waterDataIndices;
    @SuppressWarnings("unused")
    private FieldPositionValues fieldPositionValue;

    private BadCompanionDistanceDataPreparation bcdPrep;
    private int[] bcData;
    private int[] bcDataIndices;

    private GoodCompanionNeighbourhoodDataPreparation gcnhdPrep;
    private int[] gcDataIndices;

    private final FieldSolutionView fieldSolutionView;
    private boolean findAllOptimalSolutions = false;
    @SuppressWarnings("unused")
    private int currentOptimumValue;

    // CONSTRUCTOR

    public LayoutSolver(MainController controller, List<Plant> selectedPlants, HashMap<Integer, Integer> plantRowLimitations, boolean maxBadCompanionsDistance, boolean maxAmountOfSun, boolean optimizedWaterDistances, @SuppressWarnings("unused") FieldPositionValues waterSupplyPos, boolean ensureGCNH, int northSouthDistance, int spaceBetweenRows, FieldSolutionView fieldSolutionView) {
        this.controller = controller;

        this.selectedPlants = selectedPlants;
        this.plantRowLimitations = plantRowLimitations;
        this.maxBadCompanionsDistance = maxBadCompanionsDistance;
        this.maxAmountOfSun = maxAmountOfSun;
        this.optimizeWaterDistances = optimizedWaterDistances;
        this.ensureGCNH = ensureGCNH;
        this.northSouthDistance = northSouthDistance;
        this.spaceBetweenRows = spaceBetweenRows;
        this.fieldSolutionView = fieldSolutionView;

        // prepare input data
        // find the domain without duplicates, a map from plant id to the plant and a map from id to the amount of occurrences
        selectedPlants.forEach(plant -> {
            if (idToAmountMap.get(plant.getId()) == null) {
                idToPlantMap.put(plant.getId(), plant);
                selectedPlantIdsDomain.add(plant.getId());
                idToAmountMap.put(plant.getId(), 1);
            }
            // if the plant id already exists in the id to amount map, just increase the plant amount
            else {
                idToAmountMap.replace(plant.getId(), idToAmountMap.get(plant.getId()) +1);
            }
        });
        // define the domain of the fieldLayout IntVar[]
        wantedVegIDs = selectedPlantIdsDomain.stream().mapToInt(i -> i).toArray();
    }

    public void solve() {
        solution = new Solution();
        calculateFieldDetails();
        plantOrders = new ArrayList<>();
        initialPlantOrders = new ArrayList<>();
        solution.setPlantOrders(plantOrders);

        controller.setLayoutsRdyToFilter(false);
        controller.showSolutions(solution, true);

        boolean anyOptimization = maxAmountOfSun || optimizeWaterDistances ||maxBadCompanionsDistance || ensureGCNH;

        // The background threads to calculate data for optimizations on resulting layouts in a filter kind of processing afterwards.
        // only do this, if there is no optimization from the beginning.
        if (!maxAmountOfSun && !anyOptimization) {
            controller.setShowFilterBtnAmountOfSun(true);
            hdPrep = new HeightDataPreparation(fieldSolutionView.getHeightOptBtn(), spaceBetweenRows, this.controller);
            hdPrep.start();
        } else {
            controller.setShowFilterBtnAmountOfSun(false);
        }

        if (!optimizeWaterDistances && !anyOptimization) {
            controller.setShowFilterBtnWaterDistances(true);
            wdPrep = new WaterDataPreparation(fieldSolutionView.getWaterOptBtn(), this.controller);
            wdPrep.start();
        } else {
            controller.setShowFilterBtnWaterDistances(false);
        }

        if (!maxBadCompanionsDistance && !anyOptimization) {
            controller.setShowFilterBtnBCDistances(true);
            bcdPrep = new BadCompanionDistanceDataPreparation(fieldSolutionView.getBcOptBtn(), spaceBetweenRows, this.controller);
            bcdPrep.start();
        } else {
            controller.setShowFilterBtnBCDistances(false);
        }

        if (!ensureGCNH && !anyOptimization) {
            controller.setShowFilterBtnGCNH(true);
            gcnhdPrep = new GoodCompanionNeighbourhoodDataPreparation(fieldSolutionView.getGcOptBtn(), this.controller);
            gcnhdPrep.start();
        } else {
            controller.setShowFilterBtnGCNH(false);
        }

        modelerAndSolver = new ModelerAndSolver(controller, this, selectedPlants, plantRowLimitations, idToPlantMap, spaceBetweenRows, fieldSolutionView, hdPrep, wdPrep, bcdPrep, gcnhdPrep);

        //noinspection StatementWithEmptyBody
        if (findAllOptimalSolutions) {
            // All optimal solutions did not work as suggested by the library, not yet implemented own solution
//            modelerAndSolver.findAllOptimalSolutions(currentOptimumValue);
        }

        modelerAndSolver.start();
    }

    public void stopCalculations() {
        modelerAndSolver.stopCalculations();
        plantOrders = modelerAndSolver.getPlantOrders();

        if (hdPrep != null) {
            hdPrep.stopCalculations();
        }
        if (wdPrep != null) {
            wdPrep.stopCalculations();
        }
        if (bcdPrep != null) {
            bcdPrep.stopCalculations();
        }
        if (gcnhdPrep != null) {
            gcnhdPrep.stopCalculations();
        }
    }

    private int fieldWidth = 0; // for some reasons we need a field variable for usage in lambdas

    private void calculateFieldDetails() {
        StringBuilder sb = new StringBuilder();

        selectedPlants.forEach(plant -> {
            fieldWidth += plant.getSpreadDiameterInCM();

            if (spaceBetweenRows > 0) {
                fieldWidth += spaceBetweenRows;
            }

            if (northSouthDistance > 0) {
                plant.setAmount(Math.floorDiv(northSouthDistance, plant.getSpreadDiameterInCM()));
            } else {
                plant.setAmount(0);
            }
        });
        fieldWidth -= spaceBetweenRows;
        solution.setFieldWidthInCM(fieldWidth);

        if(northSouthDistance > 0) {
//            size: 100 m²; width (W-E): 10 m; height (N-S): 10 m
            if (fieldWidth >= 100 && northSouthDistance >= 100) {
                sb.append("size: ").append(fieldWidth * northSouthDistance / 10000.00).append(" m²; ");
                sb.append("width (W-E): ").append(fieldWidth / 100.00).append(" m; ");
                sb.append("height (N-S): ").append(northSouthDistance / 100.00).append(" m; ");
            } else {
                sb.append("size: ").append(fieldWidth * northSouthDistance).append(" cm²; ");
                sb.append("width (W-E): ").append(fieldWidth).append(" cm; ");
                sb.append("height (N-S): ").append(northSouthDistance).append(" cm; ");
            }

            solution.setFieldSize(sb.toString());
            solution.setFieldHeightInCM(northSouthDistance);
        } else {
            solution.setFieldHeightInCM(0);
        }

        sb = new StringBuilder();

        if (spaceBetweenRows > 0) {
            // The space between two rows is: 10 cm; Total empty space: 10 * (#plants - 1) cm
            sb.append("The space between two rows is: ").append(spaceBetweenRows).append(" cm; ");
            sb.append("Total empty space: ").append(spaceBetweenRows * (selectedPlants.size() - 1)).append(" cm;");

            solution.setSpaceBetweenRows(sb.toString());
            solution.setSpaceBetweenRowsInCM(spaceBetweenRows);
        } else {
            solution.setSpaceBetweenRowsInCM(0);
        }

        sb = new StringBuilder();
        if (maxBadCompanionsDistance) {
            sb.append("Distances between bad companions were maximized.");
            solution.setMaxBadCompanionsDistance(sb.toString());
        }

        sb = new StringBuilder();
        if (maxAmountOfSun) {
            sb.append("The amount of sun received by each plant was maximized.");
            solution.setMaxAmountOfSun(sb.toString());
        }
    }

    @SuppressWarnings("Duplicates")
    public void findLayoutWithMaxAmountOfSunConventional() {
        stopCalculations();

        if (filteredOrders != null) {
            heightData = hdPrep.getFilteredResults(filteredOrders);
            heightDataIndices = hdPrep.getFilteredResultsIndices();
        } else {
            heightData = hdPrep.getResults();
            heightDataIndices = hdPrep.getResultsIndices();
        }

        List<Integer> indices = new ArrayList<>();
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < heightData.length; i++) {
            if (min > heightData[i]) {
                indices = new ArrayList<>();
                indices.add(heightDataIndices[i]);
                min = heightData[i];
            } else if (min == heightData[i]) {
                indices.add(heightDataIndices[i]);
            }
        }

        showFilteredSolutions(indices/*, min*/);
    }

    @SuppressWarnings("Duplicates")
    public void findLayoutWithShortestDistanceToWaterSupplyConventional(FieldPositionValues fieldPositionValue) {
        this.fieldPositionValue = fieldPositionValue;
        stopCalculations();

        if (filteredOrders != null) {
            // always call the following in this order
            waterData = wdPrep.getFilteredResults(fieldPositionValue, filteredOrders);
            waterDataIndices = wdPrep.getFilteredResultsIndices();
        } else {
            waterData = wdPrep.getResults(fieldPositionValue);
            waterDataIndices = wdPrep.getResultsIndices();
        }

        List<Integer> indices = new ArrayList<>();

        double max = Double.MIN_VALUE;
        for (int i = 0; i < waterData.length; i++) {
            if (max < waterData[i]) {
                indices = new ArrayList<>();
                indices.add(waterDataIndices[i]);
                max = waterData[i];
            } else if (max == waterData[i]) {
                indices.add(waterDataIndices[i]);
            }
        }

        showFilteredSolutions(indices);
    }

    @SuppressWarnings("Duplicates")
    public void findLayoutWithMaximumDistanceBetweenBCs() {
        stopCalculations();

        if (filteredOrders != null) {
            // always call the following in this order
            bcData = bcdPrep.getFilteredResults(filteredOrders);
            bcDataIndices = bcdPrep.getFilteredResultsIndices();
        } else {
            bcData = bcdPrep.getResults();
            bcDataIndices = bcdPrep.getResultsIndices();
        }

        List<Integer> indices = new ArrayList<>();

        int max = Integer.MIN_VALUE;
        for (int i = 0; i < bcData.length; i++) {
            if (max < bcData[i]) {
                indices = new ArrayList<>();
                indices.add(bcDataIndices[i]);
                max = bcData[i];
            } else if (max == bcData[i]) {
                indices.add(bcDataIndices[i]);
            }
        }

        showFilteredSolutions(indices);
    }

    public void findLayoutWithOnlyGCAsNeighbours() {
        stopCalculations();

        if(filteredOrders != null) {
            gcDataIndices = gcnhdPrep.getFilteredResultIndices(filteredOrders);
        } else {
            gcDataIndices = gcnhdPrep.getResultIndices();
        }

        showFilteredSolutions(Arrays.stream(gcDataIndices).boxed().collect(Collectors.toList()));
    }

    private void showFilteredSolutions(List<Integer> indices) {
        filteredOrders = new ArrayList<>();
        for (Integer i : indices) {
            filteredOrders.add(new PlantOrder(plantOrders.get(i).getId(), plantOrders.get(i).getName(), plantOrders.get(i).getPlants()));
        }
        saveInitialPlantOrders(plantOrders);
        solution.setPlantOrders(filteredOrders);
        controller.filterSolutions(solution);
    }

    private void showFilteredSolutions(List<Integer> indices, int minValue) {
        filteredOrders = new ArrayList<>();
        for (Integer i : indices) {
            filteredOrders.add(new PlantOrder(plantOrders.get(i).getId(), plantOrders.get(i).getName(), plantOrders.get(i).getPlants(), minValue));
        }
        saveInitialPlantOrders(plantOrders);
        solution.setPlantOrders(filteredOrders);
        controller.filterSolutions(solution);
    }

    @SuppressWarnings("UnusedReturnValue")
    private boolean saveInitialPlantOrders(List<PlantOrder> initPO) {
        if (initialPlantOrders.size() != 0) {
            return false;
        } else {
            initialPlantOrders = new ArrayList<>(initPO);
            return true;
        }
    }

    public void restoreInitialResults() {
        filteredOrders = null;
        solution.setPlantOrders(initialPlantOrders);
        controller.refreshSolutions(false);
        controller.showSolutions(solution, false);
    }

    public int getFieldWidthInCM() {
        return fieldWidth;
    }

    public void findAllOptimalSolutions(int currentOptimumValue) {
        this.findAllOptimalSolutions = true;
        this.currentOptimumValue = currentOptimumValue;
    }

    @SuppressWarnings("unused")
    public int getFieldWidth() {
        return fieldWidth;
    }
}
