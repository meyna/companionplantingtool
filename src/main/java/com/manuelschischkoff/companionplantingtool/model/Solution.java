package com.manuelschischkoff.companionplantingtool.model;


import java.util.List;

public class Solution {

    private String maxBadCompanionsDistance;
    private String maxAmountofSun;
    private String waterSupplyPos;
    private String fieldSize;
    private String spaceBetweenRows;
    private List<PlantOrder> plantOrders;

    private int fieldHeightInCM;
    private int fieldWidthInCM;
    private int spaceBetweenRowsInCM;

    public Solution() {
    }

    public int getFieldHeightInCM() {
        return fieldHeightInCM;
    }

    public void setFieldHeightInCM(int fieldHeightInCM) {
        this.fieldHeightInCM = fieldHeightInCM;
    }

    public int getFieldWidthInCM() {
        return fieldWidthInCM;
    }

    public void setFieldWidthInCM(int fieldWidthInCM) {
        this.fieldWidthInCM = fieldWidthInCM;
    }

    public int getSpaceBetweenRowsInCM() {
        return spaceBetweenRowsInCM;
    }

    public void setSpaceBetweenRowsInCM(int spaceBetweenRowsInCM) {
        this.spaceBetweenRowsInCM = spaceBetweenRowsInCM;
    }

    public String getMaxBadCompanionsDistance() {
        return maxBadCompanionsDistance;
    }

    public void setMaxBadCompanionsDistance(String maxBadCompanionsDistance) {
        this.maxBadCompanionsDistance = maxBadCompanionsDistance;
    }

    public String getWaterSupplyPos() {
        return waterSupplyPos;
    }

    public void setWaterSupplyPos(String waterSupplyPos) {
        this.waterSupplyPos = waterSupplyPos;
    }

    public String getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(String fieldSize) {
        this.fieldSize = fieldSize;
    }

    public String getSpaceBetweenRows() {
        return spaceBetweenRows;
    }

    public void setSpaceBetweenRows(String spaceBetweenRows) {
        this.spaceBetweenRows = spaceBetweenRows;
    }

    public List<PlantOrder> getPlantOrders() {
        return plantOrders;
    }

    public void setPlantOrders(List<PlantOrder> plantOrders) {
        this.plantOrders = plantOrders;
    }

    public void setMaxAmountOfSun(String string) {
        this.maxAmountofSun = string;
    }

    public String getMaxAmountofSun() {
        return maxAmountofSun;
    }
}
