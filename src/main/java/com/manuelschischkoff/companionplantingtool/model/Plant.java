package com.manuelschischkoff.companionplantingtool.model;

import java.io.Serializable;

/**
 * A entity object, like in any other Java application. In a typical real world
 * application this could for example be a JPA entity.
 */
@SuppressWarnings("serial")
public class Plant implements Serializable, Cloneable {

    private Integer id;

    private String name = "";

    private String gerName = "";

    private int[] goodCompanionIDs = new int[0];

    private int[] badCompanionIDs = new int[0];

    private int heightInCM;

    private int spreadDiameterInCM;

    private int waterConsumption;

    private int amount;

    private int xPos;

    private int yPos;

    private int spacing;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGerName() {
        return gerName;
    }

    public void setGerName(String gerName) {
        this.gerName = gerName;
    }

    public int getHeightInCM() {
        return heightInCM;
    }

    public void setHeightInCM(int heightInCM) {
        this.heightInCM = heightInCM;
    }

    public int getSpreadDiameterInCM() {
        return spreadDiameterInCM;
    }

    public void setSpreadDiameterInCM(int spreadDiameterInCM) {
        this.spreadDiameterInCM = spreadDiameterInCM;
    }

    public int getWaterConsumption() {
        return waterConsumption;
    }

    public void setWaterConsumption(int waterConsumption) {
        this.waterConsumption = waterConsumption;
    }

    public int[] getGoodCompanionIDs() {
        return goodCompanionIDs;
    }

    public void setGoodCompanionIDs(int[] goodCompanionIDs) {
        this.goodCompanionIDs = goodCompanionIDs;
    }

    public int[] getBadCompanionIDs() {
        return badCompanionIDs;
    }

    public void setBadCompanionIDs(int[] badCompanionIDs) {
        this.badCompanionIDs = badCompanionIDs;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getXPos() {
        return xPos;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public int getYPos() {
        return yPos;
    }

    public void setYPos(int yPos) {
        this.yPos = yPos;
    }

    public int getSpacing() {
        return spacing;
    }

    public void setSpacing(int spacing) {
        this.spacing = spacing;
    }

    public boolean isPersisted() {
        return id != null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (this.id == null) {
            return false;
        }

        if (obj instanceof Plant && obj.getClass().equals(getClass())) {
            return this.id.equals(((Plant) obj).id);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (id == null ? 0 : id.hashCode());
        return hash;
    }

    @Override
    public Plant clone() throws CloneNotSupportedException {
        return (Plant) super.clone();
    }

    @Override
    public String toString() {
        return name;
    }

    public String toSearchString() {
        return name + " " + gerName;
    }

}
