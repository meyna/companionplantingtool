package com.manuelschischkoff.companionplantingtool.model;

import java.util.List;

public class PlantOrder {

    private Integer id;

    private String name;

    private List<Plant> plants;

    private Integer optimizationValue = null;

    public PlantOrder(Integer id, String name, List<Plant> plants) {
        this.id = id;
        this.name = name;
        this.plants = plants;
    }

    public PlantOrder(Integer id, String name, List<Plant> plants, int minValue) {
        this.id = id;
        this.name = name;
        this.plants = plants;
        this.optimizationValue = minValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Plant> getPlants() {
        return plants;
    }

    public void setPlants(List<Plant> plants) {
        this.plants = plants;
    }

    public Integer getOptimizationValue() {
        return optimizationValue;
    }

    public void setOptimizationValue(int optimizationValue) {
        this.optimizationValue = optimizationValue;
    }

    @Override
    public String toString() {
        return name;
    }
}
